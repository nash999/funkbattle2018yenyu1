﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialUIController : MonoBehaviour
{
  public Text _moveText;
  public Text _energyText;

  void Start()
  {
    //Debug.Log("Update text: " + GameManager.instance._avatarMoveTutorialString);
    _moveText.text = GameManager.instance._avatarMoveTutorialString;
    _energyText.text = GameManager.instance._energyGatherTutorialString;
  }
}