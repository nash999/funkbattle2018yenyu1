// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
// Updatee NOTE: Added custom acos because original acos function won't work in iPhone 5 device

Shader "Werewolf/IndicatorsMobile/FillCone"
{
  Properties
  {
	_MainColor ("Main Color", Color) = (1,1,1,1)
    _FillColor ("Fill Color", Color) = (1,1,1,1)
    _ProjectTex ("Shape", 2D) = "" {}
    _Expand ("Expand", Range (0,1)) = 0
    _Fill ("Fill", Range (0,1)) = 0
    }
    
    Subshader
    {
      Tags
      {
        "Queue"="Transparent"
      }
      Pass
      {
        ZWrite Off
        AlphaTest Greater 0
        ColorMask RGB
        Blend SrcAlpha OneMinusSrcAlpha
        Offset -1, -1
        
        CGPROGRAM
        
        //#pragma target 2.0
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile_fog
        
#include "UnityCG.cginc"
        
#define PI 3.1415926
#define PI_FLOAT     3.14159265f
#define PIBY2_FLOAT  1.5707963f    
   
        fixed _Expand;
        fixed _Fill;
        fixed4 _MainColor;
        fixed4 _FillColor;
        fixed4 _MainTex_ST;
        float4x4 unity_Projector;
        sampler2D _ProjectTex;
        
        fixed gt_than(float x, float y)
        {
          return max(sign(x - y), 0);
        }
        
        fixed ls_than(float x, float y)
        {
          return max(sign(y - x), 0);
        }
        
        struct vInput
        {
          float4 vertex : POSITION;
          fixed2 texcoord : TEXCOORD0;
        };
        
        struct vOutput
        {
          float4 uvMain : TEXCOORD0;
          
          UNITY_FOG_COORDS(2)
          float4 pos : SV_POSITION;
        };

		// |error| < 0.005
		float atan2_approximation2( float y, float x )
		{
			if ( x == 0.0f )
			{
				if ( y > 0.0f ) return PIBY2_FLOAT;
				if ( y == 0.0f ) return 0.0f;
				return -PIBY2_FLOAT;
			}
			float atan;
			float z = y/x;
			if ( abs( z ) < 1.0f )
			{
				atan = z/(1.0f + 0.28f*z*z);
				if ( x < 0.0f )
				{
					if ( y < 0.0f ) return atan - PI_FLOAT;
					return atan + PI_FLOAT;
				}
			}
			else
			{
				atan = PIBY2_FLOAT - z/(z*z + 0.28f);
				if ( y < 0.0f ) return atan - PI_FLOAT;
			}
			return atan;
		}

        float acos_mobile(float x)
        {
      	  // Faster approxi with slight error
          //return (-0.69813170079773212 * x * x - 0.87266462599716477) * x + 1.5707963267948966;
           float negate = float(x < 0);
  x = abs(x);
  float ret = -0.0187293;
  ret = ret * x;
  ret = ret + 0.0742610;
  ret = ret * x;
  ret = ret - 0.2121144;
  ret = ret * x;
  ret = ret + 1.5707288;
  ret = ret * sqrt(1.0-x);
  ret = ret - 2 * negate * ret;
  return negate * 3.14159265358979 + ret;
        }
        
        vOutput vert (vInput v)
        {
          vOutput o;
          o.pos = UnityObjectToClipPos(v.vertex);
          o.uvMain = mul(unity_Projector, v.vertex);
          UNITY_TRANSFER_FOG(o,o.pos);
          return o;
        }
        
        fixed4 frag(vOutput i) : SV_Target
        {
          // Get angle pos
          //fixed4 center = fixed4(0.5, 0.5, i.uvMain.z, i.uvMain.w);
          //fixed4 up = fixed4(0.5, 1.0, i.uvMain.z, i.uvMain.w) - center;
          //fixed4 current = i.uvMain - center;
          // Calculate angle and result in degree
          //float currentAngle = acos_mobile(dot(up, current) / (length(up) * length(current))) * (180 / PI);
       
          // Result angle is -180 - 180, so we need to get absolute value
          // Faster atan2 approxi function because atan2 is very costly in frag shader
          float currentAngle = abs(atan2_approximation2(i.uvMain.x-0.5 , i.uvMain.y-0.5) * (180 / PI));

          float expandAngle = _Expand * 180;
          
          fixed4 main = tex2Dproj(_ProjectTex, UNITY_PROJ_COORD(i.uvMain));
          fixed4 fill = tex2Dproj(_ProjectTex, UNITY_PROJ_COORD(i.uvMain));
          
          main *= _MainColor;
          fill *= _FillColor;
          
          //fixed visBlit = 0;
          //visBlit += gt_than(expandAngle, currentAngle);
          
          fixed mainBlit = max(0, sign(i.uvMain.y - _Fill));
          fixed fillBlit = max(0, sign(_Fill - i.uvMain.y));
          
          // Fill
          fixed4 res = fixed4(0, 0, 0, 0);
          res += main * fixed4(mainBlit, mainBlit, mainBlit, mainBlit);
          res += fill * fixed4(fillBlit, fillBlit, fillBlit, fillBlit);
          // Update the alpha value based on angle
          // This will hide the color outside angle
          res.a = expandAngle < currentAngle ? 0 : res.a;
          // Circle fill
          //res *= fixed4(visBlit, visBlit, visBlit, visBlit);
          
          UNITY_APPLY_FOG_COLOR(i.fogCoord, res, fixed4(0,0,0,0));
          
          return res;
        }
        ENDCG
      }
	}
}