﻿using UnityEngine;
using Facebook.Unity;

public class FBInit : MonoBehaviour
{

  // Use this for initialization
  void Start()
  {
    if(!FB.IsInitialized)
      FB.Init(this.OnInitComplete, this.OnHideUnity);
  }

  private void OnInitComplete()
  {
    Debug.Log("FB Init Complete");
  }

  private void OnHideUnity(bool isGameShown)
  {
    Debug.Log("FB:OnHideUnity isGameShown: " + isGameShown);
  }
}