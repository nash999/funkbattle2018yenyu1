#pragma warning disable 612,618
#pragma warning disable 0114
#pragma warning disable 0108

using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
		public class LogEventRequest_PostScoreEventForSurvivalMode : GSTypedRequest<LogEventRequest_PostScoreEventForSurvivalMode, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_PostScoreEventForSurvivalMode() : base("LogEventRequest"){
			request.AddString("eventKey", "PostScoreEventForSurvivalMode");
		}
		public LogEventRequest_PostScoreEventForSurvivalMode Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		
		public LogEventRequest_PostScoreEventForSurvivalMode Set_NAME( string value )
		{
			request.AddString("NAME", value);
			return this;
		}
		
		public LogEventRequest_PostScoreEventForSurvivalMode Set_ID( string value )
		{
			request.AddString("ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_PostScoreEventForSurvivalMode : GSTypedRequest<LogChallengeEventRequest_PostScoreEventForSurvivalMode, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_PostScoreEventForSurvivalMode() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "PostScoreEventForSurvivalMode");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_PostScoreEventForSurvivalMode SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_PostScoreEventForSurvivalMode Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogChallengeEventRequest_PostScoreEventForSurvivalMode Set_NAME( string value )
		{
			request.AddString("NAME", value);
			return this;
		}
		public LogChallengeEventRequest_PostScoreEventForSurvivalMode Set_ID( string value )
		{
			request.AddString("ID", value);
			return this;
		}
	}
	
}
	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_SurvivalModeGlobalLeaderboard : GSTypedRequest<LeaderboardDataRequest_SurvivalModeGlobalLeaderboard,LeaderboardDataResponse_SurvivalModeGlobalLeaderboard>
	{
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "SurvivalModeGlobalLeaderboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_SurvivalModeGlobalLeaderboard (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_SurvivalModeGlobalLeaderboard SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard : GSTypedRequest<AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard,AroundMeLeaderboardResponse_SurvivalModeGlobalLeaderboard>
	{
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "SurvivalModeGlobalLeaderboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_SurvivalModeGlobalLeaderboard (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SurvivalModeGlobalLeaderboard SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_SurvivalModeGlobalLeaderboard : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_SurvivalModeGlobalLeaderboard(GSData data) : base(data){}
		public string NAME{
			get{return response.GetString("NAME");}
		}
		public long? SCORE{
			get{return response.GetNumber("SCORE");}
		}
		public string ID{
			get{return response.GetString("ID");}
		}
	}
	
	public class LeaderboardDataResponse_SurvivalModeGlobalLeaderboard : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_SurvivalModeGlobalLeaderboard(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard> Data_SurvivalModeGlobalLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SurvivalModeGlobalLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard> First_SurvivalModeGlobalLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SurvivalModeGlobalLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard> Last_SurvivalModeGlobalLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SurvivalModeGlobalLeaderboard(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_SurvivalModeGlobalLeaderboard : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_SurvivalModeGlobalLeaderboard(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard> Data_SurvivalModeGlobalLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SurvivalModeGlobalLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard> First_SurvivalModeGlobalLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SurvivalModeGlobalLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard> Last_SurvivalModeGlobalLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_SurvivalModeGlobalLeaderboard>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SurvivalModeGlobalLeaderboard(data);});}
		}
	}
}	

namespace GameSparks.Api.Messages {


}
