﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;
using UnitySampleAssets.CrossPlatformInput;

public class JoystickInputController : MonoBehaviour
{
  public string _horizontal = "Horizontal";
  public string _vertical = "Vertical";
  public string _attackKey = "Fire1";

  public SharedData _data;
  public ActionBase _attack;

  public bool _canTakeInput = true;

  void OnEnable()
  {
    _canTakeInput = true;
    //CnInputManager.
  }

 
  void Update()
  {
    if(!CanTakeInput())
      return;

    CheckMovement();
    CheckAttack();
  }

  private void CheckMovement()
  {
    if(!_data._canMove)
      return;
    // Check touch joystick first then keyboard or physical joystick
    _data._moveDir.x = CnInputManager.GetAxis(_horizontal);
    if(_data._moveDir.x == 0)
      _data._moveDir.x = CrossPlatformInputManager.GetAxisRaw(_horizontal);

    _data._moveDir.y = CnInputManager.GetAxis(_vertical);
    if(_data._moveDir.y == 0)
      _data._moveDir.y = CrossPlatformInputManager.GetAxisRaw(_vertical);
  }

  private void CheckAttack()
  {
    if(_attack == null)
      return;
    if(!_data._canAttack)
      return;
    // If the Fire1 button is being press and it's time to fire...
    if(CnInputManager.GetButtonDown(_attackKey))
    {
      _attack.ProcessDown();
    }
    else if(CnInputManager.GetButton(_attackKey))
    {
      _attack.ProcessPress();
    }
    else if(CnInputManager.GetButtonUp(_attackKey))
    {
      _attack.ProcessUp();
    }

  }

  private bool CanTakeInput()
  {
    if(_data != null && _data._currentLife <= 0)
      return false;
    return _canTakeInput;
  }
}