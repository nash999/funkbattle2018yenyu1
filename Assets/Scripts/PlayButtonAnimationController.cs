﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButtonAnimationController : MonoBehaviour
{
  public GameObject _leftSword;
  public GameObject _rightSword;
  public Vector3 _defaultLRot = new Vector3(0,0,45);
  public Vector3 _defaultRRot = new Vector3(0,180,45);
  public Vector3 _newLRot = new Vector3(0,0,0);
  public Vector3 _newRRot = new Vector3(0,180,0);
  public float _animatedTime = 0.5f;

  public void OnGameStart()
  {
    
    LeanTween.rotate(_leftSword, _newLRot, _animatedTime).setEaseOutBounce();
    LeanTween.rotate(_rightSword, _newRRot, _animatedTime).setEaseOutBounce();
  }

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGameOverNotification, OnGameOver);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.AddListener(Constants.kCharacterSelectionNotifiation, OnCharSelect);
    Messenger.AddListener(Constants.kNetworkSystemRoomReadyNotification, OnNetworkSystemInRoom);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGameOverNotification, OnGameOver);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.RemoveListener(Constants.kCharacterSelectionNotifiation, OnCharSelect);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomReadyNotification, OnNetworkSystemInRoom);
  }

  private void OnGameOver()
  {
    LeanTween.rotate(_leftSword, _defaultLRot, _animatedTime).setEaseOutBounce();
    LeanTween.rotate(_rightSword, _defaultRRot, _animatedTime).setEaseOutBounce();
  }

  private void OnCharRestart()
  {
    OnGameOver();
  }

  private void OnCharSelect()
  {
    OnGameStart();
  }

  private void OnNetworkSystemInRoom()
  {
    OnGameOver();
  }

}