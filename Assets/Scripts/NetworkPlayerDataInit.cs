﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkPlayerDataInit : MonoBehaviour
{
  public PhotonView _view;
  public PlayerData _data;
  public AvatarAnimatorController _animatorController;
  public bool _saveDataToDisk = true;

  private Constants.CareerType m_defaultCareerType;

  public void SaveData()
  {
    if(!_view.isMine)
      return;
    if(!_saveDataToDisk)
      return;
    // General 
    //Debug.Log("save score: " + _data._currentScore);
    //Debug.Log("save skill level: " + + _data._currentSkillLevel);
    PlayerPrefs.SetInt(Constants.kPlayerScoreKey, _data._currentScore);
    PlayerPrefs.SetInt(Constants.kSkillLevelKey, _data._currentSkillLevel);

    // General level data
    PlayerPrefs.SetInt(Constants.kMaxEnergyLevelKey, _data._currentMaxEnergyLevel);
    PlayerPrefs.SetInt(Constants.kEnergyRecoveryLevelKey, _data._currentEnergyRecoveryLevel);
    PlayerPrefs.SetInt(Constants.kRotationLevelKey, _data._currentRotationSpeedLevel);
    PlayerPrefs.SetInt(Constants.kWeaponDamageForceLevelKey, _data._currentWeaponDamageForceLevel);
    PlayerPrefs.SetInt(Constants.kWeaponSlowSpeedResetLevelKey, _data._currentWeaponSlowSpeedResetLevel);
    PlayerPrefs.SetInt(Constants.kWeaponAttachDamageLevelKey, _data._currentWeaponAttachDamageLevel);

    // Skill A
    PlayerPrefs.SetInt(Constants.kAttackIntervalLevelKey, _data._currentAttackIntervalLevel);
    PlayerPrefs.SetInt(Constants.kMaxLifeLevelKey, _data._currentMaxLifeLevel);
    PlayerPrefs.SetInt(Constants.kWeaponKillTimerLevelKey, _data._currentWeaponActiveTimeLevel);

    // Skill B
    PlayerPrefs.SetInt(Constants.kWeaponRegularDamageLevelKey, _data._currentWeaponRegularDamageLevel);
    PlayerPrefs.SetInt(Constants.kWeaponUltimateDamageLevelKey, _data._currentWeaponUltimateDamageLevel);
    PlayerPrefs.SetInt(Constants.kWeaponBleedingLevelKey, _data._currentWeaponBleedingLevel);

    // Skill C
    PlayerPrefs.SetInt(Constants.kWeaponSuckBloodLevelKey, _data._currentWeaponSuckBloodLevel);
    PlayerPrefs.SetInt(Constants.kDefenseLevelKey, _data._currentDefenseLevel);
    PlayerPrefs.SetInt(Constants.kForwardSpeedLevelKey, _data._currentForwardSpeedLevel);

    // Skill Random
    PlayerPrefs.SetInt(Constants.kWeaponSlowSpeedLevelKey, _data._currentWeaponSlowSpeedLevel);
    PlayerPrefs.SetInt(Constants.kWeaponKnockbackLevelKey, _data._currentWeaponRandomKnockBackLevel);
    PlayerPrefs.SetInt(Constants.kWeaponFastAttackLevelKey, _data._currentFastAttackLevel);
    PlayerPrefs.SetInt(Constants.kSkillDodgeLevelKey, _data._currentDodgeLevel);
    PlayerPrefs.SetInt(Constants.kSkillMagnetLevelKey, _data._currentMagnetLevel);

    // Career
    PlayerPrefs.SetInt(Constants.kCareerTypeKey, (int)_data._careerType);
    _animatorController.UpdateAnimator();
  }

  void OnEnable()
  {
    m_defaultCareerType = _data._careerType;
    InitData();
    _animatorController.UpdateAnimator();
  }

  private void InitData()
  {
    if(!_view.isMine)
      return;
    if(!_saveDataToDisk)
      return;
    // Genereal data
    _data._currentScore = PlayerPrefs.GetInt(Constants.kPlayerScoreKey, 0);
    _data._currentSkillLevel = PlayerPrefs.GetInt(Constants.kSkillLevelKey, 0);
    //Debug.Log("Get skill level: " + _data._currentSkillLevel);
    _data._currentWeaponAttachDamageLevel = PlayerPrefs.GetInt(Constants.kWeaponAttachDamageLevelKey, 0);

    // General level data 
    _data._currentMaxEnergyLevel = PlayerPrefs.GetInt(Constants.kMaxEnergyLevelKey, 0);
    _data._currentEnergyRecoveryLevel = PlayerPrefs.GetInt(Constants.kEnergyRecoveryLevelKey, 0);
    _data._currentRotationSpeedLevel = PlayerPrefs.GetInt(Constants.kRotationLevelKey, 0);
    _data._currentWeaponDamageForceLevel = PlayerPrefs.GetInt(Constants.kWeaponDamageForceLevelKey, 0);
    _data._currentWeaponSlowSpeedResetLevel = PlayerPrefs.GetInt(Constants.kWeaponSlowSpeedResetLevelKey, 0);

    // Skill A
    _data._currentAttackIntervalLevel = PlayerPrefs.GetInt(Constants.kAttackIntervalLevelKey, 0);
    _data._currentMaxLifeLevel = PlayerPrefs.GetInt(Constants.kMaxLifeLevelKey, 0);
    _data._currentWeaponActiveTimeLevel = PlayerPrefs.GetInt(Constants.kWeaponKillTimerLevelKey, 0);

    // Skill B
    _data._currentWeaponRegularDamageLevel = PlayerPrefs.GetInt(Constants.kWeaponRegularDamageLevelKey, 0);
    _data._currentWeaponUltimateDamageLevel = PlayerPrefs.GetInt(Constants.kWeaponUltimateDamageLevelKey, 0);
    _data._currentWeaponBleedingLevel = PlayerPrefs.GetInt(Constants.kWeaponBleedingLevelKey, 0);

    // Skill C
    _data._currentWeaponSuckBloodLevel = PlayerPrefs.GetInt(Constants.kWeaponSuckBloodLevelKey, 0);
    _data._currentDefenseLevel = PlayerPrefs.GetInt(Constants.kDefenseLevelKey, 0);
    _data._currentForwardSpeedLevel = PlayerPrefs.GetInt(Constants.kForwardSpeedLevelKey, 0);

    // Skill Random
    _data._currentWeaponSlowSpeedLevel = PlayerPrefs.GetInt(Constants.kWeaponSlowSpeedLevelKey, 0);
    _data._currentWeaponRandomKnockBackLevel = PlayerPrefs.GetInt(Constants.kWeaponKnockbackLevelKey, 0);
    _data._currentFastAttackLevel = PlayerPrefs.GetInt(Constants.kWeaponFastAttackLevelKey, 0);
    _data._currentDodgeLevel = PlayerPrefs.GetInt(Constants.kSkillDodgeLevelKey, 0);
    _data._currentMagnetLevel = PlayerPrefs.GetInt(Constants.kSkillMagnetLevelKey, 0);

    // Career
    _data._careerType = (Constants.CareerType)PlayerPrefs.GetInt(Constants.kCareerTypeKey, (int)m_defaultCareerType);

    // Update values
    _data._currentLife = _data.MaxLife;
    _data._canAttack = true;
    _data._canMove = true;
    _data._canRotate = true;
  }
}