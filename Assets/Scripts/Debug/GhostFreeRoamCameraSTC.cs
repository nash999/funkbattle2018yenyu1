﻿//Ghost Free Roam Camera, improved from GhostFreeRoamCamera.cs by STC, 2017/06/16
//contact: stc.ntu@gmail.com
//usage: apply onto a camera (suggested main), then you can "gain control": FPS-like, flying, speedup-allowed, cursor-toggle-allowed controlled. And it's so smoooooth! Enjoy! 
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class GhostFreeRoamCameraSTC : MonoBehaviour
{
    public float initialSpeed = 10f;
    public float increaseSpeed = 1.25f;

    public bool allowMovement = true;
    public bool allowRotation = true;

    public KeyCode forwardButton = KeyCode.W;
    public KeyCode backwardButton = KeyCode.S;
    public KeyCode rightButton = KeyCode.D;
    public KeyCode leftButton = KeyCode.A;

    public float cursorSensitivity = 0.025f;

    public bool cursorToggleAllowed = true;
    public KeyCode cursorToggleButton = KeyCode.Escape;

    public bool speedUpAllowed = true;
    public KeyCode speedUpButton = KeyCode.LeftShift;
    public float speedUpSpeed = 2.5f;

    private float currentSpeed = 0f;
    private bool moving = false;
    private bool isToggled = true;
    private Vector3 movingDist;
    private bool allowMovementInitial;
    private bool allowRotationInitial;

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        allowMovementInitial = allowMovement;
        allowRotationInitial = allowRotation;
    }

    


    private void Update()
    {
        if (cursorToggleAllowed)
        {
            if (Input.GetKeyDown(cursorToggleButton))
            {
                isToggled = !isToggled;
                Cursor.visible = !Cursor.visible;

                //back inside screen
                if (isToggled)
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    allowMovement = allowMovementInitial;
                    allowRotation = allowRotationInitial;
                }

                //make cursor free
                else
                {
                    Cursor.lockState = CursorLockMode.None;
                    allowMovement = false;
                    allowRotation = false;
                    
                }
            }
        }

        if (allowMovement)
        {
            bool lastMoving = moving;
            Vector3 deltaPosition = Vector3.zero;

            if (moving) currentSpeed += increaseSpeed * Time.deltaTime;

            moving = false;
            
                CheckMove(forwardButton, ref deltaPosition, transform.forward);
                CheckMove(backwardButton, ref deltaPosition, -transform.forward);
                CheckMove(rightButton, ref deltaPosition, transform.right);
                CheckMove(leftButton, ref deltaPosition, -transform.right);
            
            

            if (moving)
            {
                if (moving != lastMoving)
                    currentSpeed = initialSpeed;

                movingDist = deltaPosition * currentSpeed * Time.deltaTime;
                if (Input.GetKey(speedUpButton)) movingDist *= speedUpSpeed;
                transform.position += movingDist;
            }
            else currentSpeed = 0f;

        }

        if (allowRotation)
        {
            Vector3 eulerAngles = transform.eulerAngles;
            eulerAngles.x += -Input.GetAxis("Mouse Y") * 359f * cursorSensitivity;
            eulerAngles.y += Input.GetAxis("Mouse X") * 359f * cursorSensitivity;

            //"overhead" prevention
            if (eulerAngles.x > 90 && eulerAngles.x < 180) eulerAngles.x = 90;
            if (eulerAngles.x < 270 && eulerAngles.x > 180) eulerAngles.x = 270;

            //apply
            transform.eulerAngles = eulerAngles;
        }

        
    }

    private void CheckMove(KeyCode keyCode, ref Vector3 deltaPosition, Vector3 directionVector)
    {
        if (Input.GetKey(keyCode))
        {
            moving = true;
            deltaPosition += directionVector;
        }
    }
}
