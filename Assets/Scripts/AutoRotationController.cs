﻿using UnityEngine;
using System.Collections;
using System;

public class AutoRotationController : MonoBehaviour
{ 
  public float _animatedTime = 5.0f;
  public float _axisDegreeAdd = 360.0f;
  public Vector3 _rotationAxis = Vector3.forward;

  // Update is called once per frame
  void OnEnable()
  {
    StartRotation();
  }

  void OnDisable()
  {
    CancelRotation();
  }

  public void StartRotation()
  {
    LeanTween.rotateAround(gameObject, _rotationAxis, _axisDegreeAdd, _animatedTime).setRepeat(-1);
  }

  public void CancelRotation()
  {
    LeanTween.cancel(gameObject);
  }
}