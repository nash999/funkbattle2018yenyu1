﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkBuffInit : MonoBehaviour
{
  public PhotonView _view;
  public WeaponData _data;

  public void OnPhotonInstantiate(PhotonMessageInfo info)
  {
    //Debug.Log("haha");
    //Debug.Log("NetworkBuffInit: " + gameObject.name);
    object[] data = _view.instantiationData;
    if(data == null)
      return;
    if(data.Length == 0)
      return;
    int exp = int.Parse(data[0].ToString());
    // Pick the higher one 
    if(exp > _data._deathEXP)
      _data._deathEXP = exp;
    string buffTag = data[1].ToString();
    _data._itemType = buffTag;
  }
}
