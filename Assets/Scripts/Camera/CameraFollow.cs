﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;

public class CameraFollow : MonoBehaviour
{
  // The position that that camera will be following.
  public List<GameObject> _targets;
  // The speed with which the camera will be following.
  public float _smoothing = 5f;
  // Distance away from player
  public Vector3 _offset;
  public float _minCameraSize = 4.5f;
  public float _maxCameraSize = 10.0f;
  public float _cameraSizeByDis = 5.0f;

  private float m_dis;
  private float m_size;

  void LateUpdate()
  {
    if(_targets.Count == 0)
      return;
    if(GameManager.instance._gameState != Constants.GameState.GamePlay)
      return;

    // Create a postion the camera is aiming for based on the offset from the target.
    //Vector3 targetCamPos = _target.position + _offset;
    Vector3 targetCamPos = GetCenter(_targets) + _offset;

    // Smoothly interpolate between the camera's current position and it's target position.
    transform.position = Vector3.Lerp(transform.position, targetCamPos, _smoothing * Time.deltaTime);

    // Update the camera size
    m_size = Mathf.Clamp(_minCameraSize + (m_dis / _cameraSizeByDis), _minCameraSize, _maxCameraSize);
    //Debug.Log("Camera follow size: " + m_size);
    Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, m_size, _smoothing * Time.deltaTime);
    
  }

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.AddListener<Hashtable>(Constants.kUpdateCameraOffsetNotification, OnCameraUpdate);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.RemoveListener<Hashtable>(Constants.kUpdateCameraOffsetNotification, OnCameraUpdate);
  }

  #region Notification Handlers

  private void OnCharSpawn(Hashtable data)
  {
    // Get the char reference
    GameObject obj = (GameObject)data[Constants.kCharacterKey];
    if(obj == null)
      return;
    _targets.Add(obj);
    //_target = obj.transform;
  }

  private void OnCameraUpdate(Hashtable data)
  { 
    Vector3 newOffset = (Vector3)data[Constants.kCameraOffsetKey];

    //vector
    _offset = newOffset;
  }

  private void OnCharDeath(Hashtable data)
  {
    GameObject deadObj = (GameObject)data[Constants.kCharacterKey];
    _targets.Remove(deadObj);
  }

  #endregion

  #region Internal 

  private Vector3 GetCenter(List<GameObject> allObjs)
  {
    
    Vector3 total = Constants.kVector3Zero;
    float count = 0;

    foreach(GameObject g in allObjs)
    {
      //PlayerLifeController life = g.GetComponentInChildren<PlayerLifeController>();
      //if(life._data._currentLife > 0)
      {
        total += g.transform.position;
        count++;
      }

    }

    Vector3 center = (total / count); 
    if(count > 0)
    {
      // Calcualte the distance between center and first obj
      m_dis = Vector3.Distance(center, allObjs[0].transform.position);
      //Debug.Log(m_dis);
    }

    return center;
  }

  #endregion
}