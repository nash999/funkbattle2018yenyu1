﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Together;
using AppodealAds.Unity.Api;

public class GameOverMenuController : MonoBehaviour
{
  public GameObject _menu;
  public GameObject _rewardMenu;
  public GameObject _rewardButtonObj;

  public float _waitTime = 3.0f;

  private string m_invokeString = "ShowMenu";

  public void OnRewardButtonDown()
  {
    _rewardMenu.SetActive(false);
    _rewardButtonObj.SetActive(true);
    _menu.SetActive(false);
    Debug.Log("OnRewardButtonDown");
    Messenger.Broadcast(Constants.kCharacterResumeNotification);
  }

  public void OnRestartButtonDown()
  {
    Debug.Log("OnRestartButtonDown");
    Messenger.Broadcast(Constants.kCharacterRestartNotification);
  }

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.AddListener(Constants.kYMVideoShowSuccess, OnVideoADSuccess);
    Messenger.AddListener(Constants.kYMVideoShowFailure, OnVideoADFailure);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharacterRestart);
    Messenger.AddListener(Constants.kNetworkSystemRoomLeftNotification, OnLeftRoom);

  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.RemoveListener(Constants.kYMVideoShowSuccess, OnVideoADSuccess);
    Messenger.RemoveListener(Constants.kYMVideoShowFailure, OnVideoADFailure);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharacterRestart);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomLeftNotification, OnLeftRoom);

  }

  #region Notification Handlers

  private void OnLeftRoom()
  {
    HideMenu();
  }

  private void OnCharacterRestart()
  {
    HideMenu();
  }

  private void OnVideoADSuccess()
  {
    // Make sure we are on the main thread
    UnityMainThreadDispatcher.Instance().Enqueue(DoPostVideoShown()); 
  }

  private IEnumerator DoPostVideoShown() 
  {
    Debug.Log("DoPostVideoShown");
    _rewardMenu.SetActive(true);
    _menu.SetActive(false);
    yield return null;
  }

  private void OnVideoADFailure()
  {
    // Player didn't complete the video ad completely
    // Disable button so player cannot click it again in this game play session
    _rewardButtonObj.SetActive(false);
  }

  private void OnCharDeath(Hashtable data)
  {
    //Debug.Log("OnCharDeath");
    GameObject deathPlayer = (GameObject)data[Constants.kCharacterKey];
    if(deathPlayer == null)
    {
      //Debug.Log("death player is null");
      return;
    }
    SharedData dpData = deathPlayer.GetComponentInChildren<SharedData>();
    if(dpData == null)
    {
      //Debug.Log("Data is null");
      return;
    }

    // Send out game over notification
    Messenger.Broadcast(Constants.kGameOverNotification);
    Invoke(m_invokeString, _waitTime);

  }

  #endregion

  private void HideMenu()
  {
    _rewardMenu.SetActive(false);
    _rewardButtonObj.SetActive(false);
    _menu.SetActive(false);
  }

  private void ShowMenu()
  {
    _rewardMenu.SetActive(false);
    _menu.SetActive(true);
    // Make sure we have ads to show
    // or we are in testing 
    #if UNITY_IOS || UNITY_ANDROID
    //Debug.Log("Can show yomob ad: " + TGSDK.CouldShowAd(Constants.kYMAdSceneId));
    //Debug.Log("Can show appodeal ad: " + Appodeal.isLoaded(Appodeal.REWARDED_VIDEO));
    //Debug.Log("Is ad in yomob debug: " + GameManager.instance._showDebugYomobAd);
    Debug.Log("Is ad in appodeal debug: " + GameManager.instance._showDebugAppodealAd);
    if
    (
      GameManager.instance._showDebugYomobAd 
      || GameManager.instance._showDebugAppodealAd 
      || GameManager.instance._passAD 
      || Appodeal.isLoaded(Appodeal.REWARDED_VIDEO) 
      //|| TGSDK.CouldShowAd(Constants.kYMAdSceneId)
    )
      _rewardButtonObj.SetActive(true);
    else
      _rewardButtonObj.SetActive(false);

    #else
      _rewardButtonObj.SetActive(false);
    #endif


  }
}