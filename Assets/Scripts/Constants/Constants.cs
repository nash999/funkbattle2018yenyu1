﻿using UnityEngine;
using System.Collections;

public class Constants : UnityEngine.ScriptableObject
{
  // For performance 
  public static readonly Vector3 kVector3Zero = Vector3.zero;
  public static readonly Quaternion kQuaternionIdentity = Quaternion.identity;

  #region Message

  public const string kTransformChangeMS = "OnTransformChanged";
  public const string kInactiveAnimationMS = "OnInactiveAnimation";
  public const string kActiveAnimationMS = "OnActiveAnimation";
  public const string kChosenAnimationMS = "OnChosenAnimation";
  public const string kNonChosenAnimationMS = "OnNonChosenAniamtion";
  public const string kForSpeedRatioUpdateMS = "OnForSpeedRatioUpdate";
  public const string kRotSpeedRatioUpdateMS = "OnRotSpeedRatioUpdate";
  public const string kInitWeaponMS = "OnWeaponInit";
  public const string kLevelUpMS = "OnLevelUp";
  public const string kSkillUpMS = "OnSkillUp";
  public const string kNetworkInstantiateMS = "OnPlayerNetworkInstantiate";

  #endregion

  #region GameSparks

  public const string kGSAvailableNotifiation = "GSAvailableNotificaiton";
  public const string kGSLoginSuccessNotification = "GSLoginSuccessNotification";
  public const string kGSLoginFailureNotificaiton = "GSLoginFailtureNotification";
  public const string kGSScorePostedNotification = "GSScorePostedNotification";

  #endregion

  #region YoMob AD

  public const string kYMAdSceneId = "Y9QEMYHlp68fGtv6c1f";
  public const string kYMVideoShowSuccess = "YMVideoShowSuccess";
  public const string kYMVideoShowFailure = "YMVideoShowFailure";

  #endregion

  #region Network Notifications

  public const string kNetworkSystemReadyNotification = "NetworkSystemReadyNotificaiton";
  public const string kNetworkSystemStartNotification = "NetworkSystemStartNotification";
  public const string kNetworkSystemRoomReadyNotification = "NetworkSystemRoomReadyNotification";
  public const string kNetworkSystemRoomLeftNotification = "NetworkSystemRoomLeftNotification";
  public const string kNetworkSystemDisconnectNotification = "NetworkSystemDisconnect";


  #endregion

  #region Notifications 

  public const string kCharacterSpawnNotification = "CharacterSpawnNotifcation";
  public const string kCharacterRestartNotification = "CharacterRestartNotification";
  public const string kCharacterResumeNotification = "CharacterResumeNotification";
  public const string kCharacterDeathNotification = "CharacterDeathNotification";
  public const string kCharacterSelectionNotifiation = "CharacterSelectionNotification";
  public const string kEnemyDeathNotification = "EnemyDeathNotification";
  public const string kGameOverNotification = "GameOverNotification";
  public const string kUpdateCameraOffsetNotification = "CameraOffsetUpdateNotification";
  public const string kTeamUpdateNotification = "TeamUpdateNotification";
  public const string kPostScoreNotification = "PostScoreNotification";
  public const string kDamageAppliedNotification = "DamageAppliedNotification";
  public const string kScoreUpdateNotification = "ScoreUpdateNotificadtion";
  public const string kCameraSizeUpdateNotification = "CameraSizeUpdateNotification";
  public const string kBuffDestroyNotification = "BuffDestroyNotification";
  public const string kGDPRReadyNotification = "GDPRReadyNotification";

  #endregion

  #region Joystick

  public const string kJoystickTransformKey = "JoystickTransformKey";
  public const string kJoystickFlickKey = "JoystickFlickKey";
  public const string kJoystickTapKey = "JoystickTapKey";

  public const string kJoystickTransformNotification = "JoystickTransformNotification";
  public const string kJoystickTransformCompleteNotification = "JoystickTransformCompleteNotification";
  public const string kJoystickFlickNotification = "JoystickFlickNotification";
  public const string kJoystickPressNotification = "JoystickPressNotification";
  public const string kJoystickReleaseNotification = "JoystickReleaseNotification";
  public const string kJoystickTapNotification = "JoystickTapNotification";



  #endregion

  #region Keys

  #if UNITY_IPHONE
  public const string kAppOdealKey = "8c43121eb46fe9027376c9b9daa1716c8f875bafaeb28785";
  #elif UNITY_ANDROID
  public const string kAppOdealKey = "db4ddfe98e39846c094d8c2d656bc0568e87d5bff8976358";
  #else
  public const string kAppOdealKey = "";
  #endif

  public const string kCharacterKey = "CharacterKey";
  public const string kColorKey = "ColorKey";
  public const string kCharacterPosKey = "CharacterPos";
  public const string kCharacterDataKey = "CharacterData";
  public const string kTeamKey = "TeamSideKey";
  public const string kCameraOffsetKey = "CameraOffsetKey";
  public const string kGSPLayerIDKey = "GSPlayerIDKey";
  public const string kGSUsernameKey = "GSUsernameKey";
  public const string kGSUserPassKey = "GSUserPassKey";
  public const string kDamageOutputKey = "DamageOutputKey";
  public const string kCameraSizeKey = "CameraSizeKey";
  public const string kItemTypeKey = "ItemTypeKey";
  public const string kPlayerNameKey = "PlayerNameKey";
  public const string kGDPRChoiceIndexKey = "GDPRChoiceIndexKey";
 

  // Levels
  public const string kPlayerScoreKey = "PlayerScoreKey";
  public const string kPlayerExpKey = "PlayerExpKey";
  public const string kSkillLevelKey = "SkillLevelKey";
  public const string kMaxLifeLevelKey = "MaxLifeLevelKey";
  public const string kMaxEnergyLevelKey = "MaxEnergyLevelKey";
  public const string kEnergyRecoveryLevelKey = "EnergyRecoveryLevelKey";
  public const string kAttackIntervalLevelKey = "AttackIntervalLevelKey";
  public const string kDefenseLevelKey = "DefenseLevelKey";
  public const string kForwardSpeedLevelKey = "ForwardSpeedLevelKey";
  public const string kRotationLevelKey = "RotationLevelKey";
  public const string kWeaponRegularDamageLevelKey = "WeaponRegularDamageLevelKey";
  public const string kWeaponUltimateDamageLevelKey = "WeaponUltimateDamageLevelKey";
  public const string kWeaponBleedingLevelKey = "WeaponBleedingLevelKey";
  public const string kWeaponSuckBloodLevelKey = "SuckBloodLevelKey";
  public const string kWeaponKillTimerLevelKey = "WeaponKillTimerLevelKey";
  public const string kWeaponDamageForceLevelKey = "WeaponDamageForceLevelKey";
  public const string kWeaponSlowSpeedLevelKey = "WeaponSlowSpeedLevelKey";
  public const string kWeaponSlowSpeedResetLevelKey = "WeaponSlowSpeedResetLevelKey";
  public const string kWeaponAttachDamageLevelKey = "WeaponAttachDamageLevelKey";
  public const string kWeaponKnockbackLevelKey = "WeaponKnockbackLevelKey";
  public const string kWeaponFastAttackLevelKey = "WeaponFastAttackLevelKey";
  public const string kSkillDodgeLevelKey = "SkillDodgeLevelKey";
  public const string kSkillMagnetLevelKey = "SkillMagnetLevelKey";
  public const string kCareerTypeKey = "CareerTypeKey";

  // Animation 
  public const string kMoveMultiplierKey = "MoveMultiplier";
  public const string kAttackMultiplierKey = "AttackMultiplier";
  public const string kIsWalkingKey = "IsWalking"; 

  // Unity Analytic Events
  public const string kAvatarChosenUAE = "ChooseAvatarUAE";
  public const string kPlayerLastLevelUAE = "PlayerLastLevelUAE";
  public const string kPlayerLevelUpUAE = "PlayerLevelUpUAE";
  public const string kYoMobUAE = "YoMobUAE";

  #endregion

  #region Tag

  public const string kPlayerTag = "Player";
  public const string kEnemyTag = "Enemy";

  #endregion

  #region Constants Variables

  public const string kServerVersionString = "FunkBattle0.0.4";
  //public const string kServerVersionString = "FunkBattle1.0.Test";

    public const int kMaxPlayersInRoom = 30;

  #endregion

  #region enum

  public enum TeamSide
  {
    Neutral = 0, // Enemy will use this side, same team cannot attack each other
    Red     = 1, // Player team, same team cannot attack each other
    Blue    = 2, // Player team, same team cannot attack each other
    PVP     = 3  // This team can attack any team, including the same team
  }

  public enum GameState
  {
    CharacterSelection,
    GamePlay,
    GameOver,
  }

  public enum UpdateType
  {
    Fire,
    Ice,
    Random,
  }

  public enum KeyType
  {
    Down,
    Press,
    Up,
  }

  public enum CareerType
  {
    Archer,
    IceArcher,
    Crossbowman,
    Ranger,
    Mage,
    WhiteMage,
    BurningMage,
    ElementMage,
    //SceneBuff,
  }

  public enum AIMovementType
  {
    WanderAndAttack,
    Follow,
  }

  public enum LevelButtonPosition
  {
    Left = 0,
    Middle = 1,
    Right = 2,
  }

  public enum CareerLevel
  {
    Newbie = 0,
    WeaponLevel = 5,
    CareerLevel1 = 10,
    CareerLevel2 = 20,
  }

  public enum RandomSkillType
  {
    SlowSpeed,
    RandomKnockBack,
    RandomFastAttack,
    RandomDodge,
    Magnet,
  }

  public enum RegularSkillType
  {
    RegularAttackSpeed,
    MaxLife,
    RegularAttackRange,
    RegularAttackDamage,
    UltimateAttackDamage,
    Bleed,
    SuckBlood,
    Defense,
    MoveSpeed,
  }

  #endregion

}