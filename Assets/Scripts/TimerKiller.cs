﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class TimerKiller : MonoBehaviour
{
  //public GameObject _root;
  public GameObject _explosion;
  public WeaponData _data;

  private string m_invokeName = "KillMe";

  void OnEnable()
  {
    Invoke(m_invokeName, _data.ActiveTime);
  }

  void OnDisable()
  {
    CancelInvoke(m_invokeName);
  } 

  private void KillMe()
  {
    if(_explosion)
    {
      GameObject spawnedObj = LeanPool.Spawn(_explosion, transform.position, Constants.kQuaternionIdentity);

      // Assign the team to spawned obj
      SharedData spawnedObjData = spawnedObj.GetComponentInChildren<SharedData>();
      if(spawnedObjData)
      {
        spawnedObjData._team = _data._team;
        spawnedObjData._expController = _data._expController;
        spawnedObjData._scoreController = _data._scoreController;

      }
    }

    DoDespawn();
  } 

  public virtual void DoDespawn()
  {
    LeanPool.Despawn(_data._root);
  }
}