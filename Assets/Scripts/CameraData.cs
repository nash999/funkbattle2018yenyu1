﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraData : MonoBehaviour
{
  // For each |_currentWeaponActiveTimeLevel| level
  public List<float> _cameraSizeRatioForWeaponActiveTimeLevel;
  // For each ultimate type 
  public List<float> _cameraSizeForLevel10;
  public List<float> _cameraSizeForLevel20;
  public List<float> _cameraResetDelayForUltimate;
}