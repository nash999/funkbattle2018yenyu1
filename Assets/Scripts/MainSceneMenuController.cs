﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneMenuController : MonoBehaviour
{
  public GameObject _mainMenu;
  public GameObject _charMenu;
  public GameObject _skillMenu;
  public GameObject _levelMenu;

  public void OnPanelCloseDown()
  {
    _charMenu.SetActive(false);
    _skillMenu.SetActive(false);
    _levelMenu.SetActive(false);
  }

  public void OnCharSelectionDown()
  {
    _charMenu.SetActive(true);
  }

  public void OnLevelSelectionDown()
  {
    _levelMenu.SetActive(true);
  }

  public void OnSkillDown()
  {
    _skillMenu.SetActive(true);
  }

  void Start()
  {
    // Disable submenus by default
    _charMenu.SetActive(false);
    _skillMenu.SetActive(false);
    _levelMenu.SetActive(false);
  }
}
