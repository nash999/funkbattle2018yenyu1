﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLifeHUDController : MonoBehaviour
{
  public Slider _healthSlider;                                 // Reference to the UI's health bar.
  public Image _damageImage;                                   // Reference to an image to flash on the screen on being hurt.

  public float flashSpeed = 5f;                               // The speed the damageImage will fade at.
  public Color flashColour = new Color(1f, 0f, 0f, 0.1f);     // The colour the damageImage is set to, to flash.

  private bool m_damaged;                                               // True when the player gets damaged.

  void OnEnable()
  {
    GameObject obj = GameObject.FindGameObjectWithTag("HealthSlider");
    if(obj)
    {
      _healthSlider = obj.GetComponent<Slider>();
      _healthSlider.value = _healthSlider.maxValue;
    }

    GameObject imgObj = GameObject.FindGameObjectWithTag("DamageImage");
    if(imgObj)
      _damageImage = imgObj.GetComponent<Image>();
  }

  void OnDisable()
  {
    _healthSlider = null;
    _damageImage = null;
  }

  void Update ()
  {
    if(_healthSlider == null)
      return;
    if(_damageImage == null)
      return;
    // If the player has just been damaged...
    if(m_damaged)
    {
      // ... set the colour of the damageImage to the flash colour.
      _damageImage.color = flashColour;
    }
    // Otherwise...
    else
    {
      // ... transition the colour back to clear.
      _damageImage.color = Color.Lerp (_damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
    }

    // Reset the damaged flag.
    m_damaged = false;
  }

  public void UpdateHUD(int currentLife)
  {
    m_damaged = true;
    _healthSlider.value = currentLife;
  }
}
