﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUIController : MonoBehaviour
{
  public GameObject _ui;

  public void OnPlayButtonDown()
  {
    _ui.SetActive(true);
  }

  // Use this for initialization
  void Start ()
  {
    // Disable by default
    _ui.SetActive(false); 
  }

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGameOverNotification, OnGameOver);
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.AddListener(Constants.kNetworkSystemRoomLeftNotification, OnLeftRoom);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGameOverNotification, OnGameOver);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomLeftNotification, OnLeftRoom);
  }

  #region Notification Handlers

  private void OnLeftRoom()
  {
    _ui.SetActive(false);
  }

  private void OnGameOver()
  {
    _ui.SetActive(false);
  }

  private void OnCharDeath(Hashtable data)
  {
    _ui.SetActive(false);
  }

  private void OnCharRestart()
  {
    _ui.SetActive(false);
  }

  #endregion
}