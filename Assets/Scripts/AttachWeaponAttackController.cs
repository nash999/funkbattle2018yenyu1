﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachWeaponAttackController : BasicWeaponAttackController
{
  private float m_lastAttackTime;

  public override void OnEnable()
  {
    base.OnEnable();
    m_lastAttackTime = _data.AttackInterval;
  }

	// Update is called once per frame
	void Update()
  {
    if(_data._ownerNetworkID == null)
      return;
    if(m_lastAttackTime > 0)
    {
      m_lastAttackTime -= Time.deltaTime;
      return;
    }

    m_lastAttackTime = _data.AttackInterval;

    ApplyDamage(transform);

	}

  private void ApplyDamage(Transform t)
  {
    //  return false;
    LifeBase otherLife = t.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null && otherLife._data._currentLife <= 0)
      return;
    int finalDamage = (int)(otherLife._data._currentLife * (_data.AttachDamagePercentage * 0.01));
    // Apply for damage
    finalDamage = finalDamage == 0 ? 1 : finalDamage;
    otherLife.TakeDamage(finalDamage, transform.position, _data);
    //Debug.Log("damage: " + finalDamage + " current life: " + otherLife._data._currentLife + " perc: " + _data.AttachDamagePercentage);
  }
}