﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class ArcherNetworkGatherAttackController : NetworkGatherAttackController
{
  public GameObject _trapObj;
  public float _trapSpawnInterval = 0.8f;

  private float m_trapInterval;
  private int m_spawnTotal = 3;
  private GameObject m_spawnObj;

  public override void SpawnObj()
  {
    CheckWeapon();

    for(int i = 0; i < m_spawnTotal; ++i)
    {
      Transform trans = _spawnTransList[i];
      ProcessSpawnObj(trans, m_spawnObj);
    }
  }

  public void Update()
  {
    CheckRangerWeapon();
  }

  private void SpawnTrap()
  {
    // Last one is used for ranger spawn 
    Transform trans = _spawnTransList[_spawnTransList.Count - 1];
    ProcessSpawnObj(trans, _trapObj); 
  }

  private void CheckWeapon()
  {
    UpdateWeaponIndex();
    m_spawnTotal = _spawnTotal[m_currentWeaponIndex];
    m_spawnObj = _weapons[m_currentWeaponIndex];
  }

  private void CheckRangerWeapon()
  {
    if(_data._careerType != Constants.CareerType.Ranger)
      return;
    m_trapInterval -= Time.deltaTime;
    if(m_trapInterval > 0)
      return;
    m_trapInterval = _trapSpawnInterval;
    SpawnTrap();
  }

}