﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmallPlayerHUDController : MonoBehaviour
{
  public RectTransform _rootRectTransform;
  public RectTransform _canvasRect;
  public SharedData _data;
  public Slider _life;
  public Slider _lifeRed;
  public Text _nameText;
  public Vector2 _uiPosition = new Vector2(0,50);

  public float _lifeAnimationDelay = 0.5f;
  public float _lifeAniamtionTime = 0.5f;

  private float m_lastLifeValue;
  private int m_animationID;

  // Threshold to hide the UI
  private Vector2 m_finalPos;
  private Vector2 m_viewportPos;
  private bool m_canProcess = false;

  void OnPlayerNetworkInstantiate()
  {
    _nameText.text = _data._nickname;

    _life.gameObject.SetActive(true);
    _nameText.gameObject.SetActive(true);
    m_canProcess = true;
  }

  void LateUpdate()
  {
    if(!m_canProcess)
      return;
    
    UpdatePos();

    //Debug.Log(_data._root.name + " life: " + _data._currentLife + " max: " + _data.MaxLife);
    _life.value = ((_data._currentLife / (float)_data.MaxLife) * _life.maxValue);
   

    if(m_lastLifeValue != _life.value)
    {
      if(m_lastLifeValue > _life.value)
      {
        LeanTween.cancel(m_animationID);
        m_animationID = LeanTween.value(_lifeRed.value, _life.value, _lifeAniamtionTime).setDelay(_lifeAnimationDelay).setOnUpdate(OnLifeValueUpdate).id;
      }
      else
      {
        _lifeRed.value = _life.value;
      }
    }
    m_lastLifeValue = _life.value;

    if(_data._currentLife <= 0)
    {
      m_canProcess = false;
      _life.gameObject.SetActive(false);
      _nameText.gameObject.SetActive(false);
    }
  }

  private void OnLifeValueUpdate(float value)
  {
    _lifeRed.value = value;
  }

  private void UpdatePos()
  {
    // Update position based on this object agsint the screen
    // Convert ship position into viewport pos
    m_viewportPos = Camera.main.WorldToViewportPoint(_data._root.transform.position);

    // Calculate the offset, because screen has different coordination 
    m_finalPos.x = ((m_viewportPos.x * _canvasRect.sizeDelta.x) - (_canvasRect.sizeDelta.x * 0.5f));
    m_finalPos.y = ((m_viewportPos.y * _canvasRect.sizeDelta.y) - (_canvasRect.sizeDelta.y * 0.5f));
    // Apply final result with offset
    //Debug.Log(_data._root.name + " pos: " + m_finalPos);
    _rootRectTransform.anchoredPosition = (m_finalPos + _uiPosition);

  }
}