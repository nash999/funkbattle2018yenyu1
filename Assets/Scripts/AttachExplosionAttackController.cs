﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachExplosionAttackController : MonoBehaviour
{
  public SharedData _attackData;

  private SharedData m_targetData;

  void OnWeaponInit()
  {
    // Get the data from parent
    m_targetData = transform.root.GetComponentInChildren<SharedData>();
  }

  void Update()
  {
    if(m_targetData)
    {
      m_targetData.ForSpeedRatio = _attackData.ForSpeedRatio;
      m_targetData.RotSpeedRatio = _attackData.RotSpeedRatio;
    }
  }

  void OnDisable()
  {
    // Reset data
    m_targetData = null;
  }
}