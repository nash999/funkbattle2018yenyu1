﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovementController : MovementController
{
  public override void UpdateData()
  {
    if(_data._currentLife <= 0)
    {
      _rigid.velocity = Vector3.zero;
      m_movement.Set(0, 0, 0);
      return;
    }
    //Debug.Log("move: " + transform.forward.x + " " + transform.forward.z);
    if(_data._canMove)
      m_movement.Set(transform.forward.x, 0f, transform.forward.z);
    else
    {
      _rigid.velocity = Vector3.zero;
      m_movement.Set(0, 0, 0);
    }

  }

  public override void DoAnimation()
  {
    //return;
    if(_anim)
    {
      _anim.SetBool (Constants.kIsWalkingKey, true);
    }
  }
}