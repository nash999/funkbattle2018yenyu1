﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkAutoRotationController : AutoRotationController
{
  public PhotonView _view;

  public void OnMasterClientSwitched()
  {
    StopNetworkRotation();
    StartNetworkRotation();
  }

  void OnEnable()
  { 
    StartNetworkRotation();
  }	

  void OnDisable()
  {
    StopNetworkRotation();
  }

  public void StartNetworkRotation()
  {
    if(_view.isMine)
      StartRotation();
  }

  public void StopNetworkRotation()
  {
    if(_view.isMine)
      StartRotation();
  }
}