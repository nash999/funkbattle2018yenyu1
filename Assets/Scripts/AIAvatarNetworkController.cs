﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAvatarNetworkController : AvatarNetworkController
{
  public new void OnPhotonInstantiate(PhotonMessageInfo info)
  {
    UpdateData();
    UpdateDisableList();
    UpdateID();
    UpdateName();
    UpdateLevel();
    UpdateSkill();
    _data._root.BroadcastMessage(Constants.kNetworkInstantiateMS, SendMessageOptions.DontRequireReceiver);
  }

  public override void UpdateName()
  {
    _data._nickname = GameManager.instance.GetRandomName();
  }

  private void UpdateData()
  {
    object[] data = _view.instantiationData;

    if(data != null && data.Length > 0)
    {
      string itemTag = data[0].ToString();
      _data._itemType = itemTag;
    }
  }

  private void UpdateLevel()
  {
    // Randomly assign a level to AI player
    _data._currentPlayerLevel = Random.Range(0, _data._maxLevel);
  }

  private void UpdateSkill()
  {
    int index = 0;
    // Randomly assign different skill level based on level
    for(int i = _data._currentSkillLevel; i < _data._currentPlayerLevel; ++i)
    {
      index = Random.Range(0, 9);
      switch(index)
      {
        case 0:
          _data._currentAttackIntervalLevel = Mathf.Clamp(_data._currentAttackIntervalLevel + 1, 0, 6);
        break;
        case 1:
          _data._currentMaxLifeLevel = Mathf.Clamp(_data._currentMaxLifeLevel + 1, 0, 6);
        break;
        case 2:
          _data._currentWeaponActiveTimeLevel = Mathf.Clamp(_data._currentWeaponActiveTimeLevel + 1, 0, 6);
        break;
        case 3:
          _data._currentWeaponRegularDamageLevel = Mathf.Clamp(_data._currentWeaponRegularDamageLevel + 1, 0, 6);
        break;
        case 4:
          _data._currentWeaponUltimateDamageLevel = Mathf.Clamp(_data._currentWeaponUltimateDamageLevel + 1, 0, 6);
        break;
        case 5:
          _data._currentWeaponSuckBloodLevel = Mathf.Clamp(_data._currentWeaponSuckBloodLevel + 1, 0, 6);
        break;
        case 6:
          _data._currentDefenseLevel = Mathf.Clamp(_data._currentDefenseLevel + 1, 0, 6);
        break;
        case 7:
          _data._currentForwardSpeedLevel = Mathf.Clamp(_data._currentForwardSpeedLevel + 1, 0, 6);
        break;
        case 8:
          _data._currentWeaponBleedingLevel = Mathf.Clamp(_data._currentWeaponBleedingLevel + 1, 0, 6);
        break;
      }

      _data._currentSkillLevel = i;
    }
    _data._root.BroadcastMessage(Constants.kForSpeedRatioUpdateMS, SendMessageOptions.DontRequireReceiver);
    _data._root.BroadcastMessage(Constants.kRotSpeedRatioUpdateMS, SendMessageOptions.DontRequireReceiver);
  }
}
