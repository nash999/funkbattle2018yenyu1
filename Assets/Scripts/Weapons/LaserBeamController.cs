﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(LineRenderer))]
public class LaserBeamController : MonoBehaviour
{ 
  public SharedData _data;
  public int _damage = 50;

  public LayerMask _layerMask;
  public float _beamScale; // Default beam scale to be kept over distance
  public float _maxBeamLength; // Maximum beam length

  public bool _isDebug = false;
  public bool _animateUV; // UV Animation
  public float _UVTime; // UV Animation speed
  //The particle system, in this case sparks which will be created by the Laser
  public GameObject _endEffect;
  public GameObject _startVFX;

  private LineRenderer m_lineRenderer;
  //Cache any transforms here
  private Transform m_transform;
  private Transform m_endEffectTransform;
  float m_beamLength; // Current beam length
  private float m_initialBeamOffset; // Initial UV offset  
  private float m_animateUVTime;

  void Awake()
  {
    m_lineRenderer = GetComponent<LineRenderer>();
    m_transform = transform;
    if(_endEffect)
      m_endEffectTransform = _endEffect.transform;

    // Randomize uv offset
    m_initialBeamOffset = Random.Range(0f, 5f);
  }

  void OnEnable()
  {
    _startVFX.SetActive(true);
    _endEffect.SetActive(true);
  }

  void OnDisable()
  {
    _startVFX.SetActive(false);
    _endEffect.SetActive(false);

  }

  // Update is called once per frame
  void Update()
  {

    AnimateUV();

    RenderLaser();
    DrawRay();
  }
  
  void RenderLaser()
  {
    //Shoot our laserbeam forwards!
    UpdateLength();
  }

  void UpdateLength()
  {
    //Raycast from the location of the cube forwards
    RaycastHit hit;
    // Calculate default beam proportion multiplier based on default scale and maximum length
    float propMult = _maxBeamLength*(_beamScale/10f);

    if(Physics.Raycast(m_transform.position, m_transform.forward, out hit, _maxBeamLength, _layerMask))
    {
      //Debug.Log("Hit: " + hit.collider.name + " pos: " + hit.point);
      // Skip triggers and owner
      if(!hit.collider.isTrigger && hit.collider.gameObject != _data._root.gameObject)
      {
        // Get current beam length and update line renderer accordingly
        m_beamLength = Vector3.Distance(m_transform.position, hit.point);
        m_lineRenderer.SetPosition(1, new Vector3(0f, 0f, m_beamLength));

        // Calculate default beam proportion multiplier based on default scale and current length
        propMult = m_beamLength*(_beamScale/10f);

        //Move our End Effect particle system to the hit point and start playing it
        if(_endEffect)
        {
          m_endEffectTransform.position = hit.point - transform.forward*0.5f;
        }

        // Apply damage
        PlayerLifeController colliderLife = hit.collider.GetComponentInChildren<PlayerLifeController>();
        if(colliderLife != null && colliderLife._data._currentLife > 0)
        {
          colliderLife.TakeDamage(_damage, transform.position, _data);
        }
      }
    }
    else
    {
      // Set beam to maximum length
      m_beamLength = _maxBeamLength;
      m_lineRenderer.SetPosition(1, new Vector3(0f, 0f, m_beamLength));

      // Adjust impact effect position
      if (_endEffect)
        m_endEffectTransform.position = transform.position + transform.forward*m_beamLength;
    }

    // Set beam scaling according to its length
    m_lineRenderer.material.SetTextureScale("_MainTex", new Vector2(propMult, 1f));

  }

  void DrawRay()
  {
    if(_isDebug)
    {
      Vector3 forward = m_transform.TransformDirection(Vector3.forward) * 10;
      Debug.DrawRay(m_transform.position, forward, Color.green);
    }
  }

  private void AnimateUV()
  {
    // Animate texture UV
    if (_animateUV)
    {
      m_animateUVTime += Time.deltaTime;

      if (m_animateUVTime > 1.0f)
        m_animateUVTime = 0f;

      m_lineRenderer.material.SetTextureOffset("_MainTex", new Vector2(m_animateUVTime * _UVTime + m_initialBeamOffset, 0f));
    }
  }
}