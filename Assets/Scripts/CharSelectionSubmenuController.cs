﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSelectionSubmenuController : MonoBehaviour
{
  public GameObject _charDisplayMenu;
  public GameObject _pressStartMenu;
  public GameObject _avatarSelectionMenu;

  public void OnCharDisplayButtonDown()
  {
    _avatarSelectionMenu.SetActive(true);
  }

  public void OnPressStartButtonDown()
  {
    _charDisplayMenu.SetActive(true);
    _pressStartMenu.SetActive(false);
  }
}
