﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class ScoreController : MonoBehaviour
{
  public PhotonView _view;
  public SharedData _data;

  public void AddScore(int score)
  {
    //Debug.Log("Add score: " + score + " to player: " + _data._nickname);
    if(_view)
    {
      _view.RPC("NetworkAddScore", PhotonTargets.All, score);
    }
    else
    {
      NetworkAddScore(score);
    }
  }

  public void NetworkAddScore(int score)
  {
    _data._currentScore += score;
    UpdateScore();
  }

  void OnEnable()
  {
    UpdateScore();
  }

  private void UpdateScore()
  {
    if(_view.isMine)
    {
      Hashtable data = new Hashtable();
      data[Constants.kPlayerScoreKey] = _data._currentScore;
      Messenger.Broadcast(Constants.kScoreUpdateNotification, data);
      PlayerPrefs.SetInt(Constants.kPlayerScoreKey, _data._currentScore);
    }
  }
}