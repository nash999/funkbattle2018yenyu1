﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public static class Helpers
{
  public static string GetTimeInString(int time)
  {
    System.TimeSpan t = System.TimeSpan.FromSeconds(time);
    return string.Format("{0:D1}:{1:D2}", t.Minutes, t.Seconds);
  }

  public static void DestroyChildren(Transform t)
  {
    for (int i = t.childCount - 1; i >= 0; --i)
    {
      //Debug.Log("Destroy: " + t.GetChild(i).gameObject);
      GameObject.Destroy(t.GetChild(i).gameObject);
    }

    t.DetachChildren();
  }

  public static GameObject FindGameObjectInChildWithTag(this GameObject parent, string tag)
  {
     Transform t = parent.transform;
     foreach(Transform tr in t)
     {
        if(tr.tag == tag)
        {
           return tr.gameObject;
        }
     }
     return null;
   }

   public static List<GameObject> FindGameObjectsInChildWithTag(this GameObject parent, string tag)
   {
      List<GameObject> objListWithTag = new List<GameObject>();

      Transform t = parent.transform;
      foreach(Transform tr in t)
      {
        if(tr.tag == tag)
        {
          objListWithTag.Add(tr.gameObject);
        }
      }

      return objListWithTag;
   }

  public static Type GetType( string TypeName )
 {
 
     // Try Type.GetType() first. This will work with types defined
     // by the Mono runtime, in the same assembly as the caller, etc.
     var type = Type.GetType( TypeName );
 
     // If it worked, then we're done here
     if( type != null )
         return type;
 
     // If the TypeName is a full name, then we can try loading the defining assembly directly
     if( TypeName.Contains( "." ) )
     {
 
         // Get the name of the assembly (Assumption is that we are using 
         // fully-qualified type names)
         var assemblyName = TypeName.Substring( 0, TypeName.IndexOf( '.' ) );
 
         // Attempt to load the indicated Assembly
         var assembly = Assembly.Load( assemblyName );
         if( assembly == null )
             return null;
 
         // Ask that assembly to return the proper Type
         type = assembly.GetType( TypeName );
         if( type != null )
             return type;
 
     }
 
     // If we still haven't found the proper type, we can enumerate all of the 
     // loaded assemblies and see if any of them define the type
     var currentAssembly = Assembly.GetExecutingAssembly();
     var referencedAssemblies = currentAssembly.GetReferencedAssemblies();
     foreach( var assemblyName in referencedAssemblies )
     {
 
         // Load the referenced assembly
         var assembly = Assembly.Load( assemblyName );
         if( assembly != null )
         {
             // See if that assembly defines the named type
             type = assembly.GetType( TypeName );
             if( type != null )
                 return type;
         }
     }
 
     // The type just couldn't be found...
     Debug.LogError("Cannot find this type: " + TypeName);
     return null;

  }

  public static Quaternion ConvertRotation(float h, float v)
  {
    //Debug.Log("hv: " + h + " : " + v);
    float angle = Mathf.Atan2(h, v) * Mathf.Rad2Deg;
    Quaternion q = Quaternion.Euler(0, angle, 0);

    return q;

  }

  /*
  public static bool IsLocal(PhotonView view)
  {
    if(view == null)
      return false;
    return view.isMine;
  }
  */
}
