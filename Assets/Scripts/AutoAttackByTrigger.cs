﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttackByTrigger : AutoAttackBase
{
  private float m_attackInterval;

  void OnTriggerStay(Collider other)
  {
    if(UpdateStep())
      CheckAttack(other.gameObject);
  }
}