﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovementFollow : AIMovementBase
{
  private GameObject m_player;        
  //private string m_playerDeathAnimString = "PlayerIsDead";
  private float m_invokeRepeatTime = 1.0f;

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGameOverNotification, OnGameOver);
    OnForSpeedRatioUpdate();
    OnRotSpeedRatioUpdate();
    InvokeRepeating("FindPlayer", 0, m_invokeRepeatTime);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGameOverNotification, OnGameOver);
    CancelInvoke("FindPlayer");
  }

  void Update()
  {
    if(GameManager.instance._gameState != Constants.GameState.GamePlay)
      return;
    if(m_player == null)
      return;

    if(_nav.enabled == false)
      return;
    // If this player have health left...
    if(_aiData._currentLife > 0)
    {
      // ... set the destination of the nav mesh agent to the player.
      _nav.SetDestination(_destination);
    }
    // Otherwise...
    else
    {
      // ... disable the nav mesh agent.
      _nav.enabled = false;
    }
  }

  private void OnGameOver()
  {
    //Debug.Log("OnGameOver");
    m_player = null;
    //m_playerData = null;
    _nav.enabled = false;
    _anim.SetBool(Constants.kIsWalkingKey, false);
  }

  private void FindPlayer()
  {
    m_player = GameManager.instance.FindClosestPlayer(gameObject);
    if(m_player == null)
    {
      OnGameOver();
    }
    else
    {
      // Make sure this player can still be followed
      SharedData enemyData = m_player.GetComponentInChildren<SharedData>();
      if(enemyData && enemyData._currentLife > 0)
      {
        // Update animation
        _anim.SetBool(Constants.kIsWalkingKey, true);
        // Set the destination
        _destination = m_player.transform.position;
        // Enable nav so it can start following
        _nav.enabled = true;
      }
      else
      {
        OnGameOver();
      }

    }
  }
}