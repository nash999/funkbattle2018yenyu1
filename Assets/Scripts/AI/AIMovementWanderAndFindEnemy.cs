﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using JetBrains.Annotations;

public class AIMovementWanderAndFindEnemy : AIMovementBase
{
  // Make sure the destination is on the ground
  public float _defaultY = 0f;
  // Mask for checking destination
  public LayerMask _moveMask;
  public LayerMask _obstacleMask;
  public LayerMask _enemyMask;
  public List<string> _tagsToCollide;
  // Sphere for random destination 
  public float _destinationSphereDistance = 100f;
  // Sphere for enemy detection
  public float _enemyDetectSphereDistance = 50.0f;

  //private Vector3 m_lastDest;
  private Vector3 m_tempDest;
  private string m_invokeString = "ProcessLogic";
  private float m_invokeInterval = 0.5f;
  private GameObject m_destObj;

  public void SetDestination(Vector3 dest, bool checkComplete = true)
  {
    if(!_nav.enabled)
      return;
    // Validate destinamtion
    if(dest.x == Mathf.Infinity || dest.y == Mathf.Infinity || dest.z == Mathf.Infinity)
      return;
    // Default y
    dest.y = _defaultY;

    // Reach the destinamtion yet?
    if(checkComplete)
    {
      float dist = _nav.remainingDistance;
      if(dist != Mathf.Infinity && dist <= _nav.stoppingDistance)
      {
        // Arrive, set next destination
        _destination = dest;
      }
    }
    else
    {
      // Update destination regardless last destination has reached or not
      _destination = dest;

    }

    //Debug.Log("Set diestination: " + _destination);
    //m_lastDest = _destination;
    _nav.SetDestination(_destination);
    _anim.SetBool(Constants.kIsWalkingKey, true);
    
  }

  void OnEnable()
  {
    InitAgent();
    InvokeRepeating(m_invokeString, 0, m_invokeInterval);
  }

  void OnDisable()
  {
    CancelInvoke(m_invokeString);
  }

  private void ProcessLogic()
  {
    if(!_aiData._canMove)
      return;
    if(!_aiData._canRotate)
      return;
    // Enemy comes first
    FindAnyEnemy();
    //Debug.Log("Process logic");
    // If cannot find enemy, we randomly pick a destination to wander
    //Debug.Log("dest: " + m_destObj.name);
    if(m_destObj == null)
    {
      FindRandomDest();
    }

  }

  private void InitAgent()
  {
    // NavAgent has its own speed & angular speed
    // Make sure they match the data values
    OnForSpeedRatioUpdate();
    OnRotSpeedRatioUpdate();

    // Destination is self
    _destination = transform.position;
    // On the ground
    _destination.y = _defaultY;
  }
 
  /// <summary>
  ///  Get a random position within distance
  /// </summary>
  /// <returns>The nav sphere.</returns>
  /// <param name="origin">Origin.</param>
  /// <param name="distance">Distance.</param>
  /// <param name="layermask">Layermask.</param>
  private Vector3 RandomNavSphere(Vector3 origin, float distance)
  {
    Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * distance;
           
    randomDirection += origin;
  
    NavMeshHit navHit;
           
    NavMesh.SamplePosition(randomDirection, out navHit, distance, NavMesh.AllAreas);

    return navHit.position;
  }

  private void FindRandomDest()
  {
    SetDestination(RandomNavSphere(transform.position, _destinationSphereDistance));
  }

  private void FindAnyEnemy()
  {
    // Find all enemies with proper layer and tag 
    Collider[] objs = Physics.OverlapSphere(transform.position, _enemyDetectSphereDistance, _enemyMask);
    foreach(Collider c in objs)
    {
      // Block by obsticle?
      if(IsBlocked(c.gameObject))
        continue;
      // Make sure they have life 
      LifeBase l = c.transform.root.GetComponentInChildren<LifeBase>();
      if(l == null)
        continue;
      if(l._data._currentLife <= 0)
        continue;
      // Make sure not self
      if(l.transform.root.gameObject == transform.root.gameObject)
        continue;
      foreach(string t in _tagsToCollide)
      {
        if(t == c.tag)
        {
          m_destObj = c.gameObject;
          SetDestination(m_destObj.transform.position, false);
          return;
        }
      }
    }
    // Cannot find any enemies
    m_destObj = null;
  }

  private bool IsBlocked(GameObject obj)
  {
    // Visible?
    RaycastHit hit;
    if (Physics.Linecast(transform.position, obj.transform.position, out hit, _obstacleMask))
    {
      //Debug.Log(obj.name + " Hit something: " + hit.collider.name);
      return true;
    }
    return false;

  }
}