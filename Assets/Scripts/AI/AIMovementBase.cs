﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovementBase : MonoBehaviour
{
  public SharedData _aiData;        
  public UnityEngine.AI.NavMeshAgent _nav;
  public Vector3 _destination;
  public Animator _anim;  

  public void OnForSpeedRatioUpdate()
  {
    _nav.speed = _aiData.ForSpeed;
    _anim.SetFloat(Constants.kMoveMultiplierKey, _aiData.ForSpeed * _aiData._moveAnimatorRatio);
  }

  public void OnRotSpeedRatioUpdate()
  {
    _nav.angularSpeed = _aiData.RotSpeed;
  }
}
