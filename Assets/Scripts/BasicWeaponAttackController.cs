﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class BasicWeaponAttackController : MonoBehaviour
{
  public GameObject _root;
  public GameObject _explosion;
  public GameObject _sfx;
  public WeaponData _data;
  public bool _attachExplosion = false;
  // Will skip those layers by counter
  public LayerMask _layerToSkip;
  // Won't pass those layers at all 
  public LayerMask _layerToBlock;
  public int _skipLayerCounter = 0;
  //public bool _attachOnlyOnce = false;

  private int m_skipCounter;

  public virtual void OnEnable()
  {
    _data._currentLife = _data.MaxLife;
    m_skipCounter = _skipLayerCounter;
  }

  void OnTriggerEnter(Collider other)
  {
    //Debug.Log(_root.name +  " trigger entered: " + other.name);
    // Reference check
    if(_data._ownerNetworkID == null)
      return;
    if(ShouldBlockLayer(other))
      return;
    CheckSkipLayer(other);
    //if(CheckSkipLayer(other))
    //  return;
    //ApplyDamage(other);
    //Debug.Log(_root.name +  " pass all layers: " + other.name);
    if(!ApplyDamage(other.gameObject))
      return;
    //Debug.Log(_root.name +  " damage applied: " + other.name);
    // TODO: Move it to coroutine
    StartCoroutine(ApplyExplosion(other));
    if(_data._currentLife <= 0)
    {
      // Show explosion
      DoDespawn();
    }
  }

  public virtual IEnumerator ApplyExplosion(Collider other)
  {
    yield return null;

    if(_explosion)
    {
    
      GameObject spawnedObj = null;

      if(_attachExplosion)
      {
        spawnedObj = LeanPool.Spawn(_explosion, Constants.kVector3Zero, Constants.kQuaternionIdentity, other.transform);
      }
      else
      {
        spawnedObj = LeanPool.Spawn(_explosion, transform.position, Constants.kQuaternionIdentity);
      }

      if(spawnedObj)
      {
        // Assign the team to spawned obj
        SharedData spawnedObjData = spawnedObj.GetComponentInChildren<SharedData>();
        if(spawnedObjData)
        {
          spawnedObjData._team = _data._team;
          spawnedObjData._expController = _data._expController;
          spawnedObjData._scoreController = _data._scoreController;
          spawnedObjData._currentWeaponSuckBloodLevel = _data._currentWeaponSuckBloodLevel;
          spawnedObjData._currentWeaponBleedingLevel = _data._currentWeaponBleedingLevel;
          spawnedObjData._currentWeaponUltimateDamageLevel = _data._currentWeaponUltimateDamageLevel;
          spawnedObjData._currentWeaponActiveTimeLevel = _data._currentWeaponActiveTimeLevel;
          spawnedObjData._currentWeaponRegularDamageLevel = _data._currentWeaponRegularDamageLevel;
          spawnedObjData._currentWeaponDamageForceLevel = _data._currentWeaponDamageForceLevel;
        }
        // Send a msg to spawned obj so they can start processing 
        spawnedObj.BroadcastMessage(Constants.kInitWeaponMS, SendMessageOptions.DontRequireReceiver);
      }
    }
    
  }

  public virtual bool ShouldBlockLayer(Collider other)
  {
    if((_layerToBlock.value == (_layerToBlock.value | (1 << other.gameObject.layer))))
    {
      // Instant kill
      ApplyExplosion(other);
      _data._currentLife = 0;
      DoDespawn();
      return true;
    }
    return false;
  }

  public virtual void CheckSkipLayer(Collider other)
  {
    // Check if other collider's layer is within the mask to skip
    if((_layerToSkip.value == (_layerToSkip.value | (1 << other.gameObject.layer))))
    {
      // Make sure we are not hitting ourselves
      LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
      if(otherLife && otherLife._data._networkID == _data._ownerNetworkID)
      {
        // Hitting ourselves
        // Do nothing and return
        return;
      }
      // Check life
      if(otherLife && otherLife._data._currentLife <= 0)
      {
        // Enemy has no life
        return;
      }
      if(m_skipCounter <= 0)
      {
        // No more skip, zero the life so it can be destroyed
        _data._currentLife = 0;
      }
      else
      {
        // Skip and add explosion
        m_skipCounter--;
      }
    }

  }

  public virtual bool ApplyDamage(GameObject other)
  {
    // Check tag
    if(other.tag != Constants.kPlayerTag && other.tag != Constants.kEnemyTag)
      return false;
    LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return false;
    // Skip owner 
    //Debug.Log(_data._ownerNetworkID + " name: " + _data._root.name);
    if(otherLife._data._networkID == _data._ownerNetworkID)
      return false;
    // Make sure they have life
    if(otherLife._data._currentLife <= 0)
      return false;
    // Apply for damage
    //Debug.Log("damege: " + _data.WeaponDamage);
    otherLife.TakeDamage(_data.WeaponDamage, transform.position, _data);
    ApplyForce(other);
    return true;
  }

  public virtual void ApplyForce(GameObject other)
  {
    // Can we add force by random percentage?
    if(Random.Range(0, 99) < _data.KnockBackPercentage)
    {
      //Debug.Log("Add knockback force");
      LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
      if(otherLife == null)
        return;
      otherLife.ApplyForce((other.transform.position - transform.position) * _data.DamageForce);

    }
  }

  public virtual void DoDespawn()
  {
    // Kill this obj
    LeanPool.Despawn(_root);
  }

  public void DoSFX()
  {
    if(_sfx)
    {
      //Debug.Log(transform.root.name +  " spawn sfx");
      GameObject spawnedObj = LeanPool.Spawn(_sfx, transform.position, Constants.kQuaternionIdentity);

      // Assign the team to spawned obj
      SharedData spawnedObjData = spawnedObj.GetComponentInChildren<SharedData>();
      if(spawnedObjData)
      {
        spawnedObjData._team = _data._team;
        spawnedObjData._expController = _data._expController;
        spawnedObjData._scoreController = _data._scoreController;

      }
    }
  }
}