﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTurningInputController : TurningInputBase
{
  public override void CheckTurning()
  {
    base.CheckTurning();

    // Rotate the obj 
    if(_data._moveDir.x != 0 || _data._moveDir.y != 0)
    {
      _data._rot = Helpers.ConvertRotation(_data._moveDir.x, _data._moveDir.y);
    }
  }
}