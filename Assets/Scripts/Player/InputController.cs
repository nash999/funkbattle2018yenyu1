﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityStandardAssets.CrossPlatformInput;
using System;
using UnitySampleAssets.CrossPlatformInput;


public class InputController : MonoBehaviour
{
  public string _horizontal = "Horizontal";
  public string _vertical = "Vertical";
  public string _attackKey = "Fire1";
  public string _defaultAttackKey = "Fire1";

  public SharedData _data;
  public ActionBase _attack;
  public Constants.TeamSide _team;

  public bool _canTakeInput = true;

  private Vector3 m_joystickPos;


  void Update()
  {
    if(!CanTakeInput())
      return;

    CheckMovement();
    CheckAttack();
  }

  private void CheckMovement()
  {
    _data._moveDir.x = CrossPlatformInputManager.GetAxisRaw(_horizontal);
    _data._moveDir.y = CrossPlatformInputManager.GetAxisRaw(_vertical);
  }

  private void CheckAttack()
  {
    // If the Fire1 button is being press and it's time to fire...
    if(Input.GetKeyDown(_attackKey) || Input.GetButtonDown(_defaultAttackKey))
    {
      _attack.ProcessDown();
    }
    else if(Input.GetKey(_attackKey) || Input.GetButton(_defaultAttackKey))
    {
      _attack.ProcessPress();
    }
    else if(Input.GetKeyUp(_attackKey) || Input.GetButtonUp(_defaultAttackKey))
    {
      _attack.ProcessUp();
    }
    else
      _attack.ProcessUp();

  }

  private bool CanTakeInput()
  {
    if(_data != null && _data._currentLife <= 0)
      return false;
    // Disable if not the same team
    if(_data._team != _team)
    {
      _canTakeInput = false;
      //gameObject.SetActive(false);
    }
    else
      _canTakeInput = true;
    return _canTakeInput;
  }
}