﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class MovementController : MonoBehaviour
{
  public SharedData _data;
  public Rigidbody _rigid;  
  public Animator _anim;


  // The vector to store the direction of the player's movement.
  protected Vector3 m_movement;        

  public virtual void Move()
  {
    // Move the player to it's current position plus the movement.
    // Normalise the movement vector and make it proportional to the speed per second.
    m_movement = m_movement.normalized;
    //Debug.Log("normalized: " + m_movement);
    //Debug.Log("m: " + m_movement);
    transform.root.position = Vector3.Lerp(transform.root.position, transform.root.position + m_movement, _data.ForSpeed * Time.deltaTime);
    //Debug.Log("pos: " + transform.root.position);
    //Debug.Log("time: " + _data.ForSpeed);
    //_rigid.MovePosition(transform.position + m_movement);
  }

  public virtual void UpdateData()
  {
    m_movement.Set(_data._moveDir.x, 0f, _data._moveDir.y);
  }

  public virtual void DoAnimation()
  {
    if(_anim)
    {
      bool walking = _data._moveDir.x != 0 || _data._moveDir.y != 0;
      //Debug.Log("walking state: " + walking + " for GO: " + transform.root.name);
      _anim.SetBool (Constants.kIsWalkingKey, walking);
    }
  }

  void OnEnable()
  {
    // Synchronize the movement speed with animation
    if(_anim == null)
      return;
    if(_data == null)
      return;
    _anim.SetFloat(Constants.kMoveMultiplierKey, _data.ForSpeed * _data._moveAnimatorRatio);   
  }

  void Update()
  {
    // Move the player around the scene.
    UpdateData();
    Move();
    DoAnimation();
  }
}