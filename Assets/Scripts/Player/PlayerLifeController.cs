﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Lean;

public class PlayerLifeController : LifeBase
{
  public GameObject _root;
  public AudioClip _hurtClip;
  public AudioClip _deathClip;                                 // The audio clip to play when the player dies.
  public Animator _anim;
  public AudioSource _playerAudio;  
  public float _timeToDespawn = 3.0f;
  public bool _canTakeDamage = true;

  protected bool m_isDead;                                                // Whether the player is dead.

  protected string m_invokeString = "Despawn";

  void OnEnable()
  {
    //Debug.Log("Reset life for char");
    _playerAudio.clip = _hurtClip;
    _data._currentLife = _data.MaxLife;
    _anim.ResetTrigger("Die");
    m_isDead = false;
  }

  void OnDiable()
  {
    CancelInvoke(m_invokeString);
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, SharedData data)
  {
    if(m_isDead)
      return;
    if(!_canTakeDamage)
      return;

    TakeDamage(amount, hitPoint);

    AddLifeToAttacker(data);
    // If the player has lost all it's health and the death flag hasn't been set yet...
    if(_data._currentLife <= 0)
    { 
      AddScoreToAttacker(data);
      // ... it should die.
      Death();
    }
    else
    {
      
      ApplySlowSpeedByAttacker(data);
    }
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, bool playSound = true)
  {
    int damageAfterDefense = amount;
    if(amount > 0)
      damageAfterDefense = ApplyDamage(amount);
    // Play the hurt sound effect
    // Negative damage means it recovers life
    // So we don't play hurt sfx here
    if(damageAfterDefense > 0 && playSound)
      _playerAudio.Play ();
  }

  private void Death()
  {
    // Set the death flag so this function won't be called again.
    m_isDead = true;

    // Tell the animator that the player is dead.
    _anim.SetTrigger ("Die");

    // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
    _playerAudio.clip = _deathClip;
    _playerAudio.Play ();

    // Send out death notification 
    Hashtable data = new Hashtable();
    data[Constants.kCharacterKey] = _root;
    data[Constants.kCharacterPosKey] = _root.transform.position;
    data[Constants.kCharacterDataKey] = _data.DeathEXP;
    Messenger.Broadcast(Constants.kCharacterDeathNotification, data);
    _rigid.angularVelocity = Constants.kVector3Zero;
    _rigid.velocity = Constants.kVector3Zero;
    // Reset movement data
    _data._moveDir.Set(0,0);
    // Call despawn after time so animation can still be visible
    Invoke(m_invokeString, _timeToDespawn);
  }

  private void Despawn()
  {
    LeanPool.Despawn(_root);
  }
}