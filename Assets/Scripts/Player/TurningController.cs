﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurningController : MonoBehaviour
{
  public SharedData _data; 
  public Rigidbody _rigid;
  public GameObject _body;

  void Start()
  {
    _data._rot = Constants.kQuaternionIdentity;
  }

  void Update()
  {
    if(!_data)
      return;
    if(!_rigid)
      return;

    //Debug.Log("turning: " + _data._rot + " speed: " + _data.RotSpeed);
    if(_data._canRotate)
    {
      _rigid.rotation = Quaternion.Lerp(_rigid.rotation, _data._rot, Time.deltaTime * _data.RotSpeed);
      //_rigid.rotation = _data._rot;
    }
    if(_body)
    {
      if(_data._attackDir.x != 0 || _data._attackDir.y != 0)
      {
        //Debug.Log("moveDir: " + _data._moveDir + " attackDir: " + _data._attackDir);
       //Quaternion moveRot = Helpers.ConvertRotation(_data._moveDir.x, _data._moveDir.y);
       //Quaternion attackRot = Helpers.ConvertRotation(_data._attackDir.x, _data._attackDir.y);
        //rot = Helpers.ConvertRotation(_data._attackDir.x, _data._attackDir.y);
        //Vector2 final = _data._moveDir - _data._attackDir;
        //Quaternion finalR = Helpers.ConvertRotation(final.x, final.y);
        //Quaternion finalR = (attackRot * Quaternion.Inverse(moveRot));
        //Debug.Log("FinalDir: " + finalR.eulerAngles);

        //Debug.Log("moveRot: " + moveRot.eulerAngles + " attackRot: " + attackRot.eulerAngles + " final rot " + (finalR).eulerAngles + " final2 rot " + finalRot.eulerAngles);
        //_body.transform.localRotation = finalR;

        _body.transform.rotation = Quaternion.LookRotation(new Vector3(_data._attackDir.x, 0, _data._attackDir.y));
      }
      else
        _body.transform.localRotation = Helpers.ConvertRotation
          (
            _data._attackDir.x, 
            _data._attackDir.y
          );
    }
  }
}
