﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

public class JoystickTurningInputController : TurningInputBase
{
  public string _horitontalKey = "RightJoystickHorizontal";
  public string _verticalKey = "RightJoystickVertical";

  private Vector2 m_joystickPos;

  public override void CheckTurning()
  {
    base.CheckTurning();

    m_joystickPos.x = CrossPlatformInputManager.GetAxisRaw(_horitontalKey);
    m_joystickPos.y = CrossPlatformInputManager.GetAxisRaw(_verticalKey);

    // If any axis has movement, we use joystick to control the turning instead
    if(m_joystickPos.x != 0.0f || m_joystickPos.y != 0.0f)
    {
      Quaternion destRot = Helpers.ConvertRotation(m_joystickPos.x,m_joystickPos.y);
      // Smoothly rotate towards the target point.
      _data._rot = destRot;

    }

    //Debug.Log("joystic turning: " + _data._rot);

  }

}
