﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleAreaWeaponAttackController : BasicWeaponAttackController
{
  public int _attackRadiusInDegree = 60;

  public override void OnEnable()
  {
    base.OnEnable();
  }

  void OnTriggerEnter(Collider other)
  {
    // Check tag
    if(other.tag != Constants.kPlayerTag && other.tag != Constants.kEnemyTag)
      return;
    // Check the angle
    Vector3 targetDir = other.transform.position - _root.transform.position;
    float angle = Vector3.Angle(targetDir, _root.transform.forward);

    if(angle > _attackRadiusInDegree)
      return;

    //Debug.Log("Attack: " + other.name);
    if(!ApplyDamage(other.gameObject))
    {
      return;
    }
    //Debug.Log("Apply explosion to the enemy: " + other.name);
    ApplyExplosion(other);
  }
}