﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class DespawnByTimer : MonoBehaviour
{
  public float _time = 1.0f;

  void OnEnable()
  {
    Invoke("KillMe", _time);
  }

  private void KillMe()
  {
    LeanPool.Despawn(gameObject, 0);
  }
}