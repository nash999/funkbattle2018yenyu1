﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EXPController : MonoBehaviour
{
  public PhotonView _view;
  public SharedData _data;
  public LevelExpData _exp;
  public AvatarSkillUIController _ui;
  public bool _saveEXPToDisk = true;

  public void AddExp(int exp)
  {
    // Accumulate the exp
    _data._currentEXP += exp;
    // Clamp the exp with max exp
    _data._currentEXP = Mathf.Clamp(_data._currentEXP, 0, _exp._exps[_data._maxLevel - 1]);

    //int oldLevel = _data._currentPlayerLevel;

    int tempLevel = GetLevelByExp(_data._currentEXP);
    // Send out notification if player level up
    if(_data._currentPlayerLevel != tempLevel)
    {
      _data._root.BroadcastMessage(Constants.kLevelUpMS, SendMessageOptions.DontRequireReceiver);
    }
    _data._currentPlayerLevel = GetLevelByExp(_data._currentEXP);

    // Save the exp
    if(_saveEXPToDisk)
      PlayerPrefs.SetInt(Constants.kPlayerExpKey, _data._currentEXP);

    // Call the UI to process the level
    if(_ui)
      _ui.CheckLevel();
  }

  public int GetLevelByExp(int exp)
  {
    int level = 0;

    // Can we level up?
    for(int i = 0; i < _exp._exps.Count; ++i)
    {
      //Debug.Log("compare: " + exp + " against: " + _exp._exps[i]);
      if(exp < _exp._exps[i])
      {
        //Debug.Log("Break: " + level);
        break;
      }
      level = i;
    }

    return Mathf.Clamp(level, 0, _data._maxLevel-1);
  }

  public int NextLevelExpBase()
  {
    int lvl = Mathf.Clamp(_data._currentPlayerLevel + 1, 0, _data._maxLevel-1);
    return _exp._exps[lvl];
  }

  public int CurrentLevelExpBase()
  {
    int lvl = Mathf.Clamp(_data._currentPlayerLevel, 0, _data._maxLevel-1);
    return _exp._exps[lvl];
  }

  void OnEnable()
  {
    // Get exp from saved data 
    // and cauclate the level
    if(_saveEXPToDisk)
    {
      int exp = PlayerPrefs.GetInt(Constants.kPlayerExpKey, 0);
      AddExp(exp);
    }
  }

}