﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Werewolf.StatusIndicators.Components;

public class MageNetworkGatherAttackController : NetworkGatherAttackController
{
  private int m_spawnTotal = 3;
  private GameObject m_spawnObj;

  public override void SpawnObj()
  {
    CheckWeapon();

    for(int i = 0; i < m_spawnTotal; ++i)
    {
      Transform trans = _spawnTransList[i];
      ProcessSpawnObj(trans, m_spawnObj);
    }
  }

  private void CheckWeapon()
  {
    UpdateWeaponIndex();

    m_spawnTotal = _spawnTotal[m_currentWeaponIndex];
    m_spawnObj = _weapons[m_currentWeaponIndex];
  }
}