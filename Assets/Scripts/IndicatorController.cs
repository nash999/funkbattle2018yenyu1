﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Werewolf.StatusIndicators.Components;

public class IndicatorController : MonoBehaviour
{
  public PhotonView _view;
  public GameObject _indicator;
 
  void OnEnable()
  {
    _indicator.SetActive(_view.isMine);
    if(_view.isMine)
    {
      // Projector may not be updated properly so some won't display, so we need to manually 
      // call to update the value once
      Cone indicator = _indicator.GetComponentInChildren<Cone>();
      indicator.OnShow();
    }
  }

  void OnDisable()
  {
    _indicator.SetActive(false);
  }
}