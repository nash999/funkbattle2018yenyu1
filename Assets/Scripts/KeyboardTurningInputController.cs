﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardTurningInputController : TurningInputBase
{
  private Vector3 m_direction = Constants.kVector3Zero;

  public override void CheckTurning()
  {
    base.CheckTurning();

    if(!_data._canRotate)
      return;

    if(_data._moveDir.x == 0 && _data._moveDir.y == 0)
      return;

    m_direction.x = _data._moveDir.x;
    m_direction.z = _data._moveDir.y;

    // Get the point along the ray that hits the calculated distance.
    Vector3 targetPoint = transform.position + m_direction;
    // Determine the target rotation.  This is the rotation if the transform looks at the target point.
    Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
    _data._rot = targetRotation;
   
  }  
}