﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OffScreenIndicator : MonoBehaviour
{
  public SharedData _data;
  public PhotonView _view;
  public GameObject _root;
  public GameObject _ui;
  public RectTransform _uiTransform;
  public Text _nameText;
  public Vector2 _imageOffset = new Vector2(15,15);

  private Vector3 m_screenPoint;
  private bool m_onScreen = false;
  private Vector3 m_tempPos;
  //private Vector3 m_heading;

  void OnDisable()
  {
    _ui.SetActive(false);
  }

  void OnPlayerNetworkInstantiate()
  {
    _nameText.text = _data._nickname;
  }

  void LateUpdate()
  {
    if(_root == null)
      return;
    //if(!m_shouldProcess)
    //  return;
    m_tempPos = _root.transform.position;

    m_screenPoint = Camera.main.WorldToScreenPoint(m_tempPos);
    //m_heading = m_tempPos - Camera.main.transform.position;

    m_onScreen = m_screenPoint.x > 0 && m_screenPoint.x < Screen.width && m_screenPoint.y > 0 && m_screenPoint.y < Screen.height;

    // If this object is visible, hide the UI
    _ui.SetActive(!m_onScreen);

    // If this object is off the screen, show and calcualte the ui
    if(!m_onScreen)
    {
      //if(Vector3.Dot(Camera.main.transform.forward, m_heading) < 0)
      {
        // Object is behind
        //Debug.Log("brhind");
        //m_screenPoint.y = (Screen.height - m_screenPoint.y);
        //m_screenPoint.x = (Screen.width - m_screenPoint.x);
      }

      // UI position
      m_screenPoint.x = Mathf.Clamp(m_screenPoint.x, 0, Screen.width - (_imageOffset.x * 2)) + _imageOffset.x;
      m_screenPoint.y = Mathf.Clamp(m_screenPoint.y, 0, Screen.height - (_imageOffset.y * 2)) + _imageOffset.y;
      m_screenPoint.z = 0;

      _uiTransform.position = m_screenPoint;
    }
  }
}