﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttackByOverlapSphere : AutoAttackBase
{
  public LayerMask _attackMask;
  public float _attackRadius = 12;

  void Update()
  {
    if(!_attack._data._canAttack)
      return;
    if(UpdateStep())
    {
      // Skip if player has no life
      if(_attack._data._currentLife <= 0)
        return;
      // Skip if player is gathering ultimate attack
      if(_attack._data._inputKey == Constants.KeyType.Press)
        return;
      // Skip if player is controlling the movement
      if(_skipForManualMovement && (_attack._data._moveDir.x != 0 || _attack._data._moveDir.y != 0))
        return;

      Collider[] objs = Physics.OverlapSphere(transform.position, _attackRadius, _attackMask, QueryTriggerInteraction.Collide);
      foreach(Collider c in objs)
        CheckAttack(c.gameObject);
    }
  }
}