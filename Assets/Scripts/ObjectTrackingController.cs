﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTrackingController : MonoBehaviour
{
  public GameObject _root;
  //public GameObject _target;
  public LifeBase _targetLife;
  public WeaponData _data;
  public float _checkTargetRadius = 2;
  public LayerMask _mask;

  private float m_dis;
  private float m_size;

  void LateUpdate()
  {
    // Always move forward
    MoveForward();
    // Find a target
    // If there is a target
    // Check if it is still within radius
    if(_targetLife == null)
    {
      FindTarget();
    }
    else
    {
      // Colliding obj is not the root obj, so the distance will be slightly off, hardcode a value to fix it 
      if(Vector3.Distance(_root.transform.position, _targetLife.transform.position) > _checkTargetRadius ||
         _targetLife._data._currentLife <= 0)
      {
        //Debug.Log(_target.name + " too far: " + Vector3.Distance(_root.transform.position, _target.transform.position));
        _targetLife = null;
      }
      else
      {
        // There is a target within distance
        // Change direction
        Vector3 targetPos = _targetLife.transform.position;
        Quaternion direction = Quaternion.LookRotation(targetPos - _root.transform.position);
        _root.transform.localRotation = Quaternion.Slerp(_root.transform.localRotation, direction, Time.deltaTime * _data.RotSpeed);
      }
    }

  }

  private void FindTarget()
  {
    
    Collider[] list = Physics.OverlapSphere(_root.transform.position, _checkTargetRadius, _mask, QueryTriggerInteraction.Collide);
    //Debug.Log("Find target: " + list.Length);
    if(list.Length == 0)
      return;
    //Debug.Log("length: " + list.Length);
    for(int i = 0; i < list.Length; ++i)
    {
      GameObject go = list[i].gameObject;
      //Debug.Log("check against: " + go);
      LifeBase life = go.transform.root.GetComponentInChildren<LifeBase>();
      if(life == null)
      {
        //Debug.Log("no life script");
        continue;
      }
      if(life._data._currentLife <= 0)
      {
        //Debug.Log("no life");
        continue;
      }
      // Skip owner
      if(life._data._networkID == _data._ownerNetworkID)
      {
        //Debug.Log("self");
        continue;
      }
      //Debug.Log("match target " + go);
      _targetLife = life;
      break;
    }
  }

  private void MoveForward()
  {
    _root.transform.Translate(Vector3.forward * Time.deltaTime * _data.ForSpeed);
  }
}