﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerData : SharedData
{
  public Constants.CareerType _careerType;

  public List<int> _randomFastAttack;
  public List<float> _magnetLevel;
  public List<float> _levelupLifeRecoverRatio;

  public int _currentFastAttackLevel;
  public int _currentMagnetLevel;
  public int _currentLifeRecoverRatioLevel;

  public int FastAttackPercentage
  {
    get
    {
      _currentFastAttackLevel = Mathf.Clamp(_currentFastAttackLevel, 0, _randomFastAttack.Count-1);
      return _randomFastAttack.Count == 0 ? 0 : _randomFastAttack[_currentFastAttackLevel];
    }
  }

  public float MagnetForce
  {
    get
    {
      _currentMagnetLevel = Mathf.Clamp(_currentMagnetLevel, 0, _magnetLevel.Count-1);
      return _magnetLevel.Count == 0 ? 0 : _magnetLevel[_currentMagnetLevel];
    }
  }
}