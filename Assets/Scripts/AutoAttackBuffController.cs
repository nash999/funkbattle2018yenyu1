﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttackBuffController : MonoBehaviour
{
  public JoystickInputController _input;
  public AvatarAnimatorController _animator;
  public float _animatedTime = 0.1f;

  private string m_invokeString = "ProcessLogic";

  private float m_oldAnimatedTime;

  void OnEnable()
  {
    // Get the data from host
    // Get the root obj
    // Invoke with a bit delay so the logic can be processed 
    // after it has been attached to parent
    Invoke(m_invokeString, 0.5f);
  }

  void OnDisable()
  {
    CancelInvoke(m_invokeString);
    if(_input == null)
      return;
    _input._attack._data.AttackInterval = m_oldAnimatedTime;
    _input = null;
    if(_animator == null)
      return;
    _animator.UpdateAnimator();
  }

  void Update()
  {
    if(_input == null)
      return;
    _input._attack.ProcessDown();
    _input._attack.ProcessUp();
  }

  private void ProcessLogic()
  {
    _input = transform.root.GetComponentInChildren<JoystickInputController>();
    _animator = transform.root.GetComponentInChildren<AvatarAnimatorController>();
    if(_input == null)
      return;
    m_oldAnimatedTime = _input._attack._data.AttackInterval;
    _input._attack._data.AttackInterval = _animatedTime;
    if(_animator == null)
      return;
    _animator.UpdateAnimator();

  }
}