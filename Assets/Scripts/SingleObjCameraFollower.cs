﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class SingleObjCameraFollower : MonoBehaviour
{
  // The position that that camera will be following.
  public GameObject _target;
  // The speed with which the camera will be following.
  public float _smoothing = 5f;
  public float _sizeSmoothing = 5f;
  public float _cameraSize = 15f;
  public float _cameraDefaultSize = 10;
  // Distance away from player
  public Vector3 _offset;

  void LateUpdate()
  {
    
    Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, _cameraSize, _sizeSmoothing * Time.deltaTime);

    if(_target == null)
      return;
    if(GameManager.instance._gameState != Constants.GameState.GamePlay)
      return;

    // Create a postion the camera is aiming for based on the offset from the target.
    //Vector3 targetCamPos = _target.position + _offset;
    Vector3 targetCamPos = _target.transform.position + _offset;

    // Smoothly interpolate between the camera's current position and it's target position.
    transform.position = Vector3.Lerp(transform.position, targetCamPos, _smoothing * Time.deltaTime);
  }

  void OnEnable()
  {
    //m_size = Camera.main.orthographicSize;
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.AddListener<Hashtable>(Constants.kUpdateCameraOffsetNotification, OnCameraUpdate);
    Messenger.AddListener<Hashtable>(Constants.kCameraSizeUpdateNotification, OnSizeUpdate);
    Messenger.AddListener(Constants.kNetworkSystemDisconnectNotification, OnSystemDisconnect);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.RemoveListener<Hashtable>(Constants.kUpdateCameraOffsetNotification, OnCameraUpdate);
    Messenger.RemoveListener<Hashtable>(Constants.kCameraSizeUpdateNotification, OnSizeUpdate);
    Messenger.RemoveListener(Constants.kNetworkSystemDisconnectNotification, OnSystemDisconnect);
  }

  #region Notification Handlers

  private void OnSizeUpdate(Hashtable data)
  {
    // Get new size from data 
    _cameraSize = int.Parse(data[Constants.kCameraSizeKey].ToString());
  }

  private void OnCharSpawn(Hashtable data)
  {
    // Get the char reference
    GameObject obj = (GameObject)data[Constants.kCharacterKey];
    if(obj == null)
      return;
    _target = obj;
  }

  private void OnCameraUpdate(Hashtable data)
  { 
    Vector3 newOffset = (Vector3)data[Constants.kCameraOffsetKey];
  }

  private void OnCharDeath(Hashtable data)
  { 
    _target = null;
    _cameraSize = _cameraDefaultSize;
  }

  private void OnCharRestart()
  {
    _cameraSize = _cameraDefaultSize;
  }

  private void OnSystemDisconnect()
  {
   
    _cameraSize = _cameraDefaultSize;
  }

  #endregion
}