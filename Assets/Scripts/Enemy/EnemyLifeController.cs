﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class EnemyLifeController : LifeBase
{
  public GameObject _root;
  // The speed at which the enemy sinks through the floor when dead.
  public int scoreValue = 10;
  // The amount added to the player's score when the enemy dies.
  public AudioClip deathClip;
  // The sound to play when the enemy dies.
  public UnityEngine.AI.NavMeshAgent _meshAgent;

  public Animator _anim;
  // Reference to the animator.
  public AudioSource _enemyAudio;
  // Reference to the particle system that plays when the enemy is damaged.
  protected bool m_isDead;

  void OnEnable()
  {
    // Setting the current health when the enemy first spawns.
    _data._currentLife = _data.MaxLife;
    _meshAgent.enabled = true;
    _rigid.isKinematic = false;
    //m_isSinking = false;
    m_isDead = false;
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, bool playSound)
  {
    ApplyDamage(amount);
    if(playSound)
      DoDamageFX();
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, SharedData attackData)
  {
    // If the enemy is dead...
    if(m_isDead)
      return;

    TakeDamage(amount, hitPoint, attackData);
    AddLifeToAttacker(attackData);

    // If the current health is less than or equal to zero...
    if(_data._currentLife <= 0 && !m_isDead)
    {
      m_isDead = true;
      AddScoreToAttacker(attackData);
      // Process death fx
      DoDeathFX();
    }
    else
    {
      ApplySlowSpeedByAttacker(attackData);
      ApplyBleeding(attackData);
    }
  }

  /// <summary>
  /// Sinking animation is called after the animation is ended
  /// </summary>
  public void DoSinkingAnimation()
  {
    // Disable the Nav Mesh Agent.
    _meshAgent.enabled = false;

    // Make rigid kinematic (since we use Translate to sink the enemy).
    _rigid.isKinematic = true;

    DoDeath();

  }

  public void DoDamageFX()
  {
    // Play the hurt sound effect.
    _enemyAudio.Play();
  }

  public void DoDeathFX()
  {
    // Tell the animator that the enemy is dead.
    _anim.SetTrigger("Dead");
    // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
    _enemyAudio.clip = deathClip;
    _enemyAudio.Play();
  }

  public virtual void DoDeath()
  {
    // Broadcast about this death
    Hashtable deathData = new Hashtable();
    deathData[Constants.kCharacterPosKey] = _root.transform.position;
    deathData[Constants.kCharacterDataKey] = _data.DeathEXP;
    Messenger.Broadcast(Constants.kEnemyDeathNotification, deathData);

    LeanPool.Despawn(_root, 3.0f);
  }

}