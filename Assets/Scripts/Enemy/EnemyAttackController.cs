﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Resources;
using UnityEngine.AI;

public class EnemyAttackController : MonoBehaviour
{
  public WeaponData _enemyData;
  public Animator _anim;
  public List<string> _attackTags;
  public LayerMask _attackMask;
  public float _attackForSpeedRatio = 0.001f;
  public float _attackRotSpeedRatio = 1f;
  public float _attackSphereRadius = 5.0f;
  public Vector3 _attackCenter;

  private PlayerLifeController m_playerLife;
  private float m_timer;

  private string m_attackAnimation = "Attack";
  private string m_reset = "Reset";
  //private IEnumerator m_attackCR;

  public void OnForSpeedRatioUpdate()
  {
    _anim.enabled = _enemyData.ForSpeedRatio == 0 ? false : true;
  }

  public void OnRotSpeedRatioUpdate()
  {
    _anim.enabled = _enemyData.RotSpeedRatio == 0 ? false : true;
  }

  void OnEnable()
  {
    _anim.SetFloat(Constants.kAttackMultiplierKey, 1.0f / _enemyData.AttackInterval);
  }

  void Update()
  {
    // Skip if no life
    if(_enemyData._currentLife <= 0)
      return;

    if(m_timer > 0)
    {
      m_timer -= Time.deltaTime;
      return;
    }
    
    m_timer = _enemyData.AttackInterval;

    FindEnemyToAttack();
    //StartCoroutine(FindEnemyToAttack());
  }

  private void FindEnemyToAttack()
  {
    //yield return new WaitForSeconds(0);
    // Get all colliding objs
    Collider[] collidingObjs = Physics.OverlapSphere(transform.position + _attackCenter, _attackSphereRadius, _attackMask);
    foreach(Collider c in collidingObjs)
    {
      // Match tag?
      foreach(string s in _attackTags)
      {
        if(c.tag == s)
        {
          // Has life?
          LifeBase l = c.transform.root.GetComponentInChildren<LifeBase>();
          if(l != null && l._data._currentLife > 0)
          {
            l.TakeDamage(_enemyData.WeaponDamage, transform.position, _enemyData);

            _anim.SetBool(m_attackAnimation, true);
            _enemyData.ForSpeedRatio = _attackForSpeedRatio;
            _enemyData.RotSpeedRatio = _attackRotSpeedRatio;

            // Restart reset timer
            CancelInvoke(m_reset);
            Invoke(m_reset, _enemyData.AttackInterval);
            // Only attack once
            return;
          }
        }
      }
    }
  }

  private void Reset()
  {
    _anim.SetBool(m_attackAnimation, false);
    _enemyData.ForSpeedRatio = 1.0f;
    _enemyData.RotSpeedRatio = 1.0f;
  }
}