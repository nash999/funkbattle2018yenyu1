﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class EnemySpawner : MonoBehaviour
{
  //public SharedData _player;       // Reference to the player's heatlh.
  public GameObject _enemy;                // The enemy prefab to be spawned.
  public float _spawnTime = 3f;            // How long between each spawn.
  public GameObject _spawnRoot;
  public List<Transform> _spawnPoints;         // An array of the spawn points this enemy can spawn from.
  public int _totalSpawedObjs = 10;
  public string _enemyItemType;

  protected string m_invokeString = "Spawn";
  protected int m_currentSpawnTotal = 0;

  public void OnEnable()
  {
    GetSpawnPositions();
    Messenger.AddListener<Hashtable>(Constants.kEnemyDeathNotification, OnEnemyDeath);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnPlayerRestart);
    Messenger.AddListener(Constants.kNetworkSystemRoomLeftNotification, OnPlayerLeft);
    StartProcess();
  }

  public void OnDisable()
  {
    ResetSpawnPositions();
    Messenger.RemoveListener<Hashtable>(Constants.kEnemyDeathNotification, OnEnemyDeath);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnPlayerRestart);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomLeftNotification, OnPlayerLeft);
    StopProcess();
  }

  public virtual void StartProcess()
  {
    // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
    InvokeRepeating(m_invokeString, _spawnTime, _spawnTime);
  }

  public virtual void StopProcess()
  {
    CancelInvoke(m_invokeString);
  }

  public virtual void Spawn()
  {
    // Skip if we are not in game play state
    if(GameManager.instance._gameState != Constants.GameState.GamePlay)
      return;
    if(m_currentSpawnTotal >= _totalSpawedObjs)
      return;
    // Find a random index between zero and one less than the number of spawn points.
    int spawnPointIndex = Random.Range(0, _spawnPoints.Count);
    GameObject spawnObj = LeanPool.Spawn(_enemy, _spawnPoints[spawnPointIndex].position, _spawnPoints[spawnPointIndex].rotation);
    SharedData sd = spawnObj.GetComponentInChildren<SharedData>();
    if(sd)
      sd._itemType = _enemyItemType;
    m_currentSpawnTotal++;
  }

  private void OnPlayerLeft()
  {
    
    CheckAndClearEnemy();
      
  }

  private void OnPlayerRestart()
  {
    CheckAndClearEnemy();
  }

  private void CheckAndClearEnemy()
  {
    m_currentSpawnTotal = 0;
  }

  private void OnEnemyDeath(Hashtable data)
  {
    //Debug.Log("Checking enemy death: " +  data[Constants.kItemTypeKey]);
    if(_enemyItemType == data[Constants.kItemTypeKey].ToString())
    {
      //Debug.Log("enemy death: " + _enemyItemType);
      m_currentSpawnTotal--;
    }
  }

  private void GetSpawnPositions()
  {
    if(_spawnRoot == null)
      return;
    ResetSpawnPositions();
    for(int i = 0; i < _spawnRoot.transform.childCount; ++i)
    {
      _spawnPoints.Add(_spawnRoot.transform.GetChild(i));
    }
  }

  private void ResetSpawnPositions()
  {
    if(_spawnRoot == null)
      return;
    _spawnPoints.Clear();
  }
}