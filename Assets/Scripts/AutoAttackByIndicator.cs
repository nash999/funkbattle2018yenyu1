﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttackByIndicator : MonoBehaviour
{
	public LayerMask _includedLayerMask;
  public QueryTriggerInteraction _interaction;
  public GameObject _center;
  public ActionBase _attack;
  //public data
  public float _radius;
  public float _angle;

	// Update is called once per frame
	void Update ()
  {
    // Skip if player has no life
    if(_attack._data._currentLife <= 0)
      return;
    // Skip if player is gathering ultimate attack
    if(_attack._data._inputKey == Constants.KeyType.Press)
      return;
    Collider[] hitColliders = Physics.OverlapSphere(_center.transform.position, _radius, _includedLayerMask, _interaction);
    if(hitColliders.Length == 0)
      return;
    
    for(int i = 0; i < hitColliders.Length; ++i)
    {
      // Skip self
      GameObject hitObj = hitColliders[i].gameObject;
      if(hitObj == _center)
        continue;
      //Debug.Log("Hit: " + hitObj.name);
      // Skip obj without life
      LifeBase lb = hitObj.transform.root.GetComponentInChildren<LifeBase>();
      if(lb == null)
        continue;
      // Still have life?
      if(lb._data._currentLife <= 0)
        return;
      // Check the angle
      Vector3 targetDir = hitObj.transform.position - _center.transform.position;
      float angle = Vector3.Angle(targetDir, _center.transform.forward);
      //Debug.Log("has life: " + hitObj.name + " angle: " + angle);
      //float angle = Vector3.Angle(_center.transform.position, hitObj.transform.position);
      if(angle > _angle)
        continue;
      
      //Debug.Log("in angle: " + hitObj.name + " angle: " + angle);
      // Face this object 
      // Attack
      _attack.ProcessDown();
      _attack.ProcessUp();
      // Only attack one 
      return;
    }	
	}
}