﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionAnimationController : MonoBehaviour
{
  public Animator _animator;
  public List<AnimationClip> _randomNotChosenAnimations;

  public AnimatorOverrideController _animatorOverrideController;

  void OnInactiveAnimation()
  {
    _animator.SetInteger("State", 0);
  }

  void OnActiveAnimation()
  {
    _animator.SetInteger("State", 1);
  }

  void OnChosenAnimation()
  {
    //Debug.Log("OnChosen");
    _animator.SetInteger("State", 2);
  }

  void OnNonChosenAniamtion()
  {
    // Randomly assign the animation clip
    _animatorOverrideController["GeneralNotChosen"] = _randomNotChosenAnimations[Random.Range(0,_randomNotChosenAnimations.Count)];
    _animator.SetInteger("State", 3);
  }

}
