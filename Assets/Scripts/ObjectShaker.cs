﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectShaker : MonoBehaviour
{
  // Obj that will be applied shaking 
  public Transform _objTransform;

  // Used for perlin noise 
  public float _magnitude = 2f;
  public float _frequency = 10f;
  // Should shake position?
  public bool _shouldShakePos = true;
  // Should shake rotation?
  public bool _shouldShakeRot = true;
  // Should reset position to default when shaking is done
  // When the object is always moving, set this to false
  // because it will reset the position to the beginning of the shaking 
  public bool _shouldResetPos = false;
  // Should reset rotation to default when shaking is done?
  public bool _shouldResetRot = true;
  // If the object is always moving, should set this to false
  public bool _shouldUseDefaultForPos = false;
  // If the object is always rotating, should set this to false
  public bool _shouldUseDefaultForRot = true;
  // How long the object should shake for
  public float _shakeDuration = 0f;

  private bool m_hasReset = true;
  private Vector3 m_defaultRot;
  private Vector3 m_defaultPos;
  private float m_decreaseFactor = 1.0f;

  public void StartShaking(float shakeTime = 0.5f)
  {
    //Debug.Log("shake");
    _shakeDuration = shakeTime;
  }

  void OnEnable()
  {
    if (_objTransform == null)
      _objTransform = GetComponent(typeof(Transform)) as Transform;
  }

  void Update()
  {
    if(_shakeDuration > 0)
    {
      if(m_hasReset)
      {
        UpdateDefaultPosRot();
        m_hasReset = false;
      }

      if(_shouldShakePos)
      {
        ApplyPosShake(PerlinShake());
      }
      if(_shouldShakeRot)
      {
        ApplyRotShake(PerlinShake());
      }

      _shakeDuration -= Time.deltaTime * m_decreaseFactor;
    }
    else
    {
      if(!m_hasReset)
      {
        m_hasReset = true;
        if(_shouldResetPos)
          ResetPos();
        if(_shouldResetRot)
          ResetRot();
      }

      _shakeDuration = 0f;
    }
  }

  private Vector2 PerlinShake()
  {
    Vector2 result;
    float seed = Time.time * _frequency;
    result.x = Mathf.Clamp01(Mathf.PerlinNoise (seed, 0f)) - 0.5f;
    result.y = Mathf.Clamp01(Mathf.PerlinNoise (0f, seed)) - 0.5f; 
    result = result * _magnitude;
    return result;
  }

  private void ApplyPosShake(Vector2 noise)
  {
    Vector3 newPos;
    if(_shouldUseDefaultForPos)
      newPos = m_defaultPos;
    else
      newPos = _objTransform.localPosition;
    newPos.x += noise.x;
    newPos.y += noise.y;
    _objTransform.localPosition = newPos;
  }

  private void ApplyRotShake(Vector2 noise)
  {
    Vector3 newRot;
    if(_shouldUseDefaultForRot)
      newRot = m_defaultRot;
    else
      newRot = _objTransform.localRotation.eulerAngles;

    newRot.x += noise.x;
    newRot.y += noise.y;
    _objTransform.localRotation = Quaternion.Euler(newRot);
  }

  private void ResetPos()
  {
    _objTransform.localPosition = m_defaultPos;
  }

  private void ResetRot()
  {
    _objTransform.localRotation = Quaternion.Euler(m_defaultRot);
  }

  private void UpdateDefaultPosRot()
  {
    m_defaultPos = _objTransform.localPosition;
    m_defaultRot = _objTransform.localRotation.eulerAngles;
  }
}