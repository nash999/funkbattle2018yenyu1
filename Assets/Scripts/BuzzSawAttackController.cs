﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class BuzzSawAttackController : MonoBehaviour
{
  public GameObject _root;
  public WeaponData _data;
  public LayerMask _layerToAttack;

  void OnEnable()
  {
    _data._currentLife = _data.MaxLife;
  }

  void OnTriggerEnter(Collider other)
  {
    //ApplyDamage(other.gameObject);
  }

  void OnCollisionEnter(Collision collision)
  {
    ApplyDamage(collision.gameObject);
  }

  public void ApplyDamage(GameObject other)
  {
    if((_layerToAttack.value == (_layerToAttack.value | (1 << other.gameObject.layer))))
    {
      LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
      if(otherLife == null)
        return;
      // Make sure they have life
      if(otherLife._data._currentLife <= 0)
        return;
      // Apply for damage
      otherLife.TakeDamage(_data.WeaponDamage, transform.position, _data);
      return;
    }
  }
}