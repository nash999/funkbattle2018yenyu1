﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class LightningAttackController : BasicWeaponAttackController
{
  public GameObject _vfxToSpawn;
  public GameObject _spawnPos;
  public List<GameObject> _spawnVfxList;
  public float _attackRadius = 10.0f;
  public LayerMask _attackLayer;

  private float m_attackInterval;

  public override void OnEnable()
  {
    base.OnEnable();
  }

  void OnDisable()
  { 
    m_attackInterval = 0;
    Reset();
  }

  void Update()
  {
    if(m_attackInterval > 0)
    {
      m_attackInterval -= Time.deltaTime;
      return;
    }
    m_attackInterval = _data.AttackInterval;

    Reset();

    // Find targets
    Collider[] targets = Physics.OverlapSphere(transform.position, _attackRadius, _attackLayer, QueryTriggerInteraction.Collide);
    foreach(Collider c in targets)
    {
      // Make sure they are not owner
      PlayerData pData = c.transform.root.GetComponentInChildren<PlayerData>();
      if(pData && pData._networkID == _data._ownerNetworkID)
        continue;
      // Spawn new vfx
      GameObject obj = LeanPool.Spawn(_vfxToSpawn, Constants.kVector3Zero, Constants.kQuaternionIdentity, _spawnPos.transform);
      // Save vfx
      _spawnVfxList.Add(obj);
      // Get script to assign target
      LightningVFXController[] scripts = obj.GetComponentsInChildren<LightningVFXController>();
      foreach(LightningVFXController v in scripts)
      v._target = c.gameObject;
      ApplyDamage(c.gameObject);
    }
  }

  private void Reset()
  {
    // Remove previous
    foreach(GameObject go in _spawnVfxList)
    LeanPool.Despawn(go);
    _spawnVfxList.Clear();
  }

}