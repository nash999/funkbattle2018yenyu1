﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class StartMenuController : MonoBehaviour
{
  public GameObject _playMenu;
  public List<GameObject> _chars;
  public GameObject _spawnRoot;
  public List<Transform> _spawnLocations;
  public InputField _nameInput;
  public Text _nameInputInfo;
  public GameObject _charPanel;
  public string _defaultName = "Anonymous player";
  public int _minNameChar = 4;
  public int _maxNameChar = 15;

  public float _timeToShowUIAfterCharacterDeath = 5.0f;

  public void OnNameInputEnd()
  {
    Debug.Log("name: " + _nameInput.text);
    UpdateName();
  }

  public void OnCharButtonDown()
  {
    //UpdateName();
    if(UpdateName())
    {
      // Send out character selection notification
      Messenger.Broadcast(Constants.kCharacterSelectionNotifiation);
      // Delay a bit so character animation can run 
      Invoke("StartSystem", 2.0f);
    }
     
  }

  void Start()
  {
    // Disable if network is not ready
    _playMenu.SetActive(PhotonNetwork.connectedAndReady);
    OnDBSystemReady();
  }

  void OnEnable()
  {
    GetLocations();
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.AddListener(Constants.kCharacterResumeNotification, OnCharResume);
    Messenger.AddListener(Constants.kGSLoginSuccessNotification, OnDBSystemReady);
    Messenger.AddListener(Constants.kNetworkSystemRoomReadyNotification, OnNetworkSystemInRoom);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.RemoveListener(Constants.kCharacterResumeNotification, OnCharResume);
    Messenger.RemoveListener(Constants.kGSLoginSuccessNotification, OnDBSystemReady);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomReadyNotification, OnNetworkSystemInRoom);
    ResetLocations();
  }

  private void StartSystem()
  {
    Messenger.Broadcast(Constants.kNetworkSystemStartNotification); 
  }

  private void ShowMenu()
  {
    _playMenu.SetActive(true);
  }

  private void SpawnObj(GameObject objToSpawn, Constants.TeamSide teamSide)
  {
    //Debug.Log("Spawn: " + objToSpawn + " team: " + teamSide);

    // Get a random location from list 
    Transform spawnPosObj = _spawnLocations[Random.Range(0, _spawnLocations.Count)];

    GameObject obj = PhotonNetwork.Instantiate
    (
      objToSpawn.name,
      spawnPosObj.position,
      spawnPosObj.rotation,
      0,
      null
    );

    //GameObject obj = LeanPool.Spawn(objToSpawn, Constants.kVector3Zero, Constants.kQuaternionIdentity);
    // Update the date 
    SharedData objData = obj.GetComponentInChildren<SharedData>();
    objData._team = teamSide;

    // Send out notification
    Hashtable data = new Hashtable();
    data[Constants.kCharacterKey] = obj;
    Messenger.Broadcast<Hashtable>(Constants.kCharacterSpawnNotification, data);

    _playMenu.SetActive(false);
    _charPanel.SetActive(false);

    // Change state
    GameManager.instance._gameState = Constants.GameState.GamePlay;
  }

  #region Notification Handlers

  private void OnCharRestart()
  {
    // Exit the room
    //if(PhotonNetwork.inRoom)
    Debug.Log("Leave room");
    PhotonNetwork.LeaveRoom();
    GameManager.instance._gameState = Constants.GameState.CharacterSelection;

    _playMenu.SetActive(true);
    _charPanel.SetActive(true);
  }

  private void OnCharResume()
  {
    OnNetworkSystemInRoom();
  }

  private void OnDBSystemReady()
  {
    _playMenu.SetActive(true);
    _charPanel.SetActive(true);
    InitName();
  }

  private void OnNetworkSystemInRoom()
  {
    // Switch team based on total players
    Constants.TeamSide side = GameManager.instance._localTeam;
    GameObject obj = _chars[GameManager.instance._currentCharacterIndex];
    SpawnObj(obj, side);

    Analytics.CustomEvent(Constants.kAvatarChosenUAE, new Dictionary<string, object>
    {
      { "AvatarName", obj.name}
    });
  }

  private bool UpdateName()
  {
    //Debug.Log("Name length: " + _nameInput.text.Length);
    // Get the name with front/back space removal 
    string name = _nameInput.text.Trim();
    // If name length is 0, we replace it with random one
    if(name.Length == 0)
    {
      name = GameManager.instance._anonymousPreName + Random.Range(GameManager.instance._anonymousRandomRangeMin, GameManager.instance._anonymouseRandomRangeMax);
      _nameInput.text = name;
      // We don't save the name for random name
      PlayerPrefs.DeleteKey(Constants.kPlayerNameKey);
    }
    else
    {
      PlayerPrefs.SetString(Constants.kPlayerNameKey, name);
    }

    _nameInputInfo.text = "";
    PhotonNetwork.playerName = name;

    /*
    if(_nameInput.text.Length < _minNameChar || _nameInput.text.Length > _maxNameChar)
    {
      
      _nameInputInfo.text = GameManager.instance._invalidNameDescPre + _minNameChar + 
      GameManager.instance._invalidNameAnd + _maxNameChar + 
      GameManager.instance._invalidNameDescPost;
      return false;
    }
    else
    {
      _nameInputInfo.text = "";
      PhotonNetwork.playerName = _nameInput.text;
      PlayerPrefs.SetString(Constants.kPlayerNameKey, _nameInput.text);
      return true;
    }
    */
    return true;
  }

  #endregion

  private void GetLocations()
  {
    if(_spawnRoot == null)
      return;
    ResetLocations();
    for(int i = 0; i < _spawnRoot.transform.childCount; ++i)
    {
      _spawnLocations.Add(_spawnRoot.transform.GetChild(i));
    }
  }

  private void ResetLocations()
  {
    if(_spawnRoot == null)
      return;
    _spawnLocations.Clear();
  }

  private void InitName()
  {
    string name = PlayerPrefs.GetString(Constants.kPlayerNameKey, "");
    if(name.Length == 0)
      return;
    _nameInput.text = name;

  }
}