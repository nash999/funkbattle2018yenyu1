﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NetworkAILifeController : NetworkLifeController
{
  public NavMeshAgent _nav;

  public override void OnEnable()
  {
    base.OnEnable();
    _nav.enabled = _view.isMine;

    Messenger.AddListener(Constants.kCharacterRestartNotification, CheckOffline);
  }

  public void OnDisable()
  {
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, CheckOffline);
  }

  public override void Death()
  {
    base.Death();
    _nav.enabled = false;
  }

  public override void NotifyDeath()
  {
    //Debug.Log("AINotifyDeath");
    // Broadcast about this death
    Hashtable deathData = new Hashtable();
    deathData[Constants.kCharacterPosKey] = _root.transform.position;
    deathData[Constants.kCharacterDataKey] = _data.DeathEXP;
    deathData[Constants.kItemTypeKey] = _data._itemType;
    Messenger.Broadcast(Constants.kEnemyDeathNotification, deathData);
  }

  private void CheckOffline()
  {
    if(PhotonNetwork.offlineMode)
    {
     _data._currentLife = 0;
      Death();
    }
  }
}
