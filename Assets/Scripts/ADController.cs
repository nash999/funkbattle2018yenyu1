﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Unity Analytics
using UnityEngine.Analytics;
// YoMob AD 
//using Together;
// Appodeal
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class ADController : MonoBehaviour, IRewardedVideoAdListener
{
  public bool _showADDebugInfo = false;

  private bool m_isADShowedAndCompleted = false;

  public void OnShowVideoAdButtonDown()
  {
    #if UNITY_IOS || UNITY_ANDROID

    // For testing purpose only
    // Make sure you turn off this for production
    if(GameManager.instance._passAD)
    {
      Messenger.Broadcast(Constants.kYMVideoShowSuccess);
      return;
    }

    ProcessPreVideo();

    //if(GameManager.instance._showDebugYomobAd)
    //{
    //  Debug.Log("Show test YOMOB ad");
    //  TGSDK.ShowTestView(Constants.kYMAdSceneId);
    //}
    //else
    {
      Debug.Log("Ready to show video ad");
      // Appodeal has high priority since its eCPM is higher
      if(Appodeal.isLoaded(Appodeal.REWARDED_VIDEO) || GameManager.instance._showDebugAppodealAd)
      {
        Debug.Log("Show Appodeal AD: test mode: " + GameManager.instance._showDebugAppodealAd);
        Appodeal.show(Appodeal.REWARDED_VIDEO);
      }
      //else
      //{
      //  Debug.Log("Show Yomob AD");
      //  TGSDK.ShowAd(Constants.kYMAdSceneId);
      //}
    }
    #endif

  }

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGDPRReadyNotification, InitADSystem);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGDPRReadyNotification, InitADSystem);
  }

	// Use this for initialization
	void InitADSystem()
  {
    #if UNITY_IOS || UNITY_ANDROID

    /*
    Debug.Log("Init TGSDK: " + TGSDKVersion.TGSDKVERSION);

    TGSDK.Initialize("tfOr18F372630u3p359F");

    TGSDK.PreloadAdSuccessCallback = OnPreloadAdSuccess;
    TGSDK.PreloadAdFailedCallback = OnPreloadAdFailed;
    TGSDK.CPAdLoadedCallback = OnCPAdLoaded;
    TGSDK.VideoAdLoadedCallback = OnVideoAdLoaded;

    TGSDK.AdShowSuccessCallback = OnAdShowSuccess;
    TGSDK.AdShowFailedCallback = OnAdShowFailed;
    TGSDK.AdCompleteCallback = OnAdComplete;
    TGSDK.AdCloseCallback = OnAdClose;
    TGSDK.AdClickCallback = OnAdClick;
    TGSDK.AdRewardSuccessCallback = OnAdRewardSuccess;
    TGSDK.AdRewardFailedCallback = OnAdRewardFailed;
   
    TGSDK.PreloadAd();
    */
    Debug.Log("Init Appodeal with consent: " + (GameManager.instance._GDPRChoiceIndex == 1));
    string appKey = Constants.kAppOdealKey;

    // Enable testing 
    if(GameManager.instance._showDebugAppodealAd)
    {
      Appodeal.setTesting(GameManager.instance._showDebugAppodealAd);
    }
    // Enable auto caching
    Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, true);
    // Disable location permission check
    Appodeal.disableLocationPermissionCheck();
    // Show debug output
    if(_showADDebugInfo)
      Appodeal.setLogLevel(Appodeal.LogLevel.Verbose);
    //Appodeal.initialize(appKey, Appodeal.REWARDED_VIDEO, false);
    Appodeal.initialize(appKey, Appodeal.REWARDED_VIDEO, (GameManager.instance._GDPRChoiceIndex == 1));
    // Add reward callback
    Appodeal.setRewardedVideoCallbacks(this);

    #endif
	}


  #region Yomob callbacks 

  public void OnPreloadAdSuccess(string ret)
  {
    Debug.Log("OnPreloadAdSuccess: " + ret);
  }

  public void OnPreloadAdFailed(string error)
  {
    Debug.Log("OnPreloadAdFailed: " + error);
    Analytics.CustomEvent(Constants.kYoMobUAE, new Dictionary<string, object>
    {
      { "ADFailed", error}
    });
  }

  public void OnCPAdLoaded(string ret)
  {
    Debug.Log("OnCPAdLoaded: " + ret);
  }

  public void OnVideoAdLoaded(string ret)
  {
    Debug.Log("OnVideoAdLoaded: " + ret);
  }

  public void OnAdShowSuccess(string ret)
  {
    Debug.Log("OnAdShowSuccess: " + ret);
  }

  public void OnAdShowFailed(string err)
  {
    Debug.Log("OnAdShowFailed: " + err);
  }

  public void OnAdComplete(string ret)
  {
    Debug.Log("OnAdComplete: " + ret);
  }

  public void OnAdClose(string ret)
  {
    #if UNITY_IOS || UNITY_ANDROID

    ProcessPostVideo();

    #endif
  }

  public void OnAdClick(string ret)
  {
    Debug.Log("OnAdClick: " + ret);
  }

  public void OnAdRewardSuccess(string ret)
  {
    Debug.Log("OnAdRewardSuccess: " + ret);
    m_isADShowedAndCompleted = true;
  }

  public void OnAdRewardFailed(string err)
  {
    Debug.Log("OnAdRewardFailed: " + err);
  }


  #endregion

  #region Rewarded Video callback handlers

  public void onRewardedVideoLoaded(bool result)
  {
    Debug.Log("Video loaded: " + result); 
  }

  public void onRewardedVideoFailedToLoad()
  {
    Debug.Log("Video failed"); 
  }

  public void onRewardedVideoExpired()
    {

    }
    public void onRewardedVideoShown() 
  {
    Debug.Log("Video shown");
    m_isADShowedAndCompleted = true;
  }

  public void onRewardedVideoClosed(bool finished) 
  {
    print("Video closed: " + finished);

    //m_isADShowedAndCompleted = finished;
    ProcessPostVideo();
  }

  public void onRewardedVideoFinished(double amount, string name)
  { 
    print("Reward: " + amount + " " + name);
  }

  private IEnumerator MuteAudio()
  {
    AudioListener.pause = true;
    AudioListener.volume = 0;
    yield return null;
  }

  private IEnumerator UnmuteAudio()
  {
    AudioListener.pause = false;
    AudioListener.volume = 1;
    yield return null;
  }

  #endregion

  private void ProcessPreVideo()
  {
    m_isADShowedAndCompleted = false;

    // Make sure we are on the main thread
    UnityMainThreadDispatcher.Instance().Enqueue(MuteAudio()); 
  }

  private void ProcessPostVideo()
  {
    //Debug.Log("ProcessPostVideo");
    if(m_isADShowedAndCompleted)
    {
      Messenger.Broadcast(Constants.kYMVideoShowSuccess);
    }
    else
    {
      Messenger.Broadcast(Constants.kYMVideoShowFailure);
    }

    // Make sure we are on the main thread
    UnityMainThreadDispatcher.Instance().Enqueue(UnmuteAudio()); 
  }
}