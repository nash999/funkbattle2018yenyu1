﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurningInputBase : MonoBehaviour
{
  public SharedData _data;
  public Vector3 _turningOffset;

  public virtual void CheckTurning()
  {
  }

  void Update()
  {
    if(!_data)
      return;
    if(_data._currentLife <= 0)
      return;
    CheckTurning();

    //Debug.Log("turning input base rot: " + _data._rot);
  
  }
}
