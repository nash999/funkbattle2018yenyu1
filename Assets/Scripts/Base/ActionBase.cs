﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionBase : MonoBehaviour
{
  public PlayerData _data;
  public Animator _anim;
  public string _animationKey = "";

  private bool m_isAnimating = false;
  private string m_invokeKey = "StopAttackAnimation";

  public virtual void ProcessDown()
  {
  }

  public virtual void ProcessPress()
  {
  }

  public virtual void ProcessUp()
  {
    DoAttackWithAnimation();
  }

  public void DoAttackWithAnimation()
  {
    if(m_isAnimating)
      return;
    StartAttackAnimation();
    DoAttack();
  }

  public virtual void DoAttack()
  {
  }
 
  private void StartAttackAnimation()
  {
    m_isAnimating = true;
    CancelInvoke(m_invokeKey);
    _anim.SetBool(_animationKey, true);
    Invoke(m_invokeKey, _data.AttackInterval);
  }

  private void StopAttackAnimation()
  {
    m_isAnimating = false;
    _anim.SetBool(_animationKey, false);
  }

}