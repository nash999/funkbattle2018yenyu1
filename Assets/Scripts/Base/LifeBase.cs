﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBase : MonoBehaviour
{
  public Rigidbody _rigid;
  public SharedData _data;
  public float _bleedingInterval = 0.5f;
  public int _bleedingTotal = 2;

  public virtual void TakeDamage(int amount, Vector3 hitPoint, bool playSound = true){}
  public virtual void TakeDamage(int amount, Vector3 hitPoint, SharedData attackData){}

  private int m_currentBleedingTotal = 0;
  private float m_currentBleedingTime = 0;
  private float m_currentBleedingPercentage = 0;

  public void NotifyDamage(string output, Color c)
  {
    //Debug.Log("haha");
    Hashtable data = new Hashtable();
    data[Constants.kCharacterKey] = gameObject;
    data[Constants.kColorKey] = c;
    data[Constants.kDamageOutputKey] = output;

    Messenger.Broadcast(Constants.kDamageAppliedNotification, data);
  }

  public int ApplyDamageWithoutDefense(int amount)
  {
    _data._currentLife -= amount;

    // Clamp the value
    _data._currentLife = Mathf.Clamp(_data._currentLife, 0, _data.MaxLife);

    return amount;
  }

  public int ApplyDamage(int amount)
  {
    // Reduce the current health by the damage amount with defense 
    int calculatedDamage = (int)(amount - ((float)amount * (float)_data.DefensePercentage * 0.01f));
    //Debug.Log("Calculate damage: " + calculatedDamage + " original: " + amount);

    return ApplyDamageWithoutDefense(calculatedDamage);
  }

  public void ApplyForce(Vector3 amount)
  {
    if(_rigid == null)
      return;
    _rigid.AddForce(amount, ForceMode.VelocityChange);
    NotifyDamage(GameManager.instance._knockString, Color.blue);
  }

  public void AddScoreToAttacker(SharedData attackerData)
  {
    //Debug.Log(gameObject.name + " check if we can add score to: " + attackerData._root.name);
    if(attackerData == null)
    {
      //Debug.Log("Attack data is null");
      return;
    }
    if(attackerData._scoreController == null)
    {
      //Debug.Log("attack data score controller is null: " + attackerData._root.name);
      return;
    }
    //Debug.Log("Add score to attacker: " + _data._currentScore);
    attackerData._scoreController.AddScore(_data.DeathScore);
  }

  public void AddLifeToAttacker(SharedData attackerData)
  {
    if(attackerData == null)
      return;
    if(attackerData._expController == null)
      return;
    //If player has blood suck, return blood
    WeaponData wd = attackerData._root.GetComponentInChildren<WeaponData>();
    if(wd == null)
      return;
    LifeBase lb = attackerData._expController._data._root.GetComponent<LifeBase>();
    if(lb)
    {
      int d = (int)((float)wd.WeaponDamage * (float)wd.BloodSuckPercentage * -0.01f);
      //Debug.Log("Add life back: " + d + " to: " + lb.gameObject.name);
      lb.TakeDamage(d, Constants.kVector3Zero);
    }
      
  }

  public void ApplySlowSpeedByAttacker(SharedData attackerData)
  {
    if(attackerData == null)
      return;
    // If player has speed slow ability, slow the movement 
    WeaponData wd = attackerData._root.GetComponentInChildren<WeaponData>();
    if(wd == null)
      return;
    if(wd.SlowSpeedPercentage == 0)
      return;
    float newValue = (1.0f - wd.SlowSpeedPercentage * 0.01f);
    _data.ForSpeedRatio = newValue;
    _data.RotSpeedRatio = newValue; 

    //Debug.Log("Slow this obj: " + newValue);
    NotifyDamage(GameManager.instance._slowString, Color.yellow);
    CancelInvoke("ResetSlowSpeed");
    Invoke("ResetSlowSpeed", wd.SlowSpeedResetTime);
  }

  public void ApplyBleeding(SharedData attackerData)
  {
    if(attackerData == null)
      return;
    WeaponData wd = attackerData._root.GetComponentInChildren<WeaponData>();
    if(wd == null)
      return;
    // Start bleeding logic if attacker has bleeding skill 
    if(wd.BleedingPercentage > 0)
    {
      m_currentBleedingPercentage = (wd.BleedingPercentage * 0.01f);
      m_currentBleedingTime = _bleedingInterval;
      m_currentBleedingTotal = _bleedingTotal;
    }
  }

  private void ResetSlowSpeed()
  {
    _data.ForSpeedRatio = 1.0f;
    _data.RotSpeedRatio = 1.0f;
  }

  void Update()
  {
    DoBleeding();
  }

  private void DoBleeding()
  {
    if(m_currentBleedingTotal <= 0)
      return;
    if(m_currentBleedingTime > 0)
    {
      m_currentBleedingTime -= Time.deltaTime;
      return;
    }
    m_currentBleedingTime = _bleedingInterval;
    int bleedingAmount = (int)(_data._currentLife * (m_currentBleedingPercentage / _bleedingTotal));
    bleedingAmount = Mathf.Clamp(bleedingAmount, 1, bleedingAmount);
    //Debug.Log("Bleeding: " + bleedingAmount + " currentlife: " + _data._currentLife + " perc: " + m_currentBleedingPercentage + " tot: " + _bleedingTotal);
    TakeDamage(bleedingAmount, Constants.kVector3Zero, false);
    m_currentBleedingTotal--;
  }
}