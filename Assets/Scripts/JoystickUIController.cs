﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickUIController : MonoBehaviour
{
  public GameObject _joystickUI;
  //public GameObject _touchManager;

	// Use this for initialization
	void Start ()
  {
    // Disable by default
    _joystickUI.SetActive(false);	
    //_touchManager.SetActive(false);
	}

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGameOverNotification, OnGameOver);
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.AddListener(Constants.kCharacterResumeNotification, OnCharResume);
    Messenger.AddListener(Constants.kNetworkSystemRoomReadyNotification, OnNetworkSystemInRoom);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharacterRestart);
    Messenger.AddListener(Constants.kNetworkSystemRoomLeftNotification, OnRoomLeft);
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGameOverNotification, OnGameOver);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.RemoveListener(Constants.kCharacterResumeNotification, OnCharResume);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomReadyNotification, OnNetworkSystemInRoom);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharacterRestart);
    Messenger.RemoveListener(Constants.kNetworkSystemRoomLeftNotification, OnRoomLeft);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
  }

  #region Notification Handlers

  private void OnCharSpawn(Hashtable data)
  {
    _joystickUI.SetActive(true);
    //_touchManager.SetActive(true);
  }

  private void OnNetworkSystemInRoom()
  {
    _joystickUI.SetActive(true);
    //_touchManager.SetActive(true);
  }

  private void OnCharResume()
  {
    _joystickUI.SetActive(true);
    //_touchManager.SetActive(true);
  }

  private void OnGameOver()
  {
    _joystickUI.SetActive(false);
    //_touchManager.SetActive(false);
  }

  private void OnCharDeath(Hashtable data)
  {
    _joystickUI.SetActive(false);
    //_touchManager.SetActive(false);
  }

  private void OnCharacterRestart()
  {
    _joystickUI.SetActive(false);
    //_touchManager.SetActive(false);
  }

  private void OnRoomLeft()
  {
    _joystickUI.SetActive(false);
    //_touchManager.SetActive(false);
  }

  #endregion
}