﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean;

public class DamageUICellController : MonoBehaviour
{
  public Text _totalText;
  public float _animatedTime = 1.0f;
  public Vector3 _dest = new Vector3(0,1,0);

  private Vector3 m_defaultPos;

  public void UpdateUI(string total, Color c)
  {
    _totalText.text = total;
    _totalText.color = c;
    //_totalText.color.a = 1.0f;
    //Debug.Log(gameObject.name + " Damage UI: " + total);
    StartAnimation();
  }

  void OnEnable()
  {
    m_defaultPos = transform.localPosition;
  }

  void OnDisable()
  {
    _totalText.text = "";
    transform.localPosition = m_defaultPos;
  }

  private void StartAnimation()
  {
    LeanTween.value(0, _animatedTime, _animatedTime).setOnUpdate(OnAnimationUpdate).setOnComplete(OnAnimationComplete);
  }

  private void OnAnimationUpdate(float value)
  { 
    //Debug.Log("-->" + value);
    transform.position += (_dest / _animatedTime);
    Color old = _totalText.color;
    old.a = value / _animatedTime;
    _totalText.color = old;
    //_totalText.color.a = (
  }

  private void OnAnimationComplete()
  {
    //LeanPool.Despawn(gameObject, 0);
  }
}