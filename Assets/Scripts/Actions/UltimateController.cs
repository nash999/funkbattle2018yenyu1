﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;
using System;

public class UltimateController : ActionBase
{
  public CameraData _cameraData;
  public List<Constants.CareerType> _weaponTypes;
  public List<bool> _shouldAttachWeapon;
  public List<GameObject> _weapons;
  public List<GameObject> _indicators;
  public List<Transform> _spawnPositions;
  public List<Vector3> _weaponSizeForLevel1;
  public List<Vector3> _weaponSizeForLevel2;
  public AudioSource _audio;
  public AudioClip _attackClip;

  protected int m_weaponIndex;

  protected GameObject m_objToSpawn;
  protected GameObject m_indicator;
  protected bool m_shouldAttachWeapon;
  private float m_defaultCameraSize;
  private Coroutine m_cameraCR;

  public override void DoAttack()
  {
    SpawnObj();
  }

  public IEnumerator UpdateCameraSize(float size, float time)
  {
    //Debug.Log("Update camera size: " + size + " time: " + time + " energy: " + _data._currentEnergy);
    yield return new WaitForSeconds(time);
    Hashtable data = new Hashtable();
    data[Constants.kCameraSizeKey] = (int)size;
    Messenger.Broadcast(Constants.kCameraSizeUpdateNotification, data);
  }

  public virtual void SpawnObj()
  {
    GameObject spawnedObj = null;
    CheckWeapon();
    // GameObject spawnedObj = LeanPool.Spawn(_attackObj, _spawnTrans.position, _spawnTrans.rotation);
    if(m_shouldAttachWeapon)
    {
      //Debug.Log("Attach ultimate");
      spawnedObj = LeanPool.Spawn(m_objToSpawn, _spawnPositions[m_weaponIndex].position, _spawnPositions[m_weaponIndex].rotation, _spawnPositions[m_weaponIndex]);
      spawnedObj.transform.localPosition = Constants.kVector3Zero;
      spawnedObj.transform.localRotation = Constants.kQuaternionIdentity;
    }
    else
    {
      //Debug.Log("Don't attach ultimate");
      spawnedObj = LeanPool.Spawn(m_objToSpawn, _spawnPositions[m_weaponIndex].position, _spawnPositions[m_weaponIndex].rotation);
    }
    spawnedObj.transform.localScale = GetWeaponSize();
    //Debug.Log("Size: " + spawnedObj.transform.localScale);
    InitSpawnObj(spawnedObj);
  }

  public void InitSpawnObj(GameObject obj)
  {
    WeaponData spawnedObjData = obj.GetComponentInChildren<WeaponData>();
    if(spawnedObjData)
    {
      spawnedObjData._team = _data._team;
      spawnedObjData._ownerNetworkID = _data._networkID;
      spawnedObjData._expController = _data._expController;
      spawnedObjData._scoreController = _data._scoreController;

      // Some data won't be used in ultimate
      //spawnedObjData._currentWeaponSuckBloodLevel = _data._currentWeaponSuckBloodLevel;
      //spawnedObjData._currentWeaponBleedingLevel = _data._currentWeaponBleedingLevel;
      spawnedObjData._currentWeaponRegularDamageLevel = _data._currentWeaponUltimateDamageLevel;
      //spawnedObjData._currentWeaponActiveTimeLevel = _data._currentWeaponActiveTimeLevel;
      //spawnedObjData._currentWeaponRegularDamageLevel = _data._currentWeaponRegularDamageLevel;
      //spawnedObjData._currentWeaponDamageForceLevel = _data._currentWeaponDamageForceLevel;
      //spawnedObjData._currentWeaponSlowSpeedLevel = _data._currentWeaponSlowSpeedLevel;
      //spawnedObjData._currentWeaponAttachDamageLevel = _data._currentWeaponAttachDamageLevel;
      //spawnedObjData._currentWeaponRandomKnockBackLevel = _data._currentWeaponRandomKnockBackLevel;
    }
  }

  public Vector3 GetWeaponSize()
  {
    UpdateWeaponIndex();
    if(_data._currentSkillLevel >= (int)(Constants.CareerLevel.CareerLevel2 - 1))
      return _weaponSizeForLevel2[m_weaponIndex];
    else
      return _weaponSizeForLevel1[m_weaponIndex];
  }

  public void CheckWeapon()
  {
    UpdateWeaponIndex();
    m_objToSpawn = _weapons[m_weaponIndex];
    m_shouldAttachWeapon = _shouldAttachWeapon[m_weaponIndex];
  }

  public void CheckIndicator()
  {
    UpdateWeaponIndex();
    m_indicator = _indicators[m_weaponIndex];
  }

  // Called by broadcast message
  public void OnSkillUp()
  {
    // When the skill level is up, check if we should increase camera size 
    if(_cameraData)
      ProcessCameraSize(GetDefaultCameraSize(), 0);
  }

  public virtual void ProcessCameraSize(float size, float time)
  {
    if(m_cameraCR != null)
    {
      StopCoroutine(m_cameraCR);
      m_cameraCR = null;
    }
    m_cameraCR = StartCoroutine(UpdateCameraSize(size, time));
  }

  public void DoAttackSFX()
  {
    if(_audio && _attackClip)
      _audio.PlayOneShot(_attackClip);
  }

  void Start()
  {
    CheckWeapon();
    CheckIndicator();
  }

  void OnEnable()
  {
    m_defaultCameraSize = Camera.main.orthographicSize;
    if(_cameraData)
      ProcessCameraSize(GetDefaultCameraSize(), 0);
  }

  void Update()
  {
    if(m_indicator == null)
      return;
    
    // Show indicator when energy is full 
    if(_data._currentEnergy >= _data.MaxEnergy)
    {
      //Debug.Log("full: " + gameObject.name);
      CheckIndicator();

      if(!m_indicator.activeSelf)
      {
        m_indicator.SetActive(true);
        // Udpate camera size
        // AI has no camera
        if(_cameraData)
          ProcessCameraSize(GetCameraSize(), 0);
      }
    }
    else
    {
      //Debug.Log("Not full: " + gameObject.name);
      if(m_indicator.activeSelf)
      {
        m_indicator.SetActive(false);
        // AI has no camera 
        if(_cameraData)
          ProcessCameraSize(GetDefaultCameraSize(), _cameraData._cameraResetDelayForUltimate[m_weaponIndex]);
      }
    }
  }

  private void UpdateWeaponIndex()
  {
    if(_data._currentSkillLevel >= (int)(Constants.CareerLevel.CareerLevel1 - 1))
    {
      for(int i = 0; i < _weaponTypes.Count; ++i)
      {
        if(_weaponTypes[i] == _data._careerType)
          m_weaponIndex = i;
      }
    }
  }

  private float GetCameraSize()
  {
    if(_data._currentSkillLevel >= (int)(Constants.CareerLevel.CareerLevel2 - 1))
      return m_defaultCameraSize * _cameraData._cameraSizeForLevel10[m_weaponIndex] * _cameraData._cameraSizeRatioForWeaponActiveTimeLevel[_data._currentWeaponActiveTimeLevel];
    else
      return m_defaultCameraSize * _cameraData._cameraSizeForLevel20[m_weaponIndex] * _cameraData._cameraSizeRatioForWeaponActiveTimeLevel[_data._currentWeaponActiveTimeLevel];
  }

  private float GetDefaultCameraSize()
  {
    return m_defaultCameraSize * _cameraData._cameraSizeRatioForWeaponActiveTimeLevel[_data._currentWeaponActiveTimeLevel];
  }
}