﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class AttackController : ActionBase
{
  public GameObject _attackObj;
  public Transform _spawnTrans;

  public override void DoAttack()
  {
    // Spawn the attack obj
    GameObject spawnedObj = LeanPool.Spawn(_attackObj, _spawnTrans.position, _spawnTrans.rotation);
    // Assign the team to spawned obj
    SharedData spawnedObjData = spawnedObj.GetComponentInChildren<SharedData>();
    if(spawnedObjData)
    {
      spawnedObjData._team = _data._team;
      spawnedObjData._expController = _data._expController;
      spawnedObjData._scoreController = _data._scoreController;
    }

  }

}
