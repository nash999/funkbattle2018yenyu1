﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class DamageUIController : MonoBehaviour
{
  public GameObject _obj;
  public GameObject _uiRoot;
  public ObjectShaker _cameraShake;

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kDamageAppliedNotification, OnDamageApplied);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kDamageAppliedNotification, OnDamageApplied);
  }

  private void OnDamageApplied(Hashtable data)
  {
    // Get obj
    GameObject go = (GameObject)data[Constants.kCharacterKey];
    Color c = (Color)data[Constants.kColorKey];
    string output = (string)data[Constants.kDamageOutputKey];
    // Get damage total
    GameObject spawnObj = LeanPool.Spawn(_obj, Constants.kVector3Zero, Constants.kQuaternionIdentity, _uiRoot.transform);

    DamageUICellController cell = spawnObj.GetComponentInChildren<DamageUICellController>();
    cell.UpdateUI(output, c);

    UIFollower follower = spawnObj.GetComponent<UIFollower>();
    follower.StartFollowing(go);

    // Only damage should shake
    if(c != Color.red)
      return;
    ShakeCamera(go);

  }

  private void ShakeCamera(GameObject go)
  {

    //LifeBase lb = go.GetComponentInChildren<LifeBase>();
    //if(lb == null)
    //  return;
    // Only player has camera data
    CameraData cd = go.GetComponentInChildren<CameraData>();
    if(cd == null)
      return;
    PhotonView view = go.GetComponent<PhotonView>();
    if(view == null)
      return;
    if(!view.isMine)
      return;
    _cameraShake.StartShaking();
    //if(PhotonNetwor == lb._data._networkID
  }

}
