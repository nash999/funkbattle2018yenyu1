﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraPropertyController : MonoBehaviour
{
  public Camera _camera;
  public Slider _angleSlider;
  public Slider _sizeSlider;
  public Slider _xSlider;
  public Slider _ySlider;
  public Slider _zSlider;
  public Text _angleText;
  public Text _sizeText;
  public Text _xText;
  public Text _yText;
  public Text _zText;

  private Vector3 m_rot;
  private Vector3 m_pos;
  private float m_size;

  public void OnAngleSliderValueChange()
  {
    m_rot.x = _angleSlider.value;
    _angleText.text = _angleSlider.value.ToString();
    _camera.transform.rotation = Quaternion.Euler(m_rot);
  }

  public void OnSizeSliderValueChange()
  {
    m_size = _sizeSlider.value;
    _sizeText.text = _sizeSlider.value.ToString();
    _camera.orthographicSize = m_size;
  }

  public void OnXSliderValueChange()
  {
    m_pos.x = _xSlider.value;
    _xText.text = _xSlider.value.ToString();
    _camera.transform.position = m_pos;
    NotifyCamera();
  }

  public void OnYSliderValueChange()
  {
    m_pos.y = _ySlider.value;
    _yText.text = _ySlider.value.ToString();
    _camera.transform.position = m_pos;
    NotifyCamera();
  }

  public void OnZSliderValueChange()
  {
    m_pos.z = _zSlider.value;
    _zText.text = _zSlider.value.ToString();
    _camera.transform.position = m_pos;
    NotifyCamera();
  }

	// Use this for initialization
	void Start ()
  { 
    // If no camera is assigned 
    // We use the default one
    if(_camera == null)
      _camera = Camera.main;

    // Update slider value from scene
    m_pos = _camera.transform.position;
    m_rot = _camera.transform.rotation.eulerAngles;
    _angleSlider.value = m_rot.x;
    _angleText.text = _angleSlider.value.ToString();
    _sizeSlider.value = _camera.orthographicSize;
    _sizeText.text = _sizeSlider.value.ToString();
    _xSlider.value = m_pos.x;
    _xText.text = _xSlider.value.ToString();
    _ySlider.value = m_pos.y;
    _yText.text = _ySlider.value.ToString();
    _zSlider.value = m_pos.z;
    _zText.text = _zSlider.value.ToString();
	}

  private void NotifyCamera()
  {
    Hashtable data = new Hashtable();
    data[Constants.kCameraOffsetKey] = m_pos;
    Messenger.Broadcast(Constants.kUpdateCameraOffsetNotification, data);
  }
}