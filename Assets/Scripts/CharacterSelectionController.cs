﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectionController : MonoBehaviour
{
  public List<GameObject> _characters;
  public int _gap;
  public float _animatedTime = 0.5f;

  public void OnRightSelectionDown()
  {
    GameManager.instance._currentCharacterIndex++;
    GameManager.instance._currentCharacterIndex %= _characters.Count;

    SortCharacters(GameManager.instance._currentCharacterIndex);

  }

  public void OnLeftSelectionDown()
  {
    if(GameManager.instance._currentCharacterIndex == 0)
      GameManager.instance._currentCharacterIndex =  (_characters.Count-1);
    else
      GameManager.instance._currentCharacterIndex--;

    SortCharacters(GameManager.instance._currentCharacterIndex);
  }

  void OnEnable()
  {
    // Sort all characters positions
    SortCharacters(GameManager.instance._currentCharacterIndex);
    Messenger.AddListener(Constants.kCharacterSelectionNotifiation, OnCharSelect);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kCharacterSelectionNotifiation, OnCharSelect);
  }

  private void SortCharacters(int currentIndex)
  { 
    Debug.Log("Character selection index: " + GameManager.instance._currentCharacterIndex);
    for(int i = 0; i < _characters.Count; ++i)
    {
      RectTransform rt = _characters[i].GetComponent<RectTransform>();
      ;
      // Calculate the gap distance based on index 
      int gd = (i - currentIndex) * _gap;
      //Debug.Log("Move " + i + " dis: " + gd);
      LeanTween.moveX(rt, gd, _animatedTime).setEaseInOutBack();
      // Update animation
      if(i == currentIndex)
      {
        //Debug.Log(i + " active");
        _characters[i].BroadcastMessage(Constants.kActiveAnimationMS, SendMessageOptions.DontRequireReceiver);
      }
      else
      {
        //Debug.Log(i + " inactive");
        _characters[i].BroadcastMessage(Constants.kInactiveAnimationMS, SendMessageOptions.DontRequireReceiver);
      }
    }
  }

  private void OnCharSelect()
  {
    for(int i = 0; i < _characters.Count; ++i)
    {
      // Update animation
      if(i == GameManager.instance._currentCharacterIndex)
        _characters[i].BroadcastMessage(Constants.kChosenAnimationMS, SendMessageOptions.DontRequireReceiver);
      else
        _characters[i].BroadcastMessage(Constants.kNonChosenAnimationMS, SendMessageOptions.DontRequireReceiver);
    }
  }

}