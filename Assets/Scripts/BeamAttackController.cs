﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Forge3D;

public class BeamAttackController : BasicWeaponAttackController
{
  public float _beamScale; // Default beam scale to be kept over distance
  public float _maxBeamLength; // Maximum beam length
  public float _UVTime; // UV Animation speed
  public float _colliderSizeRatio = 0.7f; // Ratio relative to beam size 
  public bool _animateUV; // UV Animation
  public Transform _rayImpact; // Impact transform
  public Transform _rayMuzzle; // Muzzle flash transform
  public BoxCollider _collider; // Collider used for detecting hit objs
  public LineRenderer _lineRenderer; // Line rendered component
 
  private float m_beamLength; // Current beam length
  private float m_initialBeamOffset; // Initial UV offset 
  private float m_animateUVTime;
  private float m_attackInterval;

  public override void OnEnable()
  {
    base.OnEnable();
    // Randomize uv offset
    m_initialBeamOffset = Random.Range(0f, 5f);
  }

  void OnDisable()
  { 
    m_attackInterval = 0;
  }

  void Update()
  {
    // Animate texture UV
    if (_animateUV)
    {
      m_animateUVTime += Time.deltaTime;

      if (m_animateUVTime > 1.0f)
          m_animateUVTime = 0f;

      _lineRenderer.material.SetTextureOffset("_MainTex", new Vector2(m_animateUVTime * _UVTime + m_initialBeamOffset, 0f));
    }

    UpdateBeam();

    if(m_attackInterval > 0)
    {
      m_attackInterval -= Time.deltaTime;
      return;
    }
    m_attackInterval = _data.AttackInterval;
  }

  void OnTriggerEnter(Collider other)
  {
    // Do nothing, we process logic in on trigger stay
  }

  void OnTriggerStay(Collider other)
  {

    if(m_attackInterval > 0)
      return;

    if(!ApplyDamage(other.gameObject))
      return;
    //Debug.Log(_root.name +  " damage applied: " + other.name);
    ApplyExplosion(other);
  }

  // Hit point calculation
  private void UpdateBeam()
  { 
    // Calculate default beam proportion multiplier based on default scale and maximum length
    float propMult = _maxBeamLength*(_beamScale/10f);
   
    // Set beam to maximum length
    m_beamLength = _maxBeamLength;
    _lineRenderer.SetPosition(1, new Vector3(0f, 0f, m_beamLength));

    // Calcualte collider size and pos
    UpdateColliderTransform();

    // Adjust impact effect position
    if (_rayImpact)
      _rayImpact.position = transform.position + transform.forward*m_beamLength;
    

    // Adjust muzzle position
    if (_rayMuzzle)
      _rayMuzzle.position = transform.position + transform.forward*0.1f;

    // Set beam scaling according to its length
    _lineRenderer.material.SetTextureScale("_MainTex", new Vector2(propMult, 1f));
  }

  private void UpdateColliderTransform()
  {
    _collider.center = new Vector3(0,0,_maxBeamLength*0.5f);
    _collider.size = new Vector3(_lineRenderer.endWidth * _colliderSizeRatio,_lineRenderer.endWidth * _colliderSizeRatio,_maxBeamLength); 
  }
}