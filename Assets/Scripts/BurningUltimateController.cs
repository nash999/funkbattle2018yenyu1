﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class BurningUltimateController : BasicWeaponAttackController
{
  public GameObject _attackObj;
  public GameObject _spawnPos;
  public LayerMask _attackLayer;
  public float _dropInterval = 0.5f;
  public float _bodyAttackRadius = 4;
  private float m_currentDropInterval;
  private PlayerData m_ownerData;
  private float m_attackInterval;

	// Update is called once per frame
	void Update ()
  {
    UpdatePlayer();
    DropWeapon();	

    if(m_attackInterval > 0)
    {
      m_attackInterval -= Time.deltaTime;
      return;
    }
    m_attackInterval = _data.AttackInterval;
    CheckBodyAttack();
	}

  void OnDisable()
  {
    if(m_ownerData)
    {
      m_ownerData._forSpeedRatio = 1.0f;
      m_ownerData._rotSpeedRatio = 1.0f;
      m_ownerData = null;
    }

    m_attackInterval = 0;
  
  }

  public override void OnEnable()
  {
    base.OnEnable();
    m_currentDropInterval = _dropInterval;
  }

 
  void CheckBodyAttack()
  {
    Collider[] enemies = Physics.OverlapSphere(gameObject.transform.position, _bodyAttackRadius, _attackLayer, QueryTriggerInteraction.Collide);
    if(enemies.Length == 0)
      return;
    foreach(Collider c in enemies)
    {
      //Debug.Log("Burning body attack: " + c.name);
      if(ApplyDamage(c.gameObject))
        ApplyExplosion(c);
    }
  }

  private void DropWeapon()
  {
    m_currentDropInterval -= Time.deltaTime;
    if(m_currentDropInterval > 0)
      return;
    m_currentDropInterval = _dropInterval;
    SpawnWeapon();
  }

  private void UpdatePlayer()
  {
    if(m_ownerData == null)
    {
      m_ownerData = transform.root.GetComponentInChildren<PlayerData>();
      return;
    }
    m_ownerData._forSpeedRatio = _data._forSpeedRatio;
    m_ownerData._rotSpeedRatio = _data._rotSpeedRatio;
  }

  private void SpawnWeapon()
  {
    ProcessSpawnObj(_spawnPos.transform, _attackObj);
  }

  public void ProcessSpawnObj(Transform trans, GameObject obj)
  {
    GameObject spawnedObj = LeanPool.Spawn(obj, trans.position, trans.rotation);
    // Assign the team to spawned obj
    WeaponData spawnedObjData = spawnedObj.GetComponentInChildren<WeaponData>();
    if(spawnedObjData)
    {
      spawnedObjData._team = _data._team;
      spawnedObjData._expController = _data._expController;
      spawnedObjData._scoreController = _data._scoreController;
      spawnedObjData._ownerNetworkID = _data._ownerNetworkID;
      spawnedObjData._currentWeaponSuckBloodLevel = _data._currentWeaponSuckBloodLevel;
      spawnedObjData._currentWeaponBleedingLevel = _data._currentWeaponBleedingLevel;
      spawnedObjData._currentWeaponUltimateDamageLevel = _data._currentWeaponUltimateDamageLevel;
      spawnedObjData._currentWeaponActiveTimeLevel = _data._currentWeaponActiveTimeLevel;
      spawnedObjData._currentWeaponRegularDamageLevel = _data._currentWeaponRegularDamageLevel;
      spawnedObjData._currentWeaponDamageForceLevel = _data._currentWeaponDamageForceLevel;
      spawnedObjData._currentWeaponSlowSpeedLevel = _data._currentWeaponSlowSpeedLevel;
      spawnedObjData._currentWeaponAttachDamageLevel = _data._currentWeaponAttachDamageLevel;
      spawnedObjData._currentWeaponRandomKnockBackLevel = _data._currentWeaponRandomKnockBackLevel;
    }
  }
}