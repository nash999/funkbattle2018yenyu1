﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Gestures;

public class TouchController : MonoBehaviour
{
  public GameObject _touchManager;
  public TransformGesture _joystick;
  public FlickGesture _flickGesture;
  public TapGesture _tapGesture;
  public LongPressGesture _pressGesture;
  public ReleaseGesture _releaseGesture;
  public Vector2 _deltaPosition;

  private Vector2 m_centerPosition;
  private float m_radius = 1.0f;
  private bool m_isTransfroming = false;

  void OnEnable()
  {
    if(_joystick != null)
    {
      _joystick.Transformed += OnJoystickTransform;
      _joystick.TransformStarted += OnJoystickTransformStart;
      _joystick.TransformCompleted += OnJoystickTransformComplete;
      _joystick.StateChanged += OnJoystickStateChange;
      //_joystick.
    }
    if(_flickGesture)
    {
      _flickGesture.Flicked += OnFlick;
    }
    if(_tapGesture)
    {
      _tapGesture.Tapped += OnTap;
    }
    if(_pressGesture)
    {
      _pressGesture.StateChanged += OnPressStateChange;
      //_pressGesture.Cancelled += OnPressStateChange;
      _pressGesture.LongPressed += OnPress;//
      //_pressGesture.
    }
    if(_releaseGesture)
    {
      _releaseGesture.Released += OnRelease;
    }

  }

  void OnDisable()
  {
    if(_joystick != null)
    {
      _joystick.Transformed -= OnJoystickTransform;
      _joystick.TransformStarted -= OnJoystickTransformStart;
      _joystick.TransformCompleted -= OnJoystickTransformComplete;
      _joystick.StateChanged -= OnJoystickStateChange;
    }
    if(_flickGesture)
    {
      _flickGesture.Flicked -= OnFlick;
    }
    if(_tapGesture)
    {
      _tapGesture.Tapped -= OnTap;
    }
    if(_pressGesture)
    {
      _pressGesture.StateChanged -= OnPressStateChange;
      _pressGesture.LongPressed -= OnPress;

    }
    if(_releaseGesture)
    {
      _releaseGesture.Released -= OnRelease;
    }

  }

  private void OnJoystickTransformStart(object sender, System.EventArgs e)
  {
    //Debug.Log("Touch Start: ");
    m_centerPosition = _joystick.ScreenPosition;
    //m_centerPosition = new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
  }

  private void OnJoystickTransformComplete(object sender, System.EventArgs e)
  {
    //Debug.Log("Touch complete: ");
    //_deltaPosition = Vector2.zero;


    //Hashtable data = new Hashtable();
    //data[Constants.kJoystickTransformKey] = _deltaPosition;
   
    //Messenger.Broadcast(Constants.kJoystickTransformNotification, data);
    Messenger.Broadcast(Constants.kJoystickTransformCompleteNotification);
   // Debug.Log("OnJoystickTransformComplete: " + _deltaPosition);
    m_isTransfroming = false;
  }

  private void OnJoystickTransform(object sender, System.EventArgs e)
  {
    Hashtable data = new Hashtable();

    //Debug.Log("Touch Start: " + _joystick.ScreenPosition);
    Vector2 offset = (_joystick.ScreenPosition - m_centerPosition);
    //_deltaPosition = Vector2.ClampMagnitude(offset, m_radius); 
    //Debug.Log("position " + _deltaPosition);
    //data[Constants.kJoystickTransformKey] = _deltaPosition;
    data[Constants.kJoystickTransformKey] = offset;
    //data[Constants.kJoystickTransformKey] = _joystick.ScreenPosition;

    Messenger.Broadcast(Constants.kJoystickTransformNotification, data);
    m_isTransfroming = true;

  }

  private void OnFlick(object sender, System.EventArgs e)
  {
    //Debug.Log("OnFlick: " + _flickGesture.ScreenFlickVector.normalized);
    Hashtable data = new Hashtable();
    data[Constants.kJoystickFlickKey] = _flickGesture.ScreenFlickVector.normalized;

    Messenger.Broadcast(Constants.kJoystickFlickNotification, data);
  }

  private void OnTap(object sender, System.EventArgs e)
  {
    //Debug.Log("OnTap");
    Hashtable data = new Hashtable();

    data[Constants.kJoystickTapKey] = _tapGesture.ScreenPosition;

    //Debug.Log("Tap position: " + (_tapGesture.NormalizedScreenPosition - new Vector2(0.5f, 0.5f)));
    Messenger.Broadcast(Constants.kJoystickTapNotification, data);
  }

  private void OnPress(object sender, System.EventArgs e)
  {
    //Debug.Log("OnPress");
    // When user is moving, we don't invoke press logic
    if(m_isTransfroming)
      return;
    Messenger.Broadcast(Constants.kJoystickPressNotification);
  }

  private void OnRelease(object sender, System.EventArgs e)
  {
    //Debug.Log("OnRelease");
    Messenger.Broadcast(Constants.kJoystickReleaseNotification);
  }

  private void OnPressStateChange(object sender, System.EventArgs e)
  {
    //Debug.Log("OnPressStateChange: " + _pressGesture.State);
  }

  private void OnJoystickStateChange(object sender, System.EventArgs e)
  {
    //Debug.Log("OnJoystickStateChange: " + _joystick.State);
  }

}