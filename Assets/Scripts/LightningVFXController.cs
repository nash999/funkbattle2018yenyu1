﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Forge3D;

public class LightningVFXController : MonoBehaviour
{
  //public LayerMask layerMask;

  public GameObject _target; 
  public Texture[] _beamFrames; // Animation frame sequence
  public float _frameStep; // Animation time
  public bool _randomizeFrames; // Randomization of animation frames
  public int _points; // How many points should be used to construct the beam
  public float _maxBeamLength; // Maximum beam length
  public float _beamScale; // Default beam scale to be kept over distance
  public bool _animateUV; // UV Animation
  public float _UVTime; // UV Animation speed
  public Transform _rayImpact; // Impact transform
  public Transform _rayMuzzle; // Muzzle flash transform
  public LineRenderer _lineRenderer; // Line rendered component

  private int m_frameNo; // Frame counter
  private int m_frameTimerID; // Frame timer reference
  private float m_beamLength; // Current beam length
  private float m_initialBeamOffset; // Initial UV offset

  void Awake()
  {
    // Assign first frame texture
    if(!_animateUV && _beamFrames.Length > 0)
      _lineRenderer.material.mainTexture = _beamFrames[0];

    // Randomize uv offset
    m_initialBeamOffset = Random.Range(0f, 5f);
  }

  // OnSpawned called by pool manager
  void OnEnable()
  {
    // Start animation sequence if beam frames array has more than 2 elements
    if(_beamFrames.Length > 1)
      Animate();
  }

  // OnDespawned called by pool manager
  void OnDisable()
  {
    // Reset frame counter
    m_frameNo = 0;

    // Clear frame animation timer
    if(m_frameTimerID != -1)
    {
      F3DTime.time.RemoveTimer(m_frameTimerID);
      m_frameTimerID = -1;
    }
  }

  void UpdateAttackLength()
  {
    if(_target == null)
      return;


    // Calculate default beam proportion multiplier based on default scale and maximum length
    float propMult = _maxBeamLength * (_beamScale / 10f);

    {
      // Get current beam length
      m_beamLength = Vector3.Distance(transform.position, _target.transform.position);

      _lineRenderer.SetPosition(0, transform.parent.position);
      _lineRenderer.SetPosition(1, _target.transform.position);

      // Calculate default beam proportion multiplier based on default scale and current length
      propMult = m_beamLength * (_beamScale / 10f);

      // Adjust impact effect position
      if(_rayImpact)
        _rayImpact.position = _target.transform.position - transform.forward * 0.5f;
    }

    // Adjust muzzle position
    if(_rayMuzzle)
      _rayMuzzle.position = transform.position + transform.forward * 0.1f;
    
    // Set beam scaling according to its length
    _lineRenderer.material.SetTextureScale("_MainTex", new Vector2(propMult, 1f));

  }

  // Advance texture frame
  void OnFrameStep()
  {
    // Randomize frame counter
    if(_randomizeFrames)
      m_frameNo = Random.Range(0, _beamFrames.Length);

    // Set current texture frame based on frame counter
    _lineRenderer.material.mainTexture = _beamFrames[m_frameNo];
    m_frameNo++;

    // Reset frame counter
    if(m_frameNo == _beamFrames.Length)
      m_frameNo = 0;
  }

  // Initialize frame animation
  void Animate()
  {
    // Set current frame
    m_frameNo = 0;
    _lineRenderer.material.mainTexture = _beamFrames[m_frameNo];

    // Add timer 
    m_frameTimerID = F3DTime.time.AddTimer(_frameStep, OnFrameStep);

    m_frameNo = 1;
  }

  void Update()
  {
    // Animate texture UV
    if(_animateUV)
      _lineRenderer.material.SetTextureOffset("_MainTex", new Vector2(Time.time * _UVTime + m_initialBeamOffset, 0f));

    // Process raycasting 
    UpdateAttackLength();
  }
}