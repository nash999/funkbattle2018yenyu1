﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class OverlapSphereAttackController : BasicWeaponAttackController
{
  public float _attackRadius = 10.0f;
  public LayerMask _attackLayer;

  private float m_attackInterval;

  public override void OnEnable()
  {
    base.OnEnable();
  }

  void OnDisable()
  { 
    m_attackInterval = 0;
  }

  void Update()
  {
    if(m_attackInterval > 0)
    {
      m_attackInterval -= Time.deltaTime;
      return;
    }

    m_attackInterval = _data.AttackInterval;

    // Find targets
    Collider[] targets = Physics.OverlapSphere(transform.position, _attackRadius, _attackLayer, QueryTriggerInteraction.Collide);
    foreach(Collider c in targets)
    {
      PlayerData pData = c.transform.root.GetComponentInChildren<PlayerData>();
      // Skip owner
      if(pData && pData._networkID == _data._ownerNetworkID)
        continue;
      
      if(ShouldBlockLayer(c))
        continue;
      CheckSkipLayer(c);

      ApplyDamage(c.gameObject);
      ApplyExplosion(c);
    }

    if(_data._currentLife <= 0)
    {
      // Show explosion
      DoDespawn();
    }
  }
}