﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class AvatarSkillUIController : MonoBehaviour
{
  public PhotonView _view;
  public PlayerData _data;
  public LevelExpData _exp;
  public NetworkPlayerDataInit _dataInit;

  public Vector3 _inPos;
  public Vector3 _outPos = new Vector3(0,300,0);
  public RectTransform _buttonsRoot;
  public Button _leftButton;
  public Button _middleButton;
  public Button _rightButton;
  public Text _leftButtonText;
  public Text _middleButtonText;
  public Text _rightButtonText;
  public List<Image> _leftSkillLevel;
  public List<Image> _middleSkillLevel;
  public List<Image> _rightSkillLevel;

  public float _animatedTime = 0.5f;

  public Sprite _skillOnSprite;
  public Sprite _skillOffSprite;

  public List<Constants.CareerType> _types;
  public List<Constants.LevelButtonPosition> _buttonPos;
  public string _specialSkillDesc;
  public AudioSource _audio;
  public AudioClip _selectClip;

  //private bool m_isAnimationRunning = false;
  private List<int> m_randomIndexes = new List<int>();
  private bool m_isUIOn = false;

  public void OnLeftButtonDown()
  {
    UpdateLevel(Constants.LevelButtonPosition.Left);
    DoOutAnimation();

  }

  public void OnMiddleButtonDown()
  {
    UpdateLevel(Constants.LevelButtonPosition.Middle);
    DoOutAnimation();
  }

  public void OnRightButtonDown()
  {
    UpdateLevel(Constants.LevelButtonPosition.Right);
    DoOutAnimation();
  }

	public void CheckLevel()
  {
    // Skip if we are not local player
    if(!_view.isMine)
      return;
    // Skip if the level is already updated
    if(_data._currentSkillLevel >= _data._currentPlayerLevel)
    {
      //Debug.Log("Max level");
      //Debug.Log("skill level " + _data._currentSkillLevel);
      //Debug.Log("player level: " + _data._currentPlayerLevel);
      return;
    }
    // Last 2 level don't show 
    if(_data._currentSkillLevel >= _data._maxLevel - 2)
      return;
    // Skip if root is active
    if(m_isUIOn)
      return;
    m_isUIOn = true;
    // Active root
    _buttonsRoot.gameObject.SetActive(true);
    // Increase level
    _data._currentSkillLevel++;
    //Debug.Log("Show Skill UI for skill level: " + _data._currentSkillLevel);
    // Update content
    UpdateUI();
    DoInAnimation();
  }

  public virtual void UpdateSpecial()
  {
    _data._currentWeaponBleedingLevel++;
  }

  public virtual int UpdateSpecialLevel()
  {
    return _data._currentWeaponBleedingLevel;
  }

  public virtual void ChangeRandom(Constants.LevelButtonPosition buttonIndex, int level)
  {
    //Debug.Log("update random index: " + buttonIndex);
    //Debug.Log("type: " + GameManager.instance._randomTypes[m_randomIndexes[(int)buttonIndex]]);
    switch(GameManager.instance._randomTypes[m_randomIndexes[(int)buttonIndex]])
    {
      case Constants.RandomSkillType.SlowSpeed:
        _data._currentWeaponSlowSpeedLevel++;
        break;

      case Constants.RandomSkillType.RandomKnockBack:
        _data._currentWeaponRandomKnockBackLevel++;
        break;

      case Constants.RandomSkillType.RandomFastAttack:
        _data._currentFastAttackLevel++;
        break;

      case Constants.RandomSkillType.RandomDodge:
        _data._currentDodgeLevel++;
        break;

      case Constants.RandomSkillType.Magnet:
        _data._currentMagnetLevel++;
        break;
    }
  }

  public virtual void ChangeCareer(Constants.CareerLevel level)
  {
  }

  public virtual void ChangeWeapon(Constants.LevelButtonPosition pos)
  {
    for(int i = 0; i < _types.Count; ++i)
    {
      if(pos == _buttonPos[i])
      {
        _data._careerType = _types[i];
        //Debug.Log("Change weapon to: " + _data._careerType);
      }
    }
  }

  public virtual string GetSpecialUIString()
  {
    return _specialSkillDesc;
  }

  public virtual string GetCareerUIString(Constants.CareerLevel level)
  {
    return GameManager.instance.GetCareerDesc(_data._careerType, level);
  }

  public Constants.CareerType GetCareerTypeByButtonPos(Constants.LevelButtonPosition pos)
  {
    for(int i = 0; i < _buttonPos.Count; ++i)
    {
      if(pos == _buttonPos[i])
      {
        return _types[i];
      }
    }
    // Shouldn't come here
    return Constants.CareerType.Archer;
  }

  private void RefreshRandomSkillIndex()
  {
    m_randomIndexes.Clear();

    for(int i = 0; i < 3; ++i)
    {
      int newIndex = -1;
      do
      {
        newIndex = UnityEngine.Random.Range(0, GameManager.instance._randomTypes.Count);
      }
      while(m_randomIndexes.Contains(newIndex));
      m_randomIndexes.Add(newIndex);
    }
  }

  private string GetRandomSkillDesc(int index)
  {
    return GameManager.instance._randomSkillsDesc[index];
  }

  private int GetRandomSkillLevel(Constants.RandomSkillType type)
  {
    int skillLevel = 0;

    switch(type)
    {
      case Constants.RandomSkillType.Magnet:
        skillLevel = _data._currentMagnetLevel;
        break;

      case Constants.RandomSkillType.RandomDodge:
        skillLevel = _data._currentDodgeLevel;
        break;

      case Constants.RandomSkillType.RandomFastAttack:
        skillLevel = _data._currentFastAttackLevel;
        break;

      case Constants.RandomSkillType.RandomKnockBack:
        skillLevel = _data._currentWeaponRandomKnockBackLevel;
        break;

      case Constants.RandomSkillType.SlowSpeed:
        skillLevel = _data._currentWeaponSlowSpeedLevel;
        break;
    }
    //Debug.Log("Get skill level: " + skillLevel);
    return skillLevel;
  }

  private void UpdateRandomUI()
  {
    RefreshRandomSkillIndex();

    UpdateRandomButtonsInfo();
  }

  void Start()
  {
    // Move out by default
    DoOutAnimation();
  }

  private void DoInAnimation()
  {
    //Debug.Log("Move in");
    LeanTween.move(_buttonsRoot, _inPos, _animatedTime).setEaseOutBounce();
  }

  private void DoOutAnimation()
  {
    //Debug.Log("Move out");
    LeanTween.move(_buttonsRoot, _outPos, _animatedTime).setOnComplete(OnOutAnimationEnd).setEaseOutBack();
  }

  private void UpdateLevel(Constants.LevelButtonPosition buttonIndex)
  {
    //Debug.Log("Update skill level");
    if(_data._currentSkillLevel > _data._currentPlayerLevel)
    {
      //Debug.Log("Max level");
      return;
    }
    switch(_data._currentSkillLevel)
    {
      // Type A properties
      case 1:
      case 6:
      case 11:
      case 15:
      case 21:
      case 25:
        {
          
          switch(buttonIndex)
          {
            case Constants.LevelButtonPosition.Left:
              _data._currentAttackIntervalLevel++;
              break;
            case Constants.LevelButtonPosition.Middle:
              _data._currentMaxLifeLevel++;
              break;
            case Constants.LevelButtonPosition.Right:
              _data._currentWeaponActiveTimeLevel++;
              break;
          }
        }
        break;
    // Type B properties
      case 2:
      case 7:
      case 12:
      case 16:
      case 22:
      case 26:
        {
          switch(buttonIndex)
          {
            case Constants.LevelButtonPosition.Left:
              _data._currentWeaponRegularDamageLevel++;
              break;
            case Constants.LevelButtonPosition.Middle:
              _data._currentWeaponUltimateDamageLevel++;
              break;
            case Constants.LevelButtonPosition.Right:
              UpdateSpecial();
              break;
          }
        }
        break;
    // Type C Properties
      case 3:
      case 8:
      case 13:
      case 17:
      case 23:
      case 27:
        {
          
          switch(buttonIndex)
          {
            case Constants.LevelButtonPosition.Left:
              _data._currentWeaponSuckBloodLevel++;
              break;
            case Constants.LevelButtonPosition.Middle:
              _data._currentDefenseLevel++;
              break;
            case Constants.LevelButtonPosition.Right:
              _data._currentForwardSpeedLevel++;
              break;
          }
        }
        break;
    // Weapon properties
      case 4:
        {
          ChangeWeapon(buttonIndex);
        }
        break;
    // Random properties
      case 5:
      case 10:
      case 14:
      case 18:
      case 20:
      case 24:
        {
          ChangeRandom(buttonIndex, _data._currentSkillLevel);
        }
        break;
    // Career properties
      case 9:
        {
          ChangeCareer(Constants.CareerLevel.CareerLevel1);
        }
        break;
      case 19:
        {
          ChangeCareer(Constants.CareerLevel.CareerLevel2);
        }
      break;
    }

    _dataInit.SaveData();
    // Send out skill level up notification
    _data._root.BroadcastMessage(Constants.kSkillUpMS, SendMessageOptions.DontRequireReceiver);
    if(_audio && _selectClip)
      _audio.PlayOneShot(_selectClip);

    // UA Event
    Analytics.CustomEvent(Constants.kPlayerLevelUpUAE, new Dictionary<string, object>
    {
      { "Player Level", _data._currentPlayerLevel},
      { "Skill Level", _data._currentSkillLevel},
      { "Attack Level", _data._currentAttackIntervalLevel},
      { "Defense Level", _data._currentDefenseLevel},
      { "Dodge Level", _data._currentDodgeLevel},
      { "FastAttack Level", _data._currentFastAttackLevel},
      { "ForwardSpeed Level", _data._currentForwardSpeedLevel},
      { "Magnet Level", _data._currentMagnetLevel},
      { "MaxLife Level", _data._currentMaxLifeLevel},
      { "WeaponActiveTime Level", _data._currentWeaponActiveTimeLevel},
      { "WeaponBleeding Level", _data._currentWeaponBleedingLevel},
      { "WeaponRandomKnockBack Level", _data._currentWeaponRandomKnockBackLevel},
      { "WeaponRegularDamage Level", _data._currentWeaponRegularDamageLevel},
      { "WeaponSlowSpeed Level", _data._currentWeaponSlowSpeedLevel},
      { "WeaponSuckBlood Level", _data._currentWeaponSuckBloodLevel},
      { "WeaponUltimateDamage Level", _data._currentWeaponUltimateDamageLevel}
    });
  }

  private void UpdateUI()
  {
    // Update buttons content based on current level
    // There are 3 types 
    switch(_data._currentSkillLevel)
    {
      // Type A properties
      case 1:
      case 6:
      case 11:
      case 15:
      case 21:
      case 25:
        {
          // Type A
          UpdateRegularSkillButtonsInfo
          (
            Constants.RegularSkillType.RegularAttackSpeed,
            Constants.RegularSkillType.MaxLife,
            Constants.RegularSkillType.RegularAttackRange,
            _data._currentAttackIntervalLevel,
            _data._currentMaxLifeLevel,
            _data._currentWeaponActiveTimeLevel
          );

        }
        break;
      // Type B properties
      case 2:
      case 7:
      case 12:
      case 16:
      case 22:
      case 26:
        {
          // Type B
          UpdateRegularSkillButtonsInfo
          (
            Constants.RegularSkillType.RegularAttackDamage,
            Constants.RegularSkillType.UltimateAttackDamage,
            Constants.RegularSkillType.Bleed,
            _data._currentWeaponRegularDamageLevel, 
            _data._currentWeaponUltimateDamageLevel,
            UpdateSpecialLevel()
          );

        }
        break;
      // Type C Properties
      case 3:
      case 8:
      case 13:
      case 17:
      case 23:
      case 27:
        {
          // Type C
          UpdateRegularSkillButtonsInfo
          (
            Constants.RegularSkillType.SuckBlood,
            Constants.RegularSkillType.Defense,
            Constants.RegularSkillType.MoveSpeed,
            _data._currentWeaponSuckBloodLevel,
            _data._currentDefenseLevel,
            _data._currentForwardSpeedLevel
          );

        }
        break;
    // Weapon properties
      case 4:
        {
          UpdateWeaponButtonsInfo();
        }
        break;
    // Random properties
      case 5:
      case 10:
      case 14:
      case 18:
      case 20:
      case 24:
        {
          UpdateRandomUI();
        }
        break;
    // Career properties
      case 9:
        {
          // Career advance
          UpdateCareerButtonsInfo(Constants.CareerLevel.CareerLevel1);
        }
      break;
      case 19:
        {
          // Career advance
          UpdateCareerButtonsInfo(Constants.CareerLevel.CareerLevel2);
        }
      break;
    }
  }

  private void OnOutAnimationEnd()
  {
    _buttonsRoot.gameObject.SetActive(false);
    m_isUIOn = false;
    // Check level again to make sure if we can still level up
    CheckLevel();
  }

  private void UpdateRandomButtonsInfo()
  {
    Constants.RandomSkillType lType = GameManager.instance._randomTypes[m_randomIndexes[0]];
    Constants.RandomSkillType mType = GameManager.instance._randomTypes[m_randomIndexes[1]];
    Constants.RandomSkillType rType = GameManager.instance._randomTypes[m_randomIndexes[2]];

    UpdateButtonsInfo
    (
      GameManager.instance.GetRandomSkillDesc(lType),
      GameManager.instance.GetRandomSkillDesc(mType),
      GameManager.instance.GetRandomSkillDesc(rType),
      GameManager.instance.GetRandomSkillButtonNormal(lType),
      GameManager.instance.GetRandomSkillButtonNormal(mType),
      GameManager.instance.GetRandomSkillButtonNormal(rType),
      GameManager.instance.GetRandomSkillButtonSelect(lType),
      GameManager.instance.GetRandomSkillButtonSelect(mType),
      GameManager.instance.GetRandomSkillButtonSelect(rType),
      GetRandomSkillLevel(lType),
      GetRandomSkillLevel(mType),
      GetRandomSkillLevel(rType),
      true, true, true,
      true, true, true
    );
  }

  private void UpdateCareerButtonsInfo(Constants.CareerLevel level)
  {
    UpdateButtonsInfo
    (
      "", GetCareerUIString(level), "",
      null,
      GameManager.instance.GetCareerButtonNormal(_data._careerType),
      null,
      null,
      GameManager.instance.GetCareerButtonSelect(_data._careerType),
      null,
      0,0,0,
      false, true, false,
      false, false, false,
      1.5f, 1.5f, 1.5f
    );
  }

  private void UpdateWeaponButtonsInfo()
  {
    Constants.CareerType lType = GetCareerTypeByButtonPos(Constants.LevelButtonPosition.Left);
    Constants.CareerType mType = GetCareerTypeByButtonPos(Constants.LevelButtonPosition.Middle);
    Constants.CareerType rType = GetCareerTypeByButtonPos(Constants.LevelButtonPosition.Right);

    UpdateButtonsInfo
    (
      GameManager.instance.GetWeaponDesc(lType),
      GameManager.instance.GetWeaponDesc(mType),
      GameManager.instance.GetWeaponDesc(rType),
      GameManager.instance.GetWeaponButtonNormal(lType),
      GameManager.instance.GetWeaponButtonNormal(mType),
      GameManager.instance.GetWeaponButtonNormal(rType),
      GameManager.instance.GetWeaponButtonSelect(lType),
      GameManager.instance.GetWeaponButtonSelect(mType),
      GameManager.instance.GetWeaponButtonSelect(rType),
      0, 0, 0,
      true, true, true,
      false, false, false
    );
  }

  private void UpdateRegularSkillButtonsInfo
  (
    Constants.RegularSkillType lType,
    Constants.RegularSkillType mType,
    Constants.RegularSkillType rType,
    int leftLevel, int middleLevel, int rightLevel
  )
  {
    UpdateButtonsInfo
    (
      GameManager.instance.GetRegularSkillDesc(lType),
      GameManager.instance.GetRegularSkillDesc(mType),
      GameManager.instance.GetRegularSkillDesc(rType),
      GameManager.instance.GetRegularSkillButtonNormal(lType),
      GameManager.instance.GetRegularSkillButtonNormal(mType),
      GameManager.instance.GetRegularSkillButtonNormal(rType),
      GameManager.instance.GetRegularSkillButtonSelect(lType),
      GameManager.instance.GetRegularSkillButtonSelect(mType),
      GameManager.instance.GetRegularSkillButtonSelect(rType),
      leftLevel, middleLevel, rightLevel,
      true, true, true,
      true, true, true
    );
  }

  private void UpdateButtonsInfo
  (
    string lText, string mText, string rText,
    Sprite lNormalSprite, Sprite mNormalSprite, Sprite rNormalSprite,
    Sprite lSelectSprite, Sprite mSelectSprite, Sprite rSelectSprite,
    int leftLevel, int middleLevel, int rightLevel,
    bool showLButton, bool showMButton, bool showRButton,
    bool showLLevel, bool showMLevel, bool showRLevel,
    float lButtonSize = 1, float mButtonSize = 1, float rButtonSize = 1)
  {
    _leftButtonText.text = lText;
    _middleButtonText.text = mText;
    _rightButtonText.text = rText;

    _leftButton.gameObject.SetActive(showLButton);
    _middleButton.gameObject.SetActive(showMButton);
    _rightButton.gameObject.SetActive(showRButton);
    _leftButton.transform.localScale = new Vector3(1,1,1) * lButtonSize;
    _middleButton.transform.localScale = new Vector3(1,1,1) * mButtonSize;
    _rightButton.transform.localScale = new Vector3(1,1,1) * rButtonSize;

    if(showLButton)
    {
      SpriteState left = _leftButton.spriteState;
      _leftButton.image.sprite = lNormalSprite;
      left.pressedSprite = lSelectSprite;
      _leftButton.spriteState = left;
    }

    if(showMButton)
    {
      SpriteState middle = _middleButton.spriteState;
      _middleButton.image.sprite = mNormalSprite;
      middle.pressedSprite = mSelectSprite;
      _middleButton.spriteState = middle;
    }

    if(showRButton)
    {
      SpriteState right = _rightButton.spriteState;
      _rightButton.image.sprite = rNormalSprite;
      right.pressedSprite = rSelectSprite;
      _rightButton.spriteState = right;
    }

    UpdateSkillLevelUI(_leftSkillLevel, leftLevel, showLLevel);
    UpdateSkillLevelUI(_middleSkillLevel, middleLevel, showMLevel);
    UpdateSkillLevelUI(_rightSkillLevel, rightLevel, showRLevel);
  }

  private void UpdateSkillLevelUI(List<Image> list, int currentLevel, bool showLevel)
  {
    for(int i = 0; i < list.Count; ++i)
    {
      Image b = list[i];
      if(i < currentLevel)
      {
        b.sprite = _skillOnSprite;
      }
      else
      {
        b.sprite = _skillOffSprite;
      }
      b.gameObject.SetActive(showLevel);
    }
  }
}