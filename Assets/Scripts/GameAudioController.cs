﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudioController : MonoBehaviour
{
  public AudioSource _audio;
  public AudioClip _gameOverClip;
  public AudioClip _gameStartClip;

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGameOverNotification, DoGameOverClip);
    Messenger.AddListener(Constants.kCharacterSelectionNotifiation, DoGameStartClip);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGameOverNotification, DoGameOverClip);
    Messenger.AddListener(Constants.kCharacterSelectionNotifiation, DoGameStartClip);
  }

  private void DoGameOverClip()
  {
    _audio.PlayOneShot(_gameOverClip);
  }

  private void DoGameStartClip()
  {
    _audio.PlayOneShot(_gameStartClip);
  }
}
