﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangerUltimateController : MonoBehaviour
{
  public PlayerData _data;
  public float _detectRadiusLevel1 = 15;
  public float _detectRadiusLevel2 = 18;
  public LayerMask _layermask;
  public GameObject _target;

  private float m_updateInterval;

	// Update is called once per frame
	void Update()
  {
    if(_data._careerType != Constants.CareerType.Ranger)
      return;
    if(_data._currentLife <= 0)
      return;
    CheckTarget();
    if(_data._currentEnergy != _data.MaxEnergy)
      return;
    if(_target)
    {
      // When we are faceing target, disable the input so the turning won't affect the face logic
      FaceTarget();
    }

    if(m_updateInterval > 0)
    {
      m_updateInterval -= Time.deltaTime;
      return;
    }
    m_updateInterval = _data.AttackInterval;

    FindTarget(); 
	}
  
  private void FaceTarget()
  {
    // Determine the target rotation.  This is the rotation if the transform looks at the target point.
    Quaternion targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
    _data._rot = targetRotation;
  }

  private void CheckTarget()
  {

    if(_data._inputKey == Constants.KeyType.Up)
    {
      if(_target != null)
        _target = null;
    }
    if(_target == null)
      return;
    // Skip no life
    LifeBase otherLife = _target.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return;
    // Skip dead obj
    if(otherLife._data._currentLife <= 0)
    {
      _target = null;
    }
  }

  private void FindTarget()
  {
    float radius = _detectRadiusLevel1;
    switch(_data._currentSkillLevel)
    {
      case (int)Constants.CareerLevel.CareerLevel1:
        radius = _detectRadiusLevel1;
        break;
      case (int)Constants.CareerLevel.CareerLevel2:
        radius = _detectRadiusLevel2;
        break;
    }
    // Find objects within detectable range 
    Collider[] targets = Physics.OverlapSphere(transform.position, radius, _layermask, QueryTriggerInteraction.Collide);
    //Debug.Log("Length: " + targets.Length + " radius: " + radius);
    foreach(Collider t in targets)
    {
      CheckCollider(t);
    }
    if(targets.Length == 0)
      _target = null;
  }

  private void CheckCollider(Collider other)
  {
    //Debug.Log("Check collider: " + other.name);
    if(other.tag != Constants.kPlayerTag && other.tag != Constants.kEnemyTag)
      return;
    // Skip self
    if(other.transform.root == transform.root)
      return;
    // Skip no life
    LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return;
    // Skip self
    if(otherLife._data._networkID == _data._networkID)
      return;
    // If there is a target, get the near one with priority
    // Priority
    // 1. player 
    // 2. near
    if(_target == null)
    {
      _target = other.transform.root.gameObject;
    }
    else
    {
      float otherDis = Vector3.Distance(gameObject.transform.position, other.gameObject.transform.position);
      float targetDis = Vector3.Distance(gameObject.transform.position, _target.transform.position);
      if(_target.tag == Constants.kPlayerTag)
      {
        if(other.tag == Constants.kPlayerTag)
        {
          // Two players, get the near one
          if(otherDis < targetDis)
          {
            _target = other.transform.root.gameObject;
          }
        }
      }
      else
      {
        if(other.tag == Constants.kPlayerTag)
        {
          // Change to player obj
          _target = other.transform.root.gameObject;
        }
        else
        {
          // Two enemies, get the near one
          if(otherDis < targetDis)
          {
            _target = other.transform.root.gameObject;
          }

        }
      }
    }
  }

}