﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkBuzzSawController : MonoBehaviour
{
  public PhotonView _view;
  public GameObject _saw;
  public Vector3 _dest;
  public Vector3[] _destPoints;
  public Vector3 _rot;
  public SharedData _data;

  public void OnMasterClientSwitched()
  {
    UpdateLogic();
  }

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    UpdateLogic();
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
  }

  private void OnCharSpawn(Hashtable data)
  {
    UpdateLogic();
  }

  private void UpdateLogic()
  {
    LeanTween.cancel(_saw);
    if(!_view.isMine)
      return;
    //Debug.Log("Rotate saw");
    LeanTween.rotateLocal(_saw, _saw.transform.localRotation.eulerAngles + _rot, _data.RotSpeed).setLoopCount(-1); 
    LeanTween.moveLocal(_saw, _destPoints, _data.ForSpeed).setLoopCount(-1).setLoopPingPong();

  }
}