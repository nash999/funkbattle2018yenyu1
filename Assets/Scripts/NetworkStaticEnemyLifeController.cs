﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkStaticEnemyLifeController : NetworkEnemyLifeController
{

  void OnEnable()
  {
    // Setting the current health when the enemy first spawns.
    _data._currentLife = _data.MaxLife;
    //m_isSinking = false;
    m_isDead = false;
  }
}
