﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarAnimatorController : MonoBehaviour
{
  public PlayerData _data;
  public Animator _animator;
  //public string _attackString = "AttackMultiplier";

  public void UpdateAnimator()
  {
    UpdateAttackSpeed();
  }

  void OnEnable()
  {
    UpdateAttackSpeed();
  }

  private void UpdateAttackSpeed()
  {
    //Debug.Log("set attack speed: " + _data.AttackInterval);
    _animator.SetFloat(Constants.kAttackMultiplierKey, 1.0f/_data.AttackInterval);    
  }

}