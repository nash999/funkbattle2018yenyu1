﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMovementController : MonoBehaviour
{
  public GameObject _obj;
  public Vector3 _dir = Vector3.forward;
  public SharedData _data;

  void Update()
  {
    _obj.transform.Translate(_dir * Time.deltaTime * _data.ForSpeed);
    //_obj.transform.localRotation = Constants.kQuaternionIdentity;
  }
}