﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEnergyHUDController : MonoBehaviour
{
  public Slider _energyValueUI;

  public void AddEnergy(int currentEnergy)
  {
    if(_energyValueUI == null)
      return;
    // Fill energy
    _energyValueUI.value += currentEnergy;
  }

  public float CurrentEnergy()
  {
    return _energyValueUI.value;
  }

  public bool IsFull()
  {
    return (_energyValueUI.value == _energyValueUI.maxValue);
  }

  public void ResetEnergy()
  {
    _energyValueUI.value = 0;
  }

  void OnEnable()
  {
    GameObject energyObj = GameObject.FindGameObjectWithTag("EnergySlider");
    if(energyObj)
      _energyValueUI = energyObj.GetComponent<Slider>();
    ResetEnergy();
  }

  void OnDisable()
  {
    ResetEnergy();
    _energyValueUI = null;
  }

}
