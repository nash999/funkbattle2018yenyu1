﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableByTimer : MonoBehaviour
{
  public GameObject _objToDisable;
  public WeaponData _data;

  private float m_currentTime;

  void OnEnable()
  {
    m_currentTime = _data.ActiveTime;
  }

  void Update()
  {
    m_currentTime -= Time.deltaTime;
    if(m_currentTime <= 0)
      _objToDisable.SetActive(false);
  }

}
