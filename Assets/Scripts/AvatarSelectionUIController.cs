﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarSelectionUIController : MonoBehaviour
{
  public GameObject _menu;
  public void OnAvatarSelectionButtonDown()
  {
    _menu.SetActive(false);
  }
}
