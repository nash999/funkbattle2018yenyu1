﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarSkinLevelController : MonoBehaviour
{
  public Renderer _renderer;
  public List<Material> _materials;
  public List<Constants.CareerType> _careers;
  public PlayerData _data;

  public void OnEnable()
  {
    OnSkillUp();
  }

  public void OnPlayerNetworkInstantiate()
  {
    //Debug.Log("OnPlayerNetworkInstantiate: " + transform.root.name);
    // Need to add slightly delay because network instantiate callback is called before onEnable 
    StartCoroutine(OnSkillUpWithDelay(1));
  }

  public IEnumerator OnSkillUpWithDelay(float time)
  {
    yield return new WaitForSeconds(time);
    if(_materials.Count == 0)
      yield break;
    // Skin change only occur after level 1 career
    //Debug.Log("Change skin: " + _data._currentSkillLevel);
    if(_data._currentSkillLevel >= (int)(Constants.CareerLevel.CareerLevel1 - 1))
    {
      for(int i = 0; i < _careers.Count; ++i)
      {
        if(_careers[i] == _data._careerType)
        {
          _renderer.material = _materials[i];
        }
      }
    }
  }

  public void OnSkillUp()
  {
    StartCoroutine(OnSkillUpWithDelay(0));
  }
}