﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class UIFollower : MonoBehaviour
{
  // GameObject to follow
  public GameObject _objectToFollow = null;
  // UI's RectTransform
  public RectTransform _rectTransform;
  // UI object local position. Default position is on top of the follower
  public Vector2 _uiPosition = new Vector2(0,50);
  // Should start following
  public bool _startProcessing = false;
  // String for the canvas
  public string _canvasName = "HUDCanvas";
  public List<GameObject> _objsToHide;
  public Vector3 _defaultScale = new Vector3(1,1,1);
  // Canvas where the UI is under
  private Canvas m_canvas;
  // Canvas rect. Used to calculate the actual position of the UI
  private RectTransform m_canvasRect;

  public void StartFollowing(GameObject obj)
  {
    //Debug.Log("Start following: " + obj);
    if(obj == null)
      return;
    _objectToFollow = obj;
    _startProcessing = true;
  }

  void OnEnable()
  {
    if(m_canvas == null)
    {
      m_canvas = GameObject.Find(_canvasName).GetComponent<Canvas>();
      if(m_canvasRect == null)
        m_canvasRect = m_canvas.GetComponent<RectTransform>();
    }
    foreach(GameObject obj in _objsToHide)
    {
      obj.transform.localScale = Constants.kVector3Zero;
    }
  }

  void OnDisable()
  {
    _startProcessing = false;
    _objectToFollow = null;
  }

  // Update is called once per frame
  void LateUpdate()
  {
    if(!_startProcessing)
      return;
    UpdatePosition();
    // Show objs
    foreach(GameObject obj in _objsToHide)
    {
      obj.transform.localScale = _defaultScale;
    }
  }

  private void UpdatePosition()
  {
    if(_rectTransform == null)
      return;

    if(_objectToFollow == null)
      return;

    Vector2 viewportPosition = Camera.main.WorldToViewportPoint(_objectToFollow.transform.position);
    // Calculate the offset, because screen has different coordination 
    float finalX = ((viewportPosition.x * m_canvasRect.sizeDelta.x) - (m_canvasRect.sizeDelta.x * 0.5f));
    float finalY = ((viewportPosition.y * m_canvasRect.sizeDelta.y) - (m_canvasRect.sizeDelta.y * 0.5f));
    // Add desire position 
    Vector2 finalPos = new Vector2(finalX+_uiPosition.x, finalY+_uiPosition.y);
    // Apply final result
    _rectTransform.anchoredPosition = finalPos;
    //Debug.Log("pos: " + finalPos + " folloing: " + _objectToFollow.name);
  }
}