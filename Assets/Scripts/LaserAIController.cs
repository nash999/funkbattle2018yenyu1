﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserAIController : MonoBehaviour
{
  public GameObject _laser;
  public NavMeshAgent _ai;

  public float _defaultWait = 3.0f;
  public float _interval = 5.0f;
  public float _activateTime = 2.0f;
  public bool _disableAI = true;

  private string m_laserActivateInvoke = "ActivateLaser";
  private string m_laserDeactivateInvoke = "DeactivateLaser";

  void OnEnable()
  {
    InvokeRepeating(m_laserActivateInvoke, _defaultWait, _interval);
  }

  void OnDisable()
  {
    CancelInvoke(m_laserActivateInvoke);
    CancelInvoke(m_laserDeactivateInvoke);
    _laser.SetActive(false);
  }

  private void ActivateLaser()
  {
    _laser.SetActive(true);
    if(_disableAI)
      _ai.enabled = false;
    Invoke(m_laserDeactivateInvoke, _activateTime);

  }

  private void DeactivateLaser()
  {
    _laser.SetActive(false);
    if(_disableAI)
      _ai.enabled = true;
  }

}
