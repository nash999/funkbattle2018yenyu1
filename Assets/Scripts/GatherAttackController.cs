﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;
using UnityEngine.UI;
using CnControls;

public class GatherAttackController : ActionBase
{
  public string _horizontal = "Horizontal";
  public string _vertical = "Vertical";

  public List<Transform> _spawnTransList;
  public List<GameObject> _weapons;
  public List<Constants.CareerType> _careerTypes;
  public List<int> _spawnTotal;

  public GameObject _attackObj;
  public PlayerEnergyController _energy;
  public UltimateController _ultimate;

  public float _energyRefillInterval = 0.3f;
  public float _movementSpeedReductionRatio = 0.6f;
  public float _rotationSpeedReductionRatio = 1.0f;
  public bool m_isOn = false;

  protected int m_currentWeaponIndex;

  private float m_defaultForSpeedRatio;
  private float m_defaultRotSpeedRatio;

  public override void DoAttack()
  {
    SpawnObj(); 
  }

  public virtual void SpawnObj()
  {
    // Spawn the attack objs
    foreach(Transform trans in _spawnTransList)
    {
      ProcessSpawnObj(trans, _attackObj);
    }
  }

  public override void ProcessDown()
  {
    // Prevent double touch
    if(m_isOn)
      return;
    base.ProcessDown();
    m_isOn = true;

    _data._inputKey = Constants.KeyType.Down;
  }

  public override void ProcessPress()
  {
    if(!m_isOn)
      return;
    base.ProcessPress();

    _data.ForSpeedRatio = _movementSpeedReductionRatio;
    _data.RotSpeedRatio = _rotationSpeedReductionRatio;
    _data._inputKey = Constants.KeyType.Press;
    // Fill energy
    StartEnergyGatheringAnimation();
    InvokeRepeating("FillEnergy", 0, _energyRefillInterval); 

  }

  public override void ProcessUp()
  {
    if(!m_isOn)
      return;
    StopEnergyGatheringAnimation();
    // If the energy is fill, shoot ultimate attack
    // Otherwise, shoot regular attack
    if(_energy.IsFull())
    {
      // Use ultimate attack
      _ultimate.ProcessUp();
    }
    else
    {
      base.ProcessUp();
    }

    Reset();
    m_isOn = false;
  }

  public void FillEnergy()
  {
    _energy.AddEnergy(_data.EnergyRecoveryTotal);
  }

  public void ProcessSpawnObj(Transform trans, GameObject obj)
  {
    //GameObject spawnedObj = LeanPool.Spawn(obj, trans.position, trans.rotation);

    Quaternion rot;
    if(_data._attackDir.x != 0 || _data._attackDir.y != 0)
    {
      //Quaternion moveRot = Helpers.ConvertRotation(_data._moveDir.x, _data._moveDir.y);
      //Quaternion attackRot = Helpers.ConvertRotation(_data._attackDir.x, _data._attackDir.y);
      //rot = (attackRot * moveRot);
      rot = Helpers.ConvertRotation(_data._attackDir.x, _data._attackDir.y);
      //rot = _data._rot;
      //Debug.Log("rotation: " + rot.eulerAngles);
      //Debug.Log("moveDir: " + _data._moveDir + " attackDir: " + _data._attackDir);
      //Debug.Log("transRot: " + trans.localRotation.eulerAngles + " moveRot: " + moveRot.eulerAngles + " attackRot: " + attackRot.eulerAngles + " finalRot: " + rot.eulerAngles);
    }
    else
    {
      rot = Helpers.ConvertRotation(_data._moveDir.x, _data._moveDir.y);
      //rot = _data._rot;
    }
    //GameObject spawnedObj = LeanPool.Spawn(obj, trans.position, trans.rotation);
    GameObject spawnedObj = LeanPool.Spawn(obj, trans.position, trans.localRotation * rot);
    //GameObject spawnedObj = LeanPool.Spawn(obj, trans.position, trans.localRotation * rot);

    // Assign the team to spawned obj
    WeaponData spawnedObjData = spawnedObj.GetComponentInChildren<WeaponData>();
    if(spawnedObjData)
    {
      spawnedObjData._team = _data._team;
      spawnedObjData._expController = _data._expController;
      spawnedObjData._scoreController = _data._scoreController;
      spawnedObjData._ownerNetworkID = _data._networkID;
      spawnedObjData._currentWeaponSuckBloodLevel = _data._currentWeaponSuckBloodLevel;
      spawnedObjData._currentWeaponBleedingLevel = _data._currentWeaponBleedingLevel;
      spawnedObjData._currentWeaponUltimateDamageLevel = _data._currentWeaponUltimateDamageLevel;
      spawnedObjData._currentWeaponActiveTimeLevel = _data._currentWeaponActiveTimeLevel;
      spawnedObjData._currentWeaponRegularDamageLevel = _data._currentWeaponRegularDamageLevel;
      spawnedObjData._currentWeaponDamageForceLevel = _data._currentWeaponDamageForceLevel;
      spawnedObjData._currentWeaponSlowSpeedLevel = _data._currentWeaponSlowSpeedLevel;
      spawnedObjData._currentWeaponAttachDamageLevel = _data._currentWeaponAttachDamageLevel;
      spawnedObjData._currentWeaponRandomKnockBackLevel = _data._currentWeaponRandomKnockBackLevel;
    }
  }

  public void UpdateWeaponIndex()
  {
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      if(_careerTypes[i] == _data._careerType)
      {
        m_currentWeaponIndex = i;
        break;
      }
    }
  }

  public void StartEnergyGatheringAnimation()
  {
    _anim.SetBool("IsGathering", true);
  }

  public void StopEnergyGatheringAnimation()
  {
    _anim.SetBool("IsGathering", false);
  }

  void OnEnable()
  {
    m_defaultForSpeedRatio = _data.ForSpeedRatio;
    m_defaultRotSpeedRatio = _data.RotSpeedRatio;
  }
 
  void Reset()
  {
    // Reset energy
    _energy.ResetEnergy();
    _data.ForSpeedRatio = m_defaultForSpeedRatio;
    _data.RotSpeedRatio = m_defaultRotSpeedRatio;
    _data._inputKey = Constants.KeyType.Up;
    CancelInvoke("FillEnergy");
  }
}