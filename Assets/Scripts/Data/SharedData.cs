﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharedData : MonoBehaviour
{
  public GameObject _root;
  public EXPController _expController;
  public ScoreController _scoreController;

  public string _networkID;
  public string _nickname;
  public Constants.TeamSide _team;
  public string _itemType;
  public bool _canAttack = true;
  public bool _canMove = true;
  public bool _canRotate = true;
  [HideInInspector]
  public Constants.KeyType _inputKey;
  //[HideInInspector]
  //public float _x, _y;
  public Vector2 _moveDir;
  //[HideInInspector]
  public Vector2 _attackDir;
  //[HideInInspector]
  public Quaternion _rot = Constants.kQuaternionIdentity;

  public float _forSpeedRatio = 1.0f;
  public float _rotSpeedRatio = 1.0f;
  public float _moveAnimatorRatio = 0.1f;

  public float _dashSpeed = 5.0f;
  public float _dashAntiGravityTime = 1.0f;

  public List<float> _forSpeedLevel;
  public List<float> _rotSpeedLevel;
  public List<float> _attackIntervalLevel;
  public List<int> _maxLifeLevel;
  public List<int> _maxEnergyLevel;
  public List<int> _energyRecoveryLevel;
  public List<int> _defenseLevel;
  public List<int> _randomDodge;

  public int _maxLevel = 20;

  public int _currentLife;
  public int _currentEnergy;
  public int _currentEXP;
  public int _currentScore;
  public int _currentDodgeLevel;

  public int _deathEXP;
  public int _deathScore;

  #region Levels

  public int _currentPlayerLevel;
  public int _currentSkillLevel;
  public int _currentMaxLifeLevel;
  public int _currentMaxEnergyLevel;
  public int _currentEnergyRecoveryLevel;
  public int _currentAttackIntervalLevel;
  public int _currentDefenseLevel;
  public int _currentForwardSpeedLevel;
  public int _currentRotationSpeedLevel;

  public int _currentWeaponRegularDamageLevel;
  public int _currentWeaponUltimateDamageLevel;
  public int _currentWeaponSuckBloodLevel;
  public int _currentWeaponActiveTimeLevel;
  public int _currentWeaponDamageForceLevel;
  public int _currentWeaponSlowSpeedLevel;
  public int _currentWeaponSlowSpeedResetLevel;
  public int _currentWeaponAttachDamageLevel;
  public int _currentWeaponRandomKnockBackLevel;
  public int _currentWeaponBleedingLevel;

  #endregion

  #region Getters & Setters

  public float ForSpeedRatio
  {
    set
    {
      //Debug.Log("Change for speed ratio: " + value);
      _forSpeedRatio = value;
      _root.BroadcastMessage(Constants.kForSpeedRatioUpdateMS, SendMessageOptions.DontRequireReceiver);
    }
    get
    {
      return _forSpeedRatio;
    }
  }

  public float RotSpeedRatio
  {
    set
    {
      _rotSpeedRatio = value;
      _root.BroadcastMessage(Constants.kRotSpeedRatioUpdateMS, SendMessageOptions.DontRequireReceiver);
    }
    get
    {
      return _rotSpeedRatio;
    }
  }

  public int MaxLife
  {
    get
    {
      _currentMaxLifeLevel = Mathf.Clamp(_currentMaxLifeLevel, 0, _maxLifeLevel.Count-1);
      return _maxLifeLevel.Count == 0 ? 0 : _maxLifeLevel[_currentMaxLifeLevel];
    }
  }

  public int MaxEnergy
  {
    get
    {
      _currentMaxEnergyLevel = Mathf.Clamp(_currentMaxEnergyLevel, 0, _maxEnergyLevel.Count-1);
      return _maxEnergyLevel.Count == 0 ? 0 : _maxEnergyLevel[_currentMaxEnergyLevel];
    }
  }

  public float ForSpeed
  {
    get
    {
      _currentForwardSpeedLevel = Mathf.Clamp(_currentForwardSpeedLevel, 0, _forSpeedLevel.Count-1);
      return _forSpeedLevel.Count == 0 ? 0 : _forSpeedLevel[_currentForwardSpeedLevel] * _forSpeedRatio;
    }
  }

  public float RotSpeed
  { 
    get
    {
      _currentRotationSpeedLevel = Mathf.Clamp(_currentRotationSpeedLevel, 0, _rotSpeedLevel.Count-1);
      return _rotSpeedLevel.Count == 0 ? 0 : _rotSpeedLevel[_currentRotationSpeedLevel] * _rotSpeedRatio;
    }
  }

  public float AttackInterval
  {
    get
    {
      _currentAttackIntervalLevel = Mathf.Clamp(_currentAttackIntervalLevel, 0, _attackIntervalLevel.Count-1);
      return _attackIntervalLevel.Count == 0 ? 0 : _attackIntervalLevel[_currentAttackIntervalLevel];
    }
    set
    {
      _attackIntervalLevel[_currentAttackIntervalLevel] = value;
    }
  }

  public int EnergyRecoveryTotal
  {
    get
    {
      _currentEnergyRecoveryLevel = Mathf.Clamp(_currentEnergyRecoveryLevel, 0, _energyRecoveryLevel.Count-1);
      return _energyRecoveryLevel.Count == 0 ? 0 : _energyRecoveryLevel[_currentEnergyRecoveryLevel];
    }
  }

  public int DefensePercentage
  {
    get
    {
      _currentDefenseLevel = Mathf.Clamp(_currentDefenseLevel, 0, _defenseLevel.Count-1);
      return _defenseLevel.Count == 0 ? 0 : _defenseLevel[_currentDefenseLevel];
    }
  }

  public int DodgePercentage
  {
    get
    {
      _currentDodgeLevel = Mathf.Clamp(_currentDodgeLevel, 0, _randomDodge.Count-1);
      return _randomDodge.Count == 0 ? 0 : _randomDodge[_currentDodgeLevel];
    }
  }

  public int DeathEXP
  {
    get
    {
      // first level is 0
      return _deathEXP * (_currentPlayerLevel+1);
    }
  }

  public int DeathScore
  {
    get
    {
      return _deathScore;
    }
  }

  #endregion

}
