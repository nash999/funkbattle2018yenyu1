﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponData : SharedData
{
  public string _ownerNetworkID;
  public List<int> _attackForceLevel;
  public List<int> _damageLevel;
  public List<int> _suckBloodLevel;
  public List<int> _bleedingLevel;
  public List<int> _slowSpeedLevel;
  public List<float> _activeTimeLevel;
  public List<float> _slowSpeedResetTimeLevel;
  public List<float> _attachDamageLevel;
  public List<float> _randomKnockBack;


  public int DamageForce
  {
    get
    {
      _currentWeaponDamageForceLevel = Mathf.Clamp(_currentWeaponDamageForceLevel, 0, _attackForceLevel.Count-1);
      return _attackForceLevel.Count == 0 ? 0 : _attackForceLevel[_currentWeaponDamageForceLevel];
    }
  }

  public int WeaponDamage
  {
    get
    {
      _currentWeaponRegularDamageLevel = Mathf.Clamp(_currentWeaponRegularDamageLevel, 0, _damageLevel.Count-1);
      return _damageLevel.Count == 0 ? 0 : _damageLevel[_currentWeaponRegularDamageLevel];
    }
  }

  public int BloodSuckPercentage
  {
    get
    {
      _currentWeaponSuckBloodLevel = Mathf.Clamp(_currentWeaponSuckBloodLevel, 0, _suckBloodLevel.Count-1);
      return _suckBloodLevel.Count == 0 ? 0 : _suckBloodLevel[_currentWeaponSuckBloodLevel];
    }
  }

  public int BleedingPercentage
  {
    get
    {
      _currentWeaponBleedingLevel = Mathf.Clamp(_currentWeaponBleedingLevel, 0, _bleedingLevel.Count-1);
      return _bleedingLevel.Count == 0 ? 0 : _bleedingLevel[_currentWeaponBleedingLevel];
    }
  }

  public int SlowSpeedPercentage
  {
    get
    {
      _currentWeaponSlowSpeedLevel = Mathf.Clamp(_currentWeaponSlowSpeedLevel, 0, _slowSpeedLevel.Count-1);
      return _slowSpeedLevel.Count == 0 ? 0 : _slowSpeedLevel[_currentWeaponSlowSpeedLevel];
    }
  }

  public float ActiveTime
  {
    get
    {
      _currentWeaponActiveTimeLevel = Mathf.Clamp(_currentWeaponActiveTimeLevel, 0, _activeTimeLevel.Count-1);
      return _activeTimeLevel.Count == 0 ? 0 : _activeTimeLevel[_currentWeaponActiveTimeLevel];
    }
  }

  public float SlowSpeedResetTime
  {
    get
    {
      _currentWeaponSlowSpeedResetLevel = Mathf.Clamp(_currentWeaponSlowSpeedResetLevel, 0, _slowSpeedResetTimeLevel.Count-1);
      return _slowSpeedResetTimeLevel.Count == 0 ? 0 : _slowSpeedResetTimeLevel[_currentWeaponSlowSpeedResetLevel];
    }
  }

  public float AttachDamagePercentage
  {
    get
    {
      _currentWeaponAttachDamageLevel = Mathf.Clamp(_currentWeaponAttachDamageLevel, 0, _attachDamageLevel.Count-1);
      return _attachDamageLevel.Count == 0 ? 0 : _attachDamageLevel[_currentWeaponAttachDamageLevel];
    }
  }

  public float KnockBackPercentage
  {
    get
    {
      _currentWeaponRandomKnockBackLevel = Mathf.Clamp(_currentWeaponRandomKnockBackLevel, 0, _randomKnockBack.Count-1);
      return _randomKnockBack.Count == 0 ? 0 : _randomKnockBack[_currentWeaponRandomKnockBackLevel];
    }
  }
}
