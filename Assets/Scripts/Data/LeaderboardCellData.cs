﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardCellData : ScriptableObject
{
  public string _id;
  public int _rank;
  public string _name;
  public int _score;
}