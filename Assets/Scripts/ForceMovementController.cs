﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceMovementController : MovementController
{
  public override void Move()
  {
    _rigid.AddForce(m_movement, ForceMode.Force);
  }
}
