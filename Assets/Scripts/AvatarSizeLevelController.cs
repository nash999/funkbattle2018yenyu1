﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class AvatarSizeLevelController : MonoBehaviour
{
  public GameObject _root;

  public List<float> _sizeRatioList;
  public PlayerData _data;

  private float m_lastSize;

  public void OnPlayerNetworkInstantiate()
  {
    OnLevelUp();
  }

  public void OnLevelUp()
  {
    if(_sizeRatioList.Count == 0)
      return;
    UpdateSize();
  }

  private void UpdateSize()
  {
    // Clampe the value
    int size = Mathf.Clamp(_data._currentPlayerLevel, 0, _sizeRatioList.Count-1);
    if(m_lastSize == size)
      return;
    m_lastSize = size;
    //Debug.Log("Size: " + size);
    _root.transform.localScale = new Vector3(1,1,1) * _sizeRatioList[size];
  }

}