﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpNetworkBuffSpawner : NetworkBuffSpawner
{
  //public int _spawnTotal = 10;

  public override void SpawnObj()
  {
    // Return if player is not in room
    if(!PhotonNetwork.inRoom)
      return;

    while(m_currentTotalSpawnedObjs < _totalSpawnedObjs)
    {
      // Randomly choose a obj to spawn on random location
      GameObject obj = _buffs[Random.Range(0, _buffs.Count)];
      Transform location = _spawnLocations[Random.Range(0, _spawnLocations.Count)];
      if(PhotonNetwork.inRoom)
      {
        PhotonNetwork.InstantiateSceneObject
        (
          obj.name,
          location.position,
          location.rotation,
          0,
          null
        );
        m_currentTotalSpawnedObjs++;
      }
    }

  }

  public override void OnCharSpawn(Hashtable data)
  {
    StartProcess();
  }

  public new void OnMasterClientSwitched()
  {
    StartProcess();
  }

  private void StartProcess()
  {
    CancelInvoke(m_invokeString);

    if(!PhotonNetwork.isMasterClient)
      return;
    // Start spawning 
    InvokeRepeating(m_invokeString, 0, _invokeInterval);
  }
}