﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCallback : MonoBehaviour
{
  public void StartSinking()
  {
    BroadcastMessage("DoSinkingAnimation", SendMessageOptions.DontRequireReceiver);
  }
}