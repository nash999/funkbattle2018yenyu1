﻿using UnityEngine;
using System.Collections;
//Allows us to use Lists.
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
  //Static instance of GameManager which allows it to be accessed by any other script.
  public static GameManager instance = null;

  public List<string> _nameList;
  // Random skills
  public List<string> _randomSkillsDesc;
  public List<Constants.RandomSkillType> _randomTypes;
  public List<Sprite> _randomButtonNormalSprites;
  public List<Sprite> _randomButtonSelectSprites;
  // Regular skills
  public List<string> _skillButtonDesc;
  public List<Constants.RegularSkillType> _regularSkillTypes;
  public List<Sprite> _skillButtonNormalSprites;
  public List<Sprite> _skillButtonSelectSprites;
  // Career 
  public List<string> _careerLevel1Desc;
  public List<string> _careerLevel2Desc;
  public List<Constants.CareerType> _careerTypes;
  public List<Sprite> _careerButtonNormalSprites;
  public List<Sprite> _careerButtonSelectSprites;
  // Weapon
  public List<string> _weaponDesc;
  public List<Sprite> _weaponButtonNormalSprites;
  public List<Sprite> _weaponButtonSelectSprites;

  public Constants.GameState _gameState = Constants.GameState.CharacterSelection;
  public Constants.TeamSide _localTeam;

  public string _knockString = "擊退";
  public string _slowString = "緩速";
  public string _dodgeString = "閃避";
  public string _avatarMoveTutorialString = "拖曳旋鈕移動角色";
  public string _energyGatherTutorialString = "按住旋鈕使用大招";
  public string _invalidNameDescPre = "名字長度只能在 ";
  public string _invalidNameAnd = " 和 ";
  public string _invalidNameDescPost = " 之間";
  public string _anonymousPreName = "";

  public int _currentCharacterIndex = 0;

  public bool _showDebugYomobAd = true;
  public bool _showDebugAppodealAd = false;
  public bool _passAD = false;
  public bool _isOffline = true;
  public int _GDPRChoiceIndex = 1;
  public int _anonymousRandomRangeMin = 1000;
  public int _anonymouseRandomRangeMax = 9999;

  //Awake is always called before any Start functions
  void Awake()
  {
    //Check if instance already exists
    if(instance == null)
    {
      // If not, set instance to this
      instance = this;
      // Init data
      Init();
    }
    //If instance already exists and it's not this:
    else if(instance != this)
    {
      //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
      Destroy(gameObject);  
    }  

    //Sets this to not be destroyed when reloading scene
    DontDestroyOnLoad(gameObject);
  }

  public string GetRandomName()
  {
    //return _nameList[Random.Range(0, _nameList.Count)];
    return _anonymousPreName + Random.Range(_anonymousRandomRangeMin, _anonymouseRandomRangeMax);
  }

  public GameObject FindOnePlayerByNetworkID(string id)
  {
    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
    foreach(GameObject g in players)
    {
      SharedData data = g.GetComponentInChildren<SharedData>();
      if(data != null && data._networkID == id)
      {
        return g;
      }
    }
    return null;
  }

  public GameObject FindClosestPlayer(GameObject seeker)
  {
    GameObject result = null;
    // Find the player that is still alive
    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
    foreach(GameObject g in players)
    {
      SharedData data = g.GetComponentInChildren<SharedData>();
      if(data != null && data._currentLife > 0)
      {
        if(result == null)
          result = g;
        else
        {
          if(Vector3.Distance(seeker.transform.position, result.transform.position) > Vector3.Distance(seeker.transform.position, g.transform.position))
          {
            result = g;
          }
        }
      }
    }
    return result;
  }

  public GameObject FindOnePlayer()
  {
    // Find the player that is still alive
    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
    foreach(GameObject g in players)
    {
      SharedData data = g.GetComponentInChildren<SharedData>();
      if(data != null && data._currentLife > 0)
      {
        return g;
      }
    }
    return null;
  }

  public void AssignTeam()
  {
    // Calculate team side based on total players
    _localTeam = PhotonNetwork.playerList.Length % 2 == 0 ? Constants.TeamSide.Red : Constants.TeamSide.Blue;
    Messenger.Broadcast(Constants.kTeamUpdateNotification);
    //Debug.Log("total players: " + PhotonNetwork.playerList.Length);
  }

  #region Internal

  private void Init()
  {
   
  }


  #endregion

  #region Regular Skill

  public string GetRegularSkillDesc(Constants.RegularSkillType type)
  {
    for(int i = 0; i < _regularSkillTypes.Count; ++i)
    {
      if(type == _regularSkillTypes[i])
      {
        return _skillButtonDesc[i];
      }
    }
    return null;
  }

  public Sprite GetRegularSkillButtonNormal(Constants.RegularSkillType type)
  {
    for(int i = 0; i < _regularSkillTypes.Count; ++i)
    {
      if(type == _regularSkillTypes[i])
      {
        return _skillButtonNormalSprites[i];
      }
    }
    return null;
  }

  public Sprite GetRegularSkillButtonSelect(Constants.RegularSkillType type)
  {
    for(int i = 0; i < _regularSkillTypes.Count; ++i)
    {
      if(type == _regularSkillTypes[i])
      {
        return _skillButtonSelectSprites[i];
      }
    }
    return null;
  }
 
  #endregion

  #region Random Skills 

  public string GetRandomSkillDesc(Constants.RandomSkillType type)
  {
    for(int i = 0; i < _randomTypes.Count; ++i)
    {
      if(type == _randomTypes[i])
        return _randomSkillsDesc[i];
    }
    return "";
  }

  public Sprite GetRandomSkillButtonNormal(Constants.RandomSkillType type)
  {
    for(int i = 0; i < _randomTypes.Count; ++i)
    {
      if(type == _randomTypes[i])
      {
        return _randomButtonNormalSprites[i];
      }
    }
    return null;
  }

  public Sprite GetRandomSkillButtonSelect(Constants.RandomSkillType type)
  {
    for(int i = 0; i < _randomTypes.Count; ++i)
    {
      if(type == _randomTypes[i])
      {
        return _randomButtonSelectSprites[i];
      }
    }
    return null;
  }

  #endregion

  #region Career 

  public string GetCareerDesc(Constants.CareerType type, Constants.CareerLevel level)
  {
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      if(type == _careerTypes[i])
      {
        switch(level)
        {
          case Constants.CareerLevel.CareerLevel1:
            return _careerLevel1Desc[i];
          case Constants.CareerLevel.CareerLevel2:
            return _careerLevel2Desc[i];
        }

      }
    }
    return "";
  }

  public Sprite GetCareerButtonNormal(Constants.CareerType type)
  {
    //Debug.Log("career type: " + type);
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      //Debug.Log("check: " + _careerTypes[i]);
      if(type == _careerTypes[i])
      {
        //Debug.Log("Return: " + _careerButtonNormalSprites[i]);
        return _careerButtonNormalSprites[i];
      }
    }
    return null;
  }

  public Sprite GetCareerButtonSelect(Constants.CareerType type)
  {
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      if(type == _careerTypes[i])
      {
        return _careerButtonSelectSprites[i];
      }
    }
    return null;
  }

  #endregion

  #region Weapons

  public string GetWeaponDesc(Constants.CareerType type)
  {
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      if(type == _careerTypes[i])
      {
        return _weaponDesc[i];
      }
    }
    return "";
  }

  public Sprite GetWeaponButtonNormal(Constants.CareerType type)
  {
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      if(type == _careerTypes[i])
      {
        return _weaponButtonNormalSprites[i];
      }
    }
    return null;
  }

  public Sprite GetWeaponButtonSelect(Constants.CareerType type)
  {
    for(int i = 0; i < _careerTypes.Count; ++i)
    {
      if(type == _careerTypes[i])
      {
        return _weaponButtonSelectSprites[i];
      }
    }
    return null;
  }

  #endregion

}