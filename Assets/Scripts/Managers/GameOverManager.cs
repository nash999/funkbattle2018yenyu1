﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;


public class GameOverManager : MonoBehaviour
{
  // List reference to all players' health.
  public List<PlayerLifeController> _playerLifes;
  // Reference to the animator component.
  public Animator anim;

  private bool m_allDie = false;

  void Update()
  {
    if(GameManager.instance._gameState != Constants.GameState.GamePlay)
      return;
    // Skip for no players
    if(_playerLifes.Count == 0)
      return;

    // Check which character has died
    for(int i = _playerLifes.Count-1; i >= 0; --i)
    {
      PlayerLifeController life = _playerLifes[i];
      if(life._data._currentLife > 0)
      {
        m_allDie = false;
      }
      else if(life._data._currentLife == 0)
      {
        Hashtable data = new Hashtable();
        //data[Constants.kTeamKey] = life._data._team;
        data[Constants.kCharacterKey] = life._root;
        // Remove data from list
        _playerLifes.RemoveAt(i);
      }
    }

    // Game is over when all players are dead
    if(!m_allDie)
      return;

    // Clear the data 
    _playerLifes.Clear();
    anim.SetBool("GameOver", true);
    // Udpate Game state 
    GameManager.instance._gameState = Constants.GameState.GameOver;
    Messenger.Broadcast(Constants.kGameOverNotification);
  }

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharRestart);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharRestart);
  }

  #region Notificaiton Handlers

  private void OnCharSpawn(Hashtable data)
  {
    // Get the spawn obj
    GameObject obj = (GameObject)data[Constants.kCharacterKey];
    // Get the life script
    if(obj == null)
      return;
    PlayerLifeController life = obj.GetComponentInChildren<PlayerLifeController>();
    if(life == null)
      return;
    _playerLifes.Add(life);
  }

  private void OnCharRestart()
  {
    anim.SetBool("GameOver", false);
  }

  #endregion
}