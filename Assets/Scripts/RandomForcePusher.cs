﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomForcePusher : MonoBehaviour
{ 
  public Rigidbody _rigid;
  public Vector3 _minDir;
  public Vector3 _maxDir;

  void OnEnable()
  {
    Vector3 dir = new Vector3
    (
      Random.Range(_minDir.x, _maxDir.x),
      Random.Range(_minDir.y, _maxDir.y),
      Random.Range(_minDir.z, _maxDir.z)
    );
    _rigid.AddForce(dir, ForceMode.Impulse);
  }

}
