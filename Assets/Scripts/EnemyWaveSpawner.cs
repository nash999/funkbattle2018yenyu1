﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

/// <summary>
///  Spawn enemy based on waves
/// </summary>
public class EnemyWaveSpawner : MonoBehaviour
{
  public int _currentWave;
  public int _maxWave = 2;

  public List<GameObject> _spawnObjs;
  public List<SpawnLocation> _spawnLocations;
  public List<int> _waitTimeBetweenWave;
  public List<int> _spawnObjTotal;
  public List<float> _spawnInterval;

  private bool m_firstTime = false;
  //private int m_spawnTotalForThisLevel;
  private float m_checkObjInterval = 0.5f;
  private string m_waveInvokeString = "SpawnWave";
  private string m_spawnInvokeString = "SpawnObj";
  private string m_objDeadCheckInvokeString = "CheckObj";
  public List<GameObject> m_currentSpawnedObjs = new List<GameObject>();

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
  }

  private void OnCharSpawn(Hashtable data)
  {
    // Already spawn for the first time?
    if(m_firstTime)
      return;
    m_firstTime = true;
    StartWave();
  }

  private void SpawnWave()
  {
    //m_spawnTotalForThisLevel = _spawnObjTotal[_currentWave];
    for(int i = 0; i < _spawnObjTotal[_currentWave]; ++i)
    {
      Invoke(m_spawnInvokeString, _spawnInterval[_currentWave] * i);
    }
  }

  private void SpawnObj()
  {
    // Calculate spawn location
    List<Transform> data = new List<Transform>();
    foreach(SpawnLocation sl in _spawnLocations)
    {
      // Each location has its own probability distribution
      // we use the distribution probability to calculate 
      // the random location
      for(int i = 0; i < sl._spawnProbabilityList[_currentWave]; ++i)
      {
        data.Add(sl.gameObject.transform);
      }
    }

    // Randomly pick the location
    Transform randomLocaiton = data[Random.Range(0, data.Count)];

    GameObject obj = LeanPool.Spawn
                     (
                       _spawnObjs[_currentWave],
                       randomLocaiton.position,
                       randomLocaiton.rotation
                     );

    if(m_currentSpawnedObjs.Count == 0)
    {
      // First obj for this wave
      // So we activate the checker
      InvokeRepeating(m_objDeadCheckInvokeString, m_checkObjInterval, m_checkObjInterval);
    }
    m_currentSpawnedObjs.Add(obj);

    //LeanPool.Spawn(_enemy, _spawnPoints[spawnPointIndex].position, _spawnPoints[spawnPointIndex].rotation);
  }

  private void CheckObj()
  {
    // If the obj list is enpty
    // Increase the index and cancel this invoke
    if(m_currentSpawnedObjs.Count == 0)
    {
      // Cancel check
      CancelInvoke(m_objDeadCheckInvokeString);
      // Increase wave
      _currentWave++;
      if(_currentWave < _maxWave)
      {
        // Start new wave
        StartWave();
      }
      else
      {
        Debug.Log("Max wave is reached: " + _currentWave);
      }

    }
    else
    {
      // Check each obj and see if they are still alive
      // If not, remove that obj
      for(int i = m_currentSpawnedObjs.Count - 1; i >= 0; i--)
      {
        if(!m_currentSpawnedObjs[i].activeSelf)
        {
          //Debug.Log(m_currentSpawnedObjs[i].name + " is invisible therefore dead. remove it");
          m_currentSpawnedObjs.RemoveAt(i);
        }
      }
    }
  }

  private void StartWave()
  {
    Debug.Log("Start wave: " + _currentWave);
    Invoke(m_waveInvokeString, _waitTimeBetweenWave[_currentWave]); 
  }
}