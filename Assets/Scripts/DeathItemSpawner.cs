﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;

public class DeathItemSpawner : MonoBehaviour
{
  public GameObject _expBuff;
  public List<GameObject> _spawnList;
  public List<GameObject> _specialItems;
  public List<string> _namesForSpawningSpecialItems;
  public int _maxRandomItemsToSpawn = 3;
  // How many exp items to spawn
  public int _expItemSpawnTotalThreshold = 5;

  public void Process(Vector3 pos, Vector3 rot, int exp, string itemType = null)
  {
    // Make sure network is ready
    if(!PhotonNetwork.connectedAndReady)
      return;
    // Only Master can process this 
    if(!PhotonNetwork.isMasterClient)
      return;
    // Return if player is not in room
    if(!PhotonNetwork.inRoom)
      return;

    int totalThreshold = Random.Range(1, _expItemSpawnTotalThreshold);

    int finalExp = exp / totalThreshold;
    finalExp = Mathf.Clamp(finalExp, 1, exp);
    //Debug.Log("Spawn: " + totalThreshold + " items with: " + finalTotal + " exp" + " original exp: " + exp);
    for(int i = 0; i < totalThreshold; ++i)
    {
      object[] data = new object[] {
        finalExp,
        ""
      };

      //GameObject eb = PhotonNetwork.InstantiateSceneObject(_expBuff.name, pos, Quaternion.Euler(rot), 0, data);
      PhotonNetwork.InstantiateSceneObject(_expBuff.name, pos, Quaternion.Euler(rot), 0, data);
      //Debug.Log("spawn exp: " + eb.name + " pos: " + pos + " rot: " + Quaternion.Euler(rot));
    }

    if(_namesForSpawningSpecialItems.Contains(itemType))
    {
      GameObject go = _specialItems[Random.Range(0, _specialItems.Count)];
      PhotonNetwork.InstantiateSceneObject(go.name, pos, Quaternion.Euler(rot), 0, null);
    }
    else
    {
      // Randomly select an item from list and spawn it over network 
      int totalSpawnItems = Random.Range(1, _maxRandomItemsToSpawn);
      for(int i = 0; i < totalSpawnItems; ++i)
      {
        GameObject go = _spawnList[Random.Range(0, _spawnList.Count)];
        PhotonNetwork.InstantiateSceneObject(go.name, pos, Quaternion.Euler(rot), 0, null);
        //Debug.Log("Spawn life items: " + instObj.name + " pos: " + pos + " quaternion: " + Quaternion.Euler(rot));
      }
    }
  }

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kEnemyDeathNotification, OnEnemyDeath);
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kEnemyDeathNotification, OnEnemyDeath);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
  }

  private void OnCharDeath(Hashtable data)
  {
    //Debug.Log("OnCharacterDeath");
    Vector3 pos = (Vector3)data[Constants.kCharacterPosKey];
    int exp = (int)data[Constants.kCharacterDataKey];
    Process(pos, Constants.kVector3Zero, exp);
  }

  private void OnEnemyDeath(Hashtable data)
  {
    //Debug.Log("OnEnemyDeath");
    Vector3 pos = (Vector3)data[Constants.kCharacterPosKey];
    int exp = (int)data[Constants.kCharacterDataKey];
    string itemType = data[Constants.kItemTypeKey].ToString();
    Process(pos, Constants.kVector3Zero, exp, itemType);
  }
}