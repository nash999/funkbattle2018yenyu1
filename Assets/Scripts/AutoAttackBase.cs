﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttackBase : MonoBehaviour
{
  public GameObject _center;
  public ActionBase _attack;
  public float _angle;
  public bool _skipForManualMovement = false;
  public bool _active = true;

  private float m_attackInterval;

  public bool UpdateStep()
  {
    if(m_attackInterval > 0)
    {
      m_attackInterval -= Time.deltaTime;
      return false;
    }
    m_attackInterval = _attack._data.AttackInterval;
    return true;
  }

  public void CheckAttack(GameObject obj)
  {
    if(!_active)
      return;
    // Skip if other has no life
    LifeBase lb = obj.transform.root.GetComponentInChildren<LifeBase>();
    if(lb == null)
      return;
    // Still have life?
    if(lb._data._currentLife <= 0)
      return;
    // Check the angle
    Vector3 targetDir = obj.transform.position - _center.transform.position;
    float angle = Vector3.Angle(targetDir, _center.transform.forward);

    if(angle > _angle)
    {
      //Debug.Log("angle too big: " + angle + " by: " + other.name);
      return;
    }
    //Debug.Log("Attack: " + lb._data._root.name);
    // Change the attack direction toward the target
    _attack._data._attackDir.Set(targetDir.x, targetDir.z);
    _attack.DoAttackWithAnimation();
  }
}