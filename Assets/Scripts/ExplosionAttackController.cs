﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class ExplosionAttackController : MonoBehaviour
{
  public GameObject _root;
  public WeaponData _data;

  void OnTriggerEnter(Collider other)
  {
    //Debug.Log("Trigger enger: " + other.gameObject);
    // Skip for same team
    LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return;
    if(otherLife._data._team == _data._team)
      return;
    // Apply for damage
    otherLife.TakeDamage(_data.WeaponDamage, transform.position, _data);
    Rigidbody otherRigid = other.GetComponentInParent<Rigidbody>();
    if(otherRigid)
      otherRigid.AddForce((other.transform.position - transform.position) * _data.DamageForce, ForceMode.Impulse);
  }
}