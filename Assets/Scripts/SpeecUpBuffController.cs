﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeecUpBuffController : MonoBehaviour
{
  public SharedData _data;

  private SharedData m_data;
 
  private string m_invokeString = "ProcessLogic";

  void OnEnable()
  {
    // Get the data from host
    // Get the root obj
    // Invoke with a bit delay so the logic can be processed 
    // after it has been attached to parent
    Invoke(m_invokeString, 0.5f);
    //Debug.Log("parent: " + transform.root.name);
  }

  void OnDisable()
  {
    CancelInvoke(m_invokeString);
    if(m_data == null)
      return;
    m_data.ForSpeedRatio = 1.0f;
    m_data = null;
    //Debug.Log("Reset speed up: " + transform.root.name);
  }

  void Update()
  {
    if(m_data == null)
      return;
    m_data.ForSpeedRatio = _data.ForSpeedRatio;
  }

  private void ProcessLogic()
  {
    m_data = transform.root.GetComponentInChildren<SharedData>();
  }
}