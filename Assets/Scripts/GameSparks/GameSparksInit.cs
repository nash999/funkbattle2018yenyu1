﻿using UnityEngine;
using System.Collections;

using GameSparks.Api;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;

public class GameSparksInit : MonoBehaviour
{
  // Use this for initialization
  void Start()
  {
    GS.GameSparksAvailable += HandleGameSparksAvailable;  
  }

  #region Notification Callbacks 

  #endregion

  private void HandleGameSparksAvailable(bool value)
  {
    if(!value)
      return;
    //Debug.Log("GS is available");

    Messenger.Broadcast(Constants.kGSAvailableNotifiation);
  }
}