﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;
//using Facebook.Unity;
using UnityEngine.SceneManagement;
using GameSparks.Api.Requests;

public class LoginController : MonoBehaviour
{
  private bool m_isNetworkSystemReady = false;
  private bool m_isDatabaseSystemReady = false;

  void Start()
  {
    CheckLogin();
  }

  void OnEnable()
  {
    Messenger.AddListener(Constants.kGSAvailableNotifiation, OnGSAvailable);
    Messenger.AddListener(Constants.kNetworkSystemReadyNotification, OnNetworkSystemReady);

  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGSAvailableNotifiation, OnGSAvailable);
    Messenger.RemoveListener(Constants.kNetworkSystemReadyNotification, OnNetworkSystemReady);
  }

  #region Social Functions
  /*
  private void OnFBReady()
  {
    // Process FB login
    Debug.Log("FB init ready");
    if(FB.IsLoggedIn)
    {
      Messenger.Broadcast(Constants.kShowFBLogoutPanelNotification);
    }
    else
      Messenger.Broadcast(Constants.kShowFBLoginPanelNotification);
  }

  private bool SupportByFB()
  {
    if(Application.platform == RuntimePlatform.Android)
      return true;
    if(Application.platform == RuntimePlatform.IPhonePlayer)
      return true;
    if(Application.platform == RuntimePlatform.WebGLPlayer)
      return true;
    return false;
  }

  private bool SupportByTwitter()
  {
    if(Application.platform == RuntimePlatform.Android)
      return true;
    if(Application.platform == RuntimePlatform.IPhonePlayer)
      return true;
    return false;
  }
  */
  #endregion

  private void OnGSAvailable()
  {
    m_isDatabaseSystemReady = true;
    CheckLogin();
  }

  private void OnNetworkSystemReady()
  {
    m_isNetworkSystemReady = true;
    CheckLogin();
  }

  private void CheckLogin()
  {
    //Debug.Log("Checking GS Login");
    //if(!m_isNetworkSystemReady)
    //  return;
    if(!m_isDatabaseSystemReady)
      return;
    DoAnonymouseLogin();
    /*
    if(GS.Authenticated)
    {
      Debug.Log("Already authenticated");
      // Setup basic user data
      string playerID = PlayerPrefs.GetString(Constants.kGSPLayerIDKey);
      if(PhotonNetwork.playerName.Length <= 0)
        PhotonNetwork.playerName = playerID;
      //_displayName.text = GameManager.instance._playerNickName;
      // Already login, send out login success notification
      Messenger.Broadcast(Constants.kGSLoginSuccessNotification);
    }
    else
    {
      Debug.Log("GS is not authenticated yet, check login");
      // Do we have user info already?
      string username = PlayerPrefs.GetString(Constants.kGSUsernameKey);
      string pass = PlayerPrefs.GetString(Constants.kGSUserPassKey);

      // Validate the user information
      // If user information is invalid, do login anonymously
      if(username == null || username.Length == 0 || pass == null || pass.Length == 0)
      {
        DoAnonymouseLogin();
      }
      else
      {
        // Logic using user information
        DoGSLogin(username, pass);
      }

    }
    */
  }

  private void DoGSLogin(string username, string password)
  {
    // Login user
    Debug.Log ("Do GS login: " + username);
    new GameSparks.Api.Requests.AuthenticationRequest ()
      .SetUserName (username)
      .SetPassword (password)
      .Send ((response) =>
      {

        if(!response.HasErrors)
        {
          //_info.text = "Welcome: "+response.DisplayName;
          // Save the name in player's properties
          //GameManager.instance.SavePlayerScreenName(response.DisplayName);
          // Update name UI
         // _displayName.text = GameManager.instance._playerNickName;
          PlayerPrefs.SetString(Constants.kGSPLayerIDKey, response.UserId);
          Debug.Log("GS account authentication success. UserId: " + response.UserId);
          if(PhotonNetwork.playerName.Length <= 0)
            PhotonNetwork.playerName = response.UserId;
          Debug.Log("GS display name: " + response.DisplayName);
          // Save the name and psss
          PlayerPrefs.SetString(Constants.kGSUsernameKey, username);
          PlayerPrefs.SetString(Constants.kGSUserPassKey, password);
          Messenger.Broadcast(Constants.kGSLoginSuccessNotification);

        }
        else
        {
          Debug.Log("Error Authenticating Player... \n "+response.Errors.JSON.ToString());
          //_info.text = response.Errors.JSON.ToString();
          DoLoginFailure();
        }

      });
  }

  private void DoLoginFailure()
  {
    Messenger.Broadcast(Constants.kGSLoginFailureNotificaiton);
  }

  private void DoAnonymouseLogin()
  {
    //Debug.Log("Do anonymous login");
    new DeviceAuthenticationRequest().Send((response) =>
    {
      if(response.HasErrors)
      {
        Debug.Log("GS authentication Error: " + response.Errors.JSON.ToString());
        DoLoginFailure();
      }
      else
      {
        Debug.Log("GS device authentication success. UserId: " + response.UserId);
        PlayerPrefs.SetString(Constants.kGSPLayerIDKey, response.UserId);

        // Only change name when the player name is not yet assigned
        if(PhotonNetwork.playerName.Length <= 0)
          PhotonNetwork.playerName = response.UserId;
       // _displayName.text = GameManager.instance._playerNickName;
        Messenger.Broadcast(Constants.kGSLoginSuccessNotification);
      }
    });
  }

  private void ProcessGSRegistration(string displayName, string username, string password)
  {
    // Register user
    Debug.Log ("Registering Player...");
    new GameSparks.Api.Requests.RegistrationRequest ()
      .SetDisplayName (displayName)
      .SetUserName (username)
      .SetPassword (password)
      .Send ((response) =>
      {

        if(!response.HasErrors)
        {
          Debug.Log("Player Registered \n User Name: "+response.DisplayName);
          // authorize player
          DoGSLogin(username, password);
        }
        else
        {
          Debug.Log("Error Registering Player... \n "+response.Errors.JSON.ToString());
          //_info.text = response.Errors.JSON.ToString();
          DoLoginFailure();
        }

      });
  }


  /*
  private void ProcessGSLogout()
  {
    // Clear user account info
    PlayerPrefs.DeleteKey(Constants.kGSUsernameKey);
    PlayerPrefs.DeleteKey(Constants.kGSuserPassKey);
    _username.text = "";
    _password.text = "";
    GS.Reset();
    // Randomly generate a name to player
    GameManager.instance.SavePlayerScreenName(Constants.kPlayerTag + Random.Range(10000000,100000000));
    DoLoginFailure();
    DoAnonymouseLogin();
  }
  */
}