﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class LeaderboardCellUIController : MonoBehaviour
{
  public GameObject _dataCell;
  public GameObject _root;
  public List<LeaderboardCellData> _scoreDataList;
  public List<LeaderboardCellController> _cells;
  public Color _playerTextColor = new Color(255,0,0,255);
  public Color _normalTextColor = new Color(255,255,255,255);
  public Color _emptyTextColor = new Color(255,255,255,0);
  public int _totalEntries = 5;
  public int _lastEntry = 15;

  public void RefreshList()
  {
    //Debug.Log("Refresh list");
    string localUserID = GS.GSPlatform.UserId;
    bool foundPlayer = false;
    int foundPlayerIndex = -1;
    for(int i = 0; i < _scoreDataList.Count; ++i)
    {
      LeaderboardCellData d = _scoreDataList[i];
      if(d._id == localUserID && !foundPlayer)
      {
        //Debug.Log("Found player at: " + i);
        UpdateCell(i, d, _playerTextColor);
        foundPlayer = true;
        foundPlayerIndex = i;
      }
      else
      {
        // If this entry has no name, don't show
        if(d._name == null || d._name.Length == 0)
          UpdateCell(i, d, _emptyTextColor);
        else
          UpdateCell(i, d, _normalTextColor);
      }
    }

    int lastIndex = _scoreDataList.Count - 1;
    LeaderboardCellData lastCell = _scoreDataList[lastIndex];
    if(!foundPlayer)
    {
      //Debug.Log("Not found player, add player to the last entry");
      // Add player to the last entry
      lastCell._id = localUserID;
      lastCell._rank = _lastEntry;
      lastCell._name = PhotonNetwork.playerName;
      lastCell._score = PlayerPrefs.GetInt(Constants.kPlayerScoreKey, 0);

      UpdateCell(lastIndex, lastCell, _playerTextColor);
    }
    else
    {
      if(foundPlayerIndex != lastIndex)
      {
        //Debug.Log("Found player not at last index, empty last entry");
        // Add enpty data to the last entry
        // Add player to the last entry
        lastCell._id = "";
        lastCell._rank = _lastEntry;
        lastCell._name = "";
        lastCell._score = 0;

        UpdateCell(lastIndex, lastCell, _emptyTextColor);
      }
    }
  }

  public void UpdateData(int index, string id, int rank, string name, int score)
  {
    if(index < 0 || index >= _scoreDataList.Count)
      return;
    LeaderboardCellData d = _scoreDataList[index];
    d._id = id;
    d._rank = rank;
    d._name = name;
    d._score = score;
    //UpdateCell(index, d);
  }

  private void UpdateCell(int index, LeaderboardCellData d, Color c)
  {
    LeaderboardCellController cellData = _cells[index];
    cellData._id.text = d._rank.ToString();
    cellData._name.text = d._name;
    cellData._score.text = d._score.ToString();

    // Change color
    cellData._id.color = c;
    cellData._name.color = c;
    cellData._score.color = c;
  }

  private void AddOneCell()
  {
    GameObject obj = Instantiate(_dataCell, _root.transform);
    LeaderboardCellController cell = obj.GetComponent<LeaderboardCellController>();
    cell._id.text = "";
    cell._name.text = "";
    cell._score.text = "";
    _cells.Add(cell);
  }

  private void AddOneData()
  {
    //Debug.Log("add one data id: " + id + "rank: " + rank + " name: " + name + " score: " + score);
    //LeaderboardCellData d;
    LeaderboardCellData d = ScriptableObject.CreateInstance<LeaderboardCellData>();
    d._id = "";
    d._rank = -1;
    d._name = "";
    d._score = 0;
    _scoreDataList.Add(d);
  }

  void OnEnable()
  {
    for(int i = 0; i < _totalEntries + 1; ++i)
    {
      AddOneData();
      AddOneCell();
    }
  }

  //void OnDisable()
  //{
  //  ResetLeaderboard();
  //}

  private void ResetLeaderboard()
  {
    if(_root)
      Helpers.DestroyChildren(_root.transform);
    _scoreDataList.Clear();
    _cells.Clear();
  }
}