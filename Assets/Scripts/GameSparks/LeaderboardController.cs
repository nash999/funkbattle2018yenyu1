﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api.Requests;
using GameSparks.Core;
using GameSparks.Api.Responses;

public class LeaderboardController : MonoBehaviour
{
  public int _entryTotal = 20;
  public LeaderboardCellUIController _cellUIController;
  public float _udpateInterval = 1.0f;

  private bool m_isDBReady = false;
  private bool m_isLoading = false;

  void Start()
  {
    if(GS.Authenticated)
      LoadLeaderboardData();
  }

  void OnEnable()
  {
    InvokeRepeating("LoadLeaderboardData", _udpateInterval, _udpateInterval);

    Messenger.AddListener(Constants.kGSLoginSuccessNotification, OnGSLoginSuccess);
    Messenger.AddListener(Constants.kGSScorePostedNotification, OnScoreUpdate);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kGSLoginSuccessNotification, OnGSLoginSuccess);
    Messenger.RemoveListener(Constants.kGSScorePostedNotification, OnScoreUpdate);

    CancelInvoke("LoadLeaderboardData");
  }

  private void OnGSLoginSuccess()
  {
    m_isDBReady = true;
  }

  private void OnScoreUpdate()
  {
    LoadLeaderboardData();
  }

  private void LoadLeaderboardData()
  {
    //Debug.Log("LoadLeaderboardData");
    if(!m_isDBReady)
      return;
    if(m_isLoading)
      return;
    m_isLoading = true;

    new LeaderboardDataRequest_SurvivalModeGlobalLeaderboard()
      .SetEntryCount(_entryTotal)
      .Send((response) =>
      {
        if(response.HasErrors)
        {
          Debug.Log("Error getting leaderboard data: " + response.Errors.JSON);
        }
        else
        {
          //Debug.Log("Display scores");
          //_cellUIController.ResetLeaderboard();
          // Parse the data 
          foreach(GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data)
          {
            //Debug.Log("player score data: " + entry.JSONString);
            int rank = (int)entry.Rank;
            //string playerName = entry.UserName;
            string score = entry.JSONData["SCORE"].ToString();
            string playerName = entry.JSONData["NAME"].ToString();
            //Debug.Log("data; " + entry.JSONString);
            string id = entry.JSONData["userId"].ToString();

            //Debug.Log(rank + ": " + playerName + " : " + score);
            _cellUIController.UpdateData(rank-1, id, rank, playerName, int.Parse(score));
            //_cellUIController.AddOneData(id, rank, playerName, int.Parse(score));

          }
          _cellUIController.RefreshList();
          //_cellUIController.PopulateLeaderboard();
        }

        m_isLoading = false;
      });

  }
}