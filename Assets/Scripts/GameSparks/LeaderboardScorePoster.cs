﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api.Requests;

public class LeaderboardScorePoster : MonoBehaviour
{
  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kScoreUpdateNotification, OnScoreUpdate);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kScoreUpdateNotification, OnScoreUpdate);
  }


  private void OnScoreUpdate(Hashtable data)
  {
    int score = int.Parse(data[Constants.kPlayerScoreKey].ToString());
    PostScore(PhotonNetwork.playerName, score);
  }

  private void PostScore(string name, int score)
  {
    //Debug.Log("post score: " + score);
    string localUserID = GS.GSPlatform.UserId;
    //Debug.Log("Posting leaderboard score: " + score + " name: " + name);
    new LogEventRequest_PostScoreEventForSurvivalMode()
      .Set_SCORE(score)
      .Set_NAME(name)
      .Set_ID(localUserID)
      .Send((response) =>
      {
        if(response.HasErrors)
        {
          Debug.Log("Posting score failed: " + response.Errors.JSON.ToString());
        }
        else
        {
          //Debug.Log("Posting score succeed");
          Messenger.Broadcast(Constants.kGSScorePostedNotification);
        }

      });

  }
}