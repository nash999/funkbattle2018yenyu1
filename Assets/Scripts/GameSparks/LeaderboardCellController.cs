﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardCellController : MonoBehaviour
{
  public Text _id;
  public Text _name;
  public Text _score;
}
