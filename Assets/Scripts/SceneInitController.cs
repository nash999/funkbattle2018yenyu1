﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneInitController : MonoBehaviour
{
  public int _defaultFrameRate = 60;
  public bool _resetPlayerData = false;
  public bool _setDefaultFrameRate = true;

	// Use this for initialization
	void Start ()
  {
    if(_setDefaultFrameRate)
      Application.targetFrameRate = _defaultFrameRate;
    if(_resetPlayerData)
      PlayerPrefs.DeleteAll();
	}
}