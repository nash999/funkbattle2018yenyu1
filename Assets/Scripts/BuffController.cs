﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class BuffController : MonoBehaviour
{
  public List<GameObject> _buffs;
  public GameObject _spawnRoot;
  public List<Transform> _spawnLocations;
  public int _totalSpawnedObjs = 10;
  public string _buffItemType = "Buff";

  public float _invokeInterval = 10.0f;

  protected string m_invokeString = "SpawnObj";
  protected int m_currentTotalSpawnedObjs = 0;

  public virtual void OnEnable()
  {
    GetLocations();
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.AddListener<Hashtable>(Constants.kBuffDestroyNotification, OnBuffDestroy);
  }

  public virtual void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.RemoveListener<Hashtable>(Constants.kBuffDestroyNotification, OnBuffDestroy);
    CancelInvoke(m_invokeString);
    ResetLocations();
  }

  public void OnBuffDestroy(Hashtable data)
  {
    if(_buffItemType == data[Constants.kItemTypeKey].ToString())
      m_currentTotalSpawnedObjs--;
  }

  public virtual void OnCharSpawn(Hashtable data)
  {
    //Debug.Log("OnCharSpawn");
    // Start spawning 
    InvokeRepeating(m_invokeString, _invokeInterval, _invokeInterval);
  }

  public virtual void SpawnObj()
  {
    if(m_currentTotalSpawnedObjs >= _totalSpawnedObjs)
      return;
    // Randomly choose a obj to spawn on random location
    GameObject obj = _buffs[Random.Range(0, _buffs.Count)];
    Transform location = _spawnLocations[Random.Range(0, _spawnLocations.Count)];
    GameObject spawnObj = LeanPool.Spawn(obj, location.position, location.rotation); 
    SharedData sd = spawnObj.GetComponentInChildren<SharedData>();
    if(sd)
      sd._itemType = _buffItemType;
    m_currentTotalSpawnedObjs++;
    
  }

  public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    if (stream.isWriting)
    {
      // We own this player: send the others our data
      stream.SendNext(m_currentTotalSpawnedObjs);
    }
    else
    {
      // Network player, receive data
      //Debug.Log("Received data from: " + gameObject.name);
      m_currentTotalSpawnedObjs = (int)stream.ReceiveNext();
    }
  }

  private void GetLocations()
  {
    if(_spawnRoot == null)
      return;
    ResetLocations();
    for(int i = 0; i < _spawnRoot.transform.childCount; ++i)
    {
      _spawnLocations.Add(_spawnRoot.transform.GetChild(i));
    }
  }

  private void ResetLocations()
  {
    if(_spawnRoot == null)
      return;
    _spawnLocations.Clear();
  }
}
