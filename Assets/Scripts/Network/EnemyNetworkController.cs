﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNetworkController : MonoBehaviour
{
  public PhotonView _view;
  public List<GameObject> _listToDisable;
  public SharedData _data;

  /// <summary>
  /// This function is call when this object is instantiated by photon engine
  /// It is called before enable and must be placed on root level
  /// </summary>
  /// <param name="info">Info.</param>
  public void OnPhotonInstantiate(PhotonMessageInfo info)
  {
    object[] data = _view.instantiationData;

    if(data != null && data.Length > 0)
    {
      string itemTag = data[0].ToString();
      _data._itemType = itemTag;
    }
    UpdateOwnership();

    _data._root.BroadcastMessage(Constants.kNetworkInstantiateMS, SendMessageOptions.DontRequireReceiver);
  }

  /// <summary>
  /// Synchronize Avatar data
  /// </summary>
  /// <param name="stream">Stream.</param>
  /// <param name="info">Info.</param>
  public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    if (stream.isWriting)
    {
      // We own this player: send the others our data
      stream.SendNext(_data._currentLife);
      stream.SendNext(_data._currentEnergy);
      stream.SendNext(_data._inputKey);
      stream.SendNext(_data._team);
    }
    else
    {
      // Network player, receive data
      //Debug.Log("Received data from: " + gameObject.name);
      _data._currentLife = (int)stream.ReceiveNext();
      _data._currentEnergy = (int)stream.ReceiveNext();
      _data._inputKey = (Constants.KeyType)stream.ReceiveNext();
      _data._team = (Constants.TeamSide)stream.ReceiveNext();
    }
  }

  public void OnMasterClientSwitched()
  {
    UpdateOwnership();
    //Debug.Log("OnMasterClientSwitched: Updateownership");
  }

  private void UpdateOwnership()
  {
    // Enable/Disable based on network ownership
    foreach(GameObject g in _listToDisable)
    {
      g.SetActive(_view.isMine);
    }
  }

}