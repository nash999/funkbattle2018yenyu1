﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class NetworkGatherAttackController : GatherAttackController
{
  public PhotonView _view;
  public float _attackAgainInterval = 0.2f;

  private string m_rpcCallMethod = "SpawnNetworkObj";

  public override void DoAttack()
  {
    if(!_data._canAttack)
      return;
    if(_view == null)
      return;
    _view.RPC(m_rpcCallMethod, PhotonTargets.All, null);
    // Possible to shoot another one? 
    if(_data.FastAttackPercentage > 0)
    {
      if(Random.Range(0, 100) < _data.FastAttackPercentage)
      {
        //Debug.Log("fast attack");
        StartCoroutine(DoAttackAgain(_attackAgainInterval));
      }
    }
  }

  [PunRPC]
  public void SpawnNetworkObj()
  {
    //DoAttackSFX();
    SpawnObj();
  }

  private IEnumerator DoAttackAgain(float sec)
  {
    yield return new WaitForSeconds(sec);
    DoAttack();
  }
}