﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class NetworkLifeController : PlayerLifeController
{
  public PhotonView _view;

  public virtual void OnEnable()
  {
    //Debug.Log("Reset life for char");
    if(_hurtClip != null)
      _playerAudio.clip = _hurtClip;
    _data._currentLife = _data.MaxLife;
    _anim.ResetTrigger("Die");
    m_isDead = false;
  }

  void OnDiable()
  {
    //CancelInvoke(m_invokeString);
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, SharedData attackData)
  {
    if(!_view.isMine)
      return;
    if(m_isDead)
      return;
    if(!_canTakeDamage)
      return;

    TakeDamage(amount, hitPoint);
    AddLifeToAttacker(attackData);

    // If the player has lost all it's health and the death flag hasn't been set yet...
    if(_data._currentLife <= 0)
    {
      AddScoreToAttacker(attackData);
      // ... it should die.
      Death();
    }
    else
    {
      ApplySlowSpeedByAttacker(attackData);
      ApplyBleeding(attackData);
    }

  }

  public override void TakeDamage(int amount, Vector3 hitPoint, bool playSound = true)
  {
    int calculatedDamage = amount;
    bool canDodge = false;
    // Check if this damage can be dodge
    if(Random.Range(0, 99) < _data.DodgePercentage && calculatedDamage > 0)
    {
      canDodge = true;
    }
    else
    {
      if(amount > 0)
        calculatedDamage = ApplyDamage(amount);
      else
        calculatedDamage = ApplyDamageWithoutDefense(amount);
    }
    if(_view.isMine)
    {
      if(canDodge)
      {
        NotifyDamage(GameManager.instance._dodgeString, Color.blue);
      }
      else
      {
        if(calculatedDamage > 0)
        {
          NotifyDamage(Mathf.Abs(calculatedDamage).ToString(), Color.red);
        }
        else if(calculatedDamage < 0)
        {
          NotifyDamage(Mathf.Abs(calculatedDamage).ToString(), Color.green);
        }
      }
    }
    // Play the hurt sound effect
    // Negative damage means it recovers life
    // So we don't play hurt sfx here
    if(calculatedDamage > 0 && playSound)
    {
      _view.RPC("ProcessHurtFX", PhotonTargets.All, null);
    }
  }

  public virtual void Death()
  {
    // Set the death flag so this function won't be called again.
    m_isDead = true;

    _view.RPC("ProcessDieFX", PhotonTargets.All, null);
    //Debug.Log("Stop velocity");
    _rigid.velocity = Constants.kVector3Zero;
    _rigid.angularVelocity = Constants.kVector3Zero;
    _data._moveDir.Set(0,0);
    // Call despawn after time so animation can still be visible
    Invoke(m_invokeString, _timeToDespawn);
    NotifyDeath();
  }

  public virtual void NotifyDeath()
  {
    // Send out death notification 
    Hashtable data = new Hashtable();
    data[Constants.kCharacterKey] = _root;
    data[Constants.kCharacterPosKey] = _root.transform.position;
    data[Constants.kCharacterDataKey] = (int)(_data.DeathEXP);
    Messenger.Broadcast(Constants.kCharacterDeathNotification, data);

    // UA Event
    Analytics.CustomEvent(Constants.kPlayerLastLevelUAE, new Dictionary<string, object>
    {
      { "EXP Level", _data._currentPlayerLevel},
      { "Skill Level", _data._currentSkillLevel}
    });
  }

  private void Despawn()
  {
    //Debug.Log("Killed this obj: " + gameObject.name);
    PhotonNetwork.Destroy(_root);
  }

  [PunRPC]
  public void ProcessHurtFX()
  {
    _playerAudio.Play();
  }

  [PunRPC]
  public void ProcessDieFX()
  {
    // Make sure the life is zero
    _data._currentLife = 0;
    // Tell the animator that the player is dead.
    _anim.SetTrigger ("Die");

    // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
    if(_deathClip != null)
      _playerAudio.clip = _deathClip;
    _playerAudio.Play ();
  }

  // Called by broadcast message
  public void OnSkillUp()
  {
    // When player's skill is up, recover player's life

    // Recover life based on current life / max life ratio
    float ratio = _data._currentLife / (float)_data._maxLifeLevel[_data._currentMaxLifeLevel];
    //Debug.Log("Life ratio: " + ratio + " current life: " + _data._currentLife);
    _data._currentLife = (int)(_data.MaxLife * ratio);
    //Debug.Log("life after: " + _data._currentLife);

  }
}