﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour
{
  public PhotonView _view;
  public float _timeToReconnect = 1.0f;

  void OnEnable()
  {
    Messenger.AddListener(Constants.kNetworkSystemStartNotification, OnSystemStart);
  }
  
  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kNetworkSystemStartNotification, OnSystemStart);
  }
  
  // Use this for initialization
  void Start()
  {
    // Setup network engine and connect it to server
    InitNetwork();
  }

  #region Notification Callbacks 

  private void OnSystemStart()
  {
    // Trying to join a random room
    // If fail, create a new one
    // Join room
    PhotonNetwork.JoinRandomRoom();
  }

  #endregion

  private void CreateRoom()
  {
    Debug.Log("CreateRoom");
    int maxPlayers = Constants.kMaxPlayersInRoom;

    RoomOptions options = new RoomOptions();
    options.MaxPlayers = (byte)maxPlayers;
    options.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
    options.IsOpen = true;
    options.IsVisible = true;
    options.PublishUserId = true;

    // Create AI player data
    for(int i = 0; i < Constants.kMaxPlayersInRoom; ++i)
    {
      //string name = GameManager.instance.getRandomName();
      //Constants.TeamSide side = i < Constants.kMaxPlayersInRoom / 2 ? Constants.TeamSide.Red: Constants.TeamSide.Blue;
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kPlayerNameKey] = name;
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kTeamSideKey] = side;
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kAIPlayerIndexKey] = Random.Range(0, Constants.kMaxShipModels);
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kRegularWeaponKey] = Random.Range(0, Constants.kMaxWeaponModels);
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kShipEngineKey] = Random.Range(0, Constants.kMaxEngineModels);
      //int wingIndex = Random.Range(0, Constants.kMaxWingModels);
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kShipWingLeftKey] = wingIndex;
      //options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kShipWingRightKey] = wingIndex;
      
      //Debug.Log(i + " Create AI: " + name + " for team: " + options.CustomRoomProperties[Constants.kAIPlayerDataKey + "_" + i + "_" + Constants.kTeamSideKey]);
    }

    PhotonNetwork.CreateRoom(null, options, TypedLobby.Default);
  }

  private void InitNetwork()
  {
    // If player is already in game, we don't init
    if(PhotonNetwork.inRoom)
    {
      Debug.Log("Player is already in room, we come back later");
      // Reconnect is set seconds 
      Invoke("InitNetwork", _timeToReconnect);
      return;
    }
    if(GameManager.instance._isOffline)
    {
      PhotonNetwork.offlineMode = true;
      Debug.Log("Network is set to offline mode");
    }
    else
    {
      PhotonNetwork.offlineMode = false;
      // Initialize Photon engine
      if(!PhotonNetwork.connectedAndReady)
      {
        //PhotonNetwork.logLevel  = PhotonLogLevel.Full;
        PhotonNetwork.automaticallySyncScene = true;
        //version of the game/demo. used to separate older clients from newer ones (e.g. if incompatible)
        PhotonNetwork.ConnectUsingSettings(Constants.kServerVersionString);
      }
      else
      {
        Debug.Log("PhotonNetwork is already connected");
      }
    }
  }
 
  #region PhotonNetwork Callbacks 

  public void OnMasterClientSwitched()
  {
    //Debug.Log("OnMasterClientSwitched");
  }

  void OnJoinedLobby()
  {
    Debug.Log("OnJoinedLobby");
    // Send notification
    Messenger.Broadcast(Constants.kNetworkSystemReadyNotification);
  }
  
  void OnConnectedToPhoton()
  {
    Debug.Log("OnConnectedToPhoton");
  }
  
  void OnDisconnectedFromPhoton()
  {
    GameManager.instance._gameState = Constants.GameState.CharacterSelection;
    Messenger.Broadcast(Constants.kNetworkSystemDisconnectNotification);
    PhotonNetwork.offlineMode = true;
    Debug.Log("OnDisconnectedFromPhoton");
    Debug.Log("Trying to reconnect in: " + _timeToReconnect + " seconds");
    // Reconnect is set seconds 
    Invoke("InitNetwork", _timeToReconnect);

  }
  
  void OnPhotonRandomJoinFailed()
  {
    Debug.Log("OnPhotonRandomJoinFailed");
    Debug.Log("So we create a new room");
    CreateRoom();
  }
  
  void OnFailedToConnectToPhoton(DisconnectCause cause)
  {
    Debug.Log("OnFailedToConnectToPhoton: " + cause);
    // Set network to offline mode
    //PhotonNetwork.offlineMode = true;
    // Init again after set time
    //Debug.Log("OnFailedToConnectToPhoton");
    //Debug.Log("Trying to reconnect in: " + _timeToReconnect + " seconds");
    // Reconnect is set seconds 
    //Invoke("InitNetwork", _timeToReconnect);
    //Messenger.Broadcast(Constants.kNetworkDisconnectNotification);
  }

  void OnCreatedRoom()
  {
    //Debug.Log("OnCreatedRoom");
    //Debug.Log("Called by Master client only");
  }

  void OnJoinedRoom()
  {
    // JoinedRoom is called by master client only
    //Debug.Log("OnJoinedRoom");
    GameManager.instance.AssignTeam();
    // Send out notification
    Messenger.Broadcast(Constants.kNetworkSystemRoomReadyNotification);
  }

  void OnLeftRoom()
  {
    Debug.Log("OnLeftRoom");
    Messenger.Broadcast(Constants.kNetworkSystemRoomLeftNotification);
  }

  void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
  { 
    // PlayerConnected is called by master client when non master clients are joined
    //Debug.Log("OnPhotonPlayerConnected");
    
  }
  
  void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
  {
    //Debug.Log("OnPhotonPlayerDisconnected");
  }

  #endregion PhotonNetwork Callbacks 

  #region PUNRPC

  /*
  [PunRPC]
  private void DoGameStartRPC()
  {
    //Debug.Log("DoGameStartRPC");
    Messenger.Broadcast(Constants.kRoomReadyToBattleNotification);
  }
  */
  #endregion
}