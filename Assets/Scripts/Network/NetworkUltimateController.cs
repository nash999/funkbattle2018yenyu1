﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkUltimateController : UltimateController
{
  public PhotonView _view;

  private string m_RPCCallMethod = "RPCSpawnObj";

  public override void DoAttack()
  {
    if(_view == null)
      return;
    _view.RPC(m_RPCCallMethod, PhotonTargets.All, null);

  }

  public override void ProcessCameraSize(float size, float time)
  {
    if(_view && _view.isMine)
      base.ProcessCameraSize(size, time); 
  }

  [PunRPC]
  public void RPCSpawnObj()
  {
    DoAttackSFX();
    SpawnObj();
  }

}