﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkBuffSpawner : BuffController
{
  public override void OnCharSpawn(Hashtable data)
  {
    StartProcess();
  }

  public override void SpawnObj()
  {
    // Return if player is not in room
    if(!PhotonNetwork.inRoom)
      return;

    if(m_currentTotalSpawnedObjs >= _totalSpawnedObjs)
      return;
    // Randomly choose a obj to spawn on random location
    GameObject obj = _buffs[Random.Range(0, _buffs.Count)];
    Transform location = _spawnLocations[Random.Range(0, _spawnLocations.Count)];

    if(PhotonNetwork.inRoom)
    {
      object[] data = new object[]
      {
        0,
        _buffItemType
      };

      PhotonNetwork.InstantiateSceneObject
      (
        obj.name,
        location.position,
        location.rotation,
        0,
        data
      );
      m_currentTotalSpawnedObjs++;
    }
  }

  public void OnMasterClientSwitched()
  {
    StartProcess();
  }

  private void StartProcess()
  {
    //Debug.Log("StartProcess");
    CancelInvoke(m_invokeString);

    if(!PhotonNetwork.isMasterClient)
      return;
    // Start spawning 
    InvokeRepeating(m_invokeString, Random.Range(1, _invokeInterval), _invokeInterval);
  }

  public new void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    if (stream.isWriting)
    {
      // We own this player: send the others our data
      stream.SendNext(m_currentTotalSpawnedObjs);
    }
    else
    {
      // Network player, receive data
      //Debug.Log("Received data from: " + gameObject.name);
      m_currentTotalSpawnedObjs = (int)stream.ReceiveNext();
    }
  }
}
