﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkTimerKiller : TimerKiller
{
  public PhotonView _view;

  public override void DoDespawn()
  {
    if(_view.isMine)
    {
      Hashtable data = new Hashtable();
      data[Constants.kItemTypeKey] = _data._itemType;
      Messenger.Broadcast(Constants.kBuffDestroyNotification, data);
      PhotonNetwork.Destroy(_data._root);   
    }
  }
}