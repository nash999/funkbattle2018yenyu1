﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarNetworkController : MonoBehaviour
{
  public PhotonView _view;
  public List<GameObject> _listToDisable;
  public PlayerData _data;

  /// <summary>
  /// This function is call when this object is instantiated by photon engine
  /// It is called before enable and must be placed on root level
  /// </summary>
  /// <param name="info">Info.</param>
  public void OnPhotonInstantiate(PhotonMessageInfo info)
  {
    UpdateDisableList();
    UpdateID();
    UpdateName();
    _data._root.BroadcastMessage(Constants.kNetworkInstantiateMS, SendMessageOptions.DontRequireReceiver);
  }

  public virtual void UpdateName()
  {    
    if(_view.owner != null && _view.owner.NickName != null)
      _data._nickname = _view.owner.NickName;
  }

  public void UpdateDisableList()
  {
    // Enable/Disable based on network ownership
    foreach(GameObject g in _listToDisable)
    {
      g.SetActive(_view.isMine);
    }
  }

  public void UpdateID()
  {
    // Save the network ID
    //_data._networkID = info.sender.UserId;
    _data._networkID = _view.viewID.ToString();
  }

  /// <summary>
  /// Synchronize Avatar data
  /// </summary>
  /// <param name="stream">Stream.</param>
  /// <param name="info">Info.</param>
  public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    if(stream.isWriting)
    {
      // We own this player: send the others our data
      stream.SendNext(_data._currentLife);
      stream.SendNext(_data._currentEnergy);
      stream.SendNext(_data._inputKey);
      stream.SendNext(_data._team);
      stream.SendNext(_data._careerType);
      stream.SendNext(_data._nickname);

      // General levels
      stream.SendNext(_data._currentPlayerLevel);
      stream.SendNext(_data._currentSkillLevel);
      stream.SendNext(_data._currentMaxLifeLevel);
      stream.SendNext(_data._currentMaxEnergyLevel);
      stream.SendNext(_data._currentEnergyRecoveryLevel);
      stream.SendNext(_data._currentAttackIntervalLevel);
      stream.SendNext(_data._currentDefenseLevel);
      stream.SendNext(_data._currentForwardSpeedLevel);
      stream.SendNext(_data._currentRotationSpeedLevel);
      stream.SendNext(_data._currentFastAttackLevel);
      stream.SendNext(_data._currentMagnetLevel);

      // Weapon levels
      stream.SendNext(_data._currentWeaponRegularDamageLevel);
      stream.SendNext(_data._currentWeaponUltimateDamageLevel);
      stream.SendNext(_data._currentWeaponSuckBloodLevel);
      stream.SendNext(_data._currentWeaponActiveTimeLevel);
      stream.SendNext(_data._currentWeaponDamageForceLevel);
      stream.SendNext(_data._currentWeaponSlowSpeedLevel);
      stream.SendNext(_data._currentWeaponSlowSpeedResetLevel);
      stream.SendNext(_data._currentWeaponAttachDamageLevel);
      stream.SendNext(_data._currentWeaponRandomKnockBackLevel);
      stream.SendNext(_data._currentWeaponBleedingLevel);
    }
    else
    {
      // Network player, receive data
      //Debug.Log("Received data from: " + gameObject.name);
      int tempPlayerLevel;
      int tempSkillLevel;
      _data._currentLife = (int)stream.ReceiveNext();
      _data._currentEnergy = (int)stream.ReceiveNext();
      _data._inputKey = (Constants.KeyType)stream.ReceiveNext();
      _data._team = (Constants.TeamSide)stream.ReceiveNext();
      _data._careerType = (Constants.CareerType)stream.ReceiveNext();
      _data._nickname = (string)stream.ReceiveNext();

      tempPlayerLevel = (int)stream.ReceiveNext();
      tempSkillLevel = (int)stream.ReceiveNext();
      _data._currentMaxLifeLevel = (int)stream.ReceiveNext();
      _data._currentMaxEnergyLevel = (int)stream.ReceiveNext();
      _data._currentEnergyRecoveryLevel = (int)stream.ReceiveNext();
      _data._currentAttackIntervalLevel = (int)stream.ReceiveNext();
      _data._currentDefenseLevel = (int)stream.ReceiveNext();
      _data._currentForwardSpeedLevel = (int)stream.ReceiveNext();
      _data._currentRotationSpeedLevel = (int)stream.ReceiveNext();
      _data._currentFastAttackLevel = (int)stream.ReceiveNext();
      _data._currentMagnetLevel = (int)stream.ReceiveNext();

      // Weapon levels
      _data._currentWeaponRegularDamageLevel = (int)stream.ReceiveNext();
      _data._currentWeaponUltimateDamageLevel = (int)stream.ReceiveNext();
      _data._currentWeaponSuckBloodLevel = (int)stream.ReceiveNext();
      _data._currentWeaponActiveTimeLevel = (int)stream.ReceiveNext();
      _data._currentWeaponDamageForceLevel = (int)stream.ReceiveNext();
      _data._currentWeaponSlowSpeedLevel = (int)stream.ReceiveNext();
      _data._currentWeaponSlowSpeedResetLevel = (int)stream.ReceiveNext();
      _data._currentWeaponAttachDamageLevel = (int)stream.ReceiveNext();
      _data._currentWeaponRandomKnockBackLevel = (int)stream.ReceiveNext();
      _data._currentWeaponBleedingLevel = (int)stream.ReceiveNext();

      // If there is a change, we notify others
      if(tempPlayerLevel != _data._currentPlayerLevel)
      {
        _data._currentPlayerLevel = tempPlayerLevel;
        _data._root.BroadcastMessage(Constants.kLevelUpMS, SendMessageOptions.DontRequireReceiver);
      }
      if(tempSkillLevel != _data._currentSkillLevel)
      {
        _data._currentSkillLevel = tempSkillLevel;
        _data._root.BroadcastMessage(Constants.kSkillUpMS, SendMessageOptions.DontRequireReceiver);
      }
    }
  }

  [PunRPC]
  public void NetworkAddScore(int score)
  {
    if(_data && _data._scoreController)
      _data._scoreController.NetworkAddScore(score);
  }
}