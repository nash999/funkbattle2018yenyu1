﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffNetworkController : MonoBehaviour
{
  public PhotonView _view;
  public List<GameObject> _listToDisable;

  /// <summary>
  /// This function is call when this object is instantiated by photon engine
  /// It is called before enable and must be placed on root level
  /// </summary>
  /// <param name="info">Info.</param>
  public void OnPhotonInstantiate(PhotonMessageInfo info)
  {
    // Enable/Disable based on network ownership
    foreach(GameObject g in _listToDisable)
    {
      g.SetActive(_view.isMine);
    }

    // Check if we should attach to anything
    //Debug.Log("haha");
    object[] data = _view.instantiationData;
    if(data.Length == 0)
      return;
    bool shouldAttach = bool.Parse(data[0].ToString());
    if(shouldAttach == false)
      return;
    // Find the obj to attach
    string networkID = data[1].ToString();
    GameObject objToAttach = GameManager.instance.FindOnePlayerByNetworkID(networkID);
    if(objToAttach)
      transform.SetParent(objToAttach.transform, false);
  }
}