﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkBuffAttackController : BasicWeaponAttackController
{
  public PhotonView _view;

  public override void OnEnable()
  {
    base.OnEnable();
  }

  public override IEnumerator ApplyExplosion(Collider other)
  {
    yield return null;

    if(_explosion)
    {
    
      // Get the network ID 
      SharedData otherData = other.transform.root.GetComponentInChildren<SharedData>();

      object[] data = new object[] {
        _attachExplosion, // Should attach
        otherData._networkID
      };

      PhotonNetwork.Instantiate
    (
        _explosion.name,
        Constants.kVector3Zero,
        Constants.kQuaternionIdentity,
        0,
        data
      );
    }

  }

  public override bool ApplyDamage(GameObject other)
  {
    if(other.tag != Constants.kPlayerTag)
      return false;
    // Skip for non player
    LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return false;
    // Only has life 
    if(otherLife._data._currentLife <= 0)
      return false;
    // Only real player can gain exp
    if(otherLife._data._expController == null)
      return false;
    if(otherLife._data._scoreController == null)
      return false;
    // Apply for damage
    // Debug.Log("Apply damage to: " + otherLife.name + " : " + _data.WeaponDamage);
    otherLife.TakeDamage(_data.WeaponDamage, transform.position, _data);
    // Apply exp
    otherLife._data._expController.AddExp(_data.DeathEXP);
    // Apply score
    otherLife._data._scoreController.AddScore(_data.DeathScore);
    ApplyForce(other);
    return true;
  }

  public override void DoDespawn()
  {
    DoSFX();
    if(_view.isMine)
    {
      //Debug.Log("Kill this obj: " + gameObject.name);
      Hashtable data = new Hashtable();
      data[Constants.kItemTypeKey] = _data._itemType;
      Messenger.Broadcast(Constants.kBuffDestroyNotification, data);
      PhotonNetwork.Destroy(_root);
    }
  }
}