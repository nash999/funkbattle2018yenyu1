﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkEnemySpawner : EnemySpawner
{
  public override void Spawn()
  {
    // Skip if we are not in game play state
    if(GameManager.instance._gameState != Constants.GameState.GamePlay)
      return;
    // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
    if(!PhotonNetwork.isMasterClient)
      return;
    // Return if player is not in room
    if(!PhotonNetwork.inRoom)
      return;

    if(m_currentSpawnTotal >= _totalSpawedObjs)
    {
      //Debug.Log("Have enough enemies spawned: " + _enemyItemType);
      return;
    }

    // Find a random index between zero and one less than the number of spawn points.
    int spawnPointIndex = Random.Range(0, _spawnPoints.Count);

    object[] data = new object[]
    {
      _enemyItemType
    };
    //Debug.Log("Spawn enemy: " + _enemyItemType);

    PhotonNetwork.InstantiateSceneObject
    (
      _enemy.name,
      _spawnPoints[spawnPointIndex].position,
      _spawnPoints[spawnPointIndex].rotation,
      0,
      data
    );

    m_currentSpawnTotal++;
  }

  public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    if (stream.isWriting)
    {
      // We own this player: send the others our data
      stream.SendNext(m_currentSpawnTotal);
    }
    else
    {
      // Network player, receive data
      m_currentSpawnTotal = (int)stream.ReceiveNext();
    }
  }
}