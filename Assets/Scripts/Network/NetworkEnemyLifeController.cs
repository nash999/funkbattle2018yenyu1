﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkEnemyLifeController : EnemyLifeController
{
  public PhotonView _view;
  public Collider _bodyCollider;

  void OnEnable()
  {
    // Setting the current health when the enemy first spawns.
    _data._currentLife = _data.MaxLife;
    // Movement and animation are controlled by network
    if(_view.isMine)
      _meshAgent.enabled = true;
    _rigid.isKinematic = false;
    //m_isSinking = false;
    m_isDead = false;
    _bodyCollider.enabled = true;

    Messenger.AddListener(Constants.kCharacterRestartNotification, CheckOffline);
  }  

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, CheckOffline);
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, bool playSound = true)
  {
    //Debug.Log(gameObject + " Take damage: " + amount);
    int damageAfterDefense = amount;
    if(amount > 0)
      damageAfterDefense = ApplyDamage(amount);

    if(damageAfterDefense > 0)
    {
      NotifyDamage(Mathf.Abs(damageAfterDefense).ToString(), Color.red);
    }
    else if(damageAfterDefense < 0)
    {
      NotifyDamage(Mathf.Abs(damageAfterDefense).ToString(), Color.green);
    }
    if(playSound)
      _view.RPC("DoDamageFX", PhotonTargets.All, hitPoint);
  }

  public override void TakeDamage(int amount, Vector3 hitPoint, SharedData attackData)
  {
    //Debug.Log("Take damage");
    if(!_view.isMine)
    {
      //Debug.Log("View is not mine");
      return;
    }
    // If the enemy is dead...
    if(m_isDead)
      return;

    TakeDamage(amount, hitPoint);
    AddLifeToAttacker(attackData);

    // If the current health is less than or equal to zero...
    if(_data._currentLife <= 0)
    {
      // Add score
      AddScoreToAttacker(attackData);
      Death();
    }
    else
    {
      ApplySlowSpeedByAttacker(attackData);
      ApplyBleeding(attackData);
    }

  }

  public virtual void Death()
  {
    // The enemy is dead.
    m_isDead = true;
    _view.RPC("DoDeathFX", PhotonTargets.All);
    DoSinkingAnimation();
  }


  public new void DoSinkingAnimation()
  {
    //Debug.Log("enemylife start sinking");

    // Disable the Nav Mesh Agent.
    if(_meshAgent)
      _meshAgent.enabled = false;

    // Make rigid kinematic (since we use Translate to sink the enemy).
    _rigid.isKinematic = true;

    NotifyDeath();
    Invoke("DoDeath", 3.0f);

  }

  public override void DoDeath()
  {

    PhotonNetwork.Destroy(_root);
  }

  [PunRPC]
  public void DoDamageFX(Vector3 hitPoint)
  {
    // Play the hurt sound effect.
    _enemyAudio.Play();
  }

  [PunRPC]
  public new void DoDeathFX()
  {
    // Tell the animator that the enemy is dead.
    _anim.SetTrigger("Dead");
    // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
    _enemyAudio.clip = deathClip;
    _enemyAudio.Play();
    _bodyCollider.enabled = false;
  }

  private void NotifyDeath()
  {
    // Broadcast about this death
    Hashtable deathData = new Hashtable();
    deathData[Constants.kCharacterPosKey] = _root.transform.position;
    //Debug.Log("exp: " + _data.KilledEXP);
    deathData[Constants.kCharacterDataKey] = _data.DeathEXP;
    deathData[Constants.kItemTypeKey] = _data._itemType;
    Messenger.Broadcast(Constants.kEnemyDeathNotification, deathData);
  }

  private void CheckOffline()
  {
    if(PhotonNetwork.offlineMode)
    {
     _data._currentLife = 0;
      Death();
    }
  }
}