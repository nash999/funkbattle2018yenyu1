﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyFXController : MonoBehaviour
{
  public GameObject _vfx;
  public SharedData _data;
  public int _minValueToActivate = 20;

  void Update()
  {
    if(_vfx == null)
      return;
    //Debug.Log("key: " + _data._key);
    if(_data._inputKey == Constants.KeyType.Press)
    {
      if(_data._currentEnergy >= _minValueToActivate && !_vfx.activeSelf)
        _vfx.SetActive(true);
    }
    else
    {
      if(_vfx.activeSelf)
        _vfx.SetActive(false);
    }
  }
}
