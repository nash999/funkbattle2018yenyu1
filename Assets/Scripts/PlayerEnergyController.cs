﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnergyController : MonoBehaviour
{
  public SharedData _data;

  public void AddEnergy(int currentEnergy)
  {
    // Fill energy
    _data._currentEnergy = Mathf.Clamp(_data._currentEnergy + currentEnergy, 0, _data.MaxEnergy);
  }

  public float CurrentEnergy()
  {
    return _data._currentEnergy;
  }

  public bool IsFull()
  {
    return (_data._currentEnergy == _data.MaxEnergy);
  }

  public void ResetEnergy()
  {
    _data._currentEnergy = 0;
  }

  void OnEnable()
  {
    ResetEnergy();
  }

  void OnDisable()
  {
    ResetEnergy();
  }
}