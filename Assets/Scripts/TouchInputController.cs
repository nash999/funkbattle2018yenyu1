﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInputController : MonoBehaviour
{
  public AvatarNetworkController _network;

  public SharedData _data;
  public ActionBase _attack;
  public DashController _dash;
  public Vector3 _turningOffset = new Vector3(0, 0, 0);

  public bool _canTakeInput = true;

  //private Vector3 m_dashDir = Vector3.zero;
  //private bool m_doDash = false;
  private bool m_doAttack = false;
  private bool m_doPressAttack = false;

  void OnEnable()
  {
    if(_network && _network._view && _network._view.isMine)
    {
      Messenger.AddListener<Hashtable>(Constants.kJoystickTransformNotification, OnJoystickTransform);
      Messenger.AddListener(Constants.kJoystickTransformCompleteNotification, OnJoystickTransformComplete);
      Messenger.AddListener<Hashtable>(Constants.kJoystickFlickNotification, OnJoystickFlick);
      Messenger.AddListener<Hashtable>(Constants.kJoystickTapNotification, OnJoystickTap);
      Messenger.AddListener(Constants.kJoystickPressNotification, OnJoystickPress);
      Messenger.AddListener(Constants.kJoystickReleaseNotification, OnJoystickRelease);
    }
  }

  void OnDisable()
  {
    if(_network && _network._view && _network._view.isMine)
    {
      Messenger.RemoveListener<Hashtable>(Constants.kJoystickTransformNotification, OnJoystickTransform);
      Messenger.RemoveListener(Constants.kJoystickTransformCompleteNotification, OnJoystickTransformComplete);
      Messenger.RemoveListener<Hashtable>(Constants.kJoystickFlickNotification, OnJoystickFlick);
      Messenger.RemoveListener<Hashtable>(Constants.kJoystickTapNotification, OnJoystickTap);
      Messenger.RemoveListener(Constants.kJoystickPressNotification, OnJoystickPress);
      Messenger.RemoveListener(Constants.kJoystickReleaseNotification, OnJoystickRelease);
    }
  }

  private void OnJoystickTransform(Hashtable data)
  {
    if(!CanTakeInput())
      return;
    if(!_data._canMove)
      return;

    // Get joystick position
    Vector2 JoystickPos = (Vector2)data[Constants.kJoystickTransformKey];

    /*
    Vector3 objPosWithOffset = _data._root.transform.position + _turningOffset;
    // Generate a plane that intersects the transform's position with an upwards normal.
    Plane playerPlane = new Plane(Vector3.up, objPosWithOffset);

    // Generate a ray from the cursor position
    Ray ray = Camera.main.ScreenPointToRay (JoystickPos);
 
    // Determine the point where the cursor ray intersects the plane.
    // This will be the point that the object must look towards to be looking at the mouse.
    // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
    //   then find the point along that ray that meets that distance.  This will be the point
    //   to look at.
    float hitdist = 0.0f;
    // If the ray is parallel to the plane, Raycast will return false.
    if (playerPlane.Raycast (ray, out hitdist)) 
    {
      // Get the point along the ray that hits the calculated distance.
      //Vector3 targetPoint = ray.GetPoint(hitdist);
      Vector3 finalDir = (ray.GetPoint(hitdist) - objPosWithOffset);

      _data._rot = Quaternion.LookRotation(finalDir);
      // Determine the target rotation.  This is the rotation if the transform looks at the target point.
      //_data._moveDir.Set(finalDir.x, finalDir.y);

      //_shipData._weaponRotation = Quaternion.LookRotation(targetPoint - objPosWithOffset);
    }
    */

    // Calcualte direction based on avatar position
    //Vector3 objPos = Camera.main.WorldToScreenPoint(_data._root.transform.position);
    //Debug.Log("objPos: " + objPos);
    //Vector2 dir = (JoystickPos - new Vector2(objPos.x, objPos.y)).normalized;
    Vector2 dir = JoystickPos.normalized;
    //Debug.Log("joy: " + JoystickPos + " dir: " + dir);
    //Debug.Log("transform");
    _data._moveDir.Set(dir.x, dir.y);
  }

  private void OnJoystickTransformComplete()
  {
    //Debug.Log("complete");
    //_data._moveDir.Set(0, 0);
  }

  private void OnJoystickFlick(Hashtable data)
  {
    if(!CanTakeInput())
      return;

    //Debug.Log("Flick");
    Vector2 flickDir = (Vector2)data[Constants.kJoystickFlickKey];
    _dash.DoDash(flickDir);
    //m_doDash = true;
  }

  private void OnJoystickTap(Hashtable data)
  {
    if(!CanTakeInput())
      return;

    if(!_data._canAttack)
      return;

    // Get the tap position 
    Vector2 dir = (Vector2)data[Constants.kJoystickTapKey];
    m_doAttack = true;
    _attack.ProcessDown();

    // Calculate the angle 
    /*
    Vector2 avatarPos = Camera.main.WorldToScreenPoint(_data._root.transform.position);
    //float angleTouch = Vector2.Angle(avatarPos, dir);
    // Get the normized direction
    Vector2 d = (dir - avatarPos).normalized;
    //Debug.Log("dir: " + d);
    //_data._attackDir.Set(dir.x, dir.y);
    _data._attackDir.Set(d.x, d.y);
    */

    // Calculation the direction
    Vector3 objPosWithOffset = _data._root.transform.position + _turningOffset;
    // Generate a plane that intersects the transform's position with an upwards normal.
    Plane playerPlane = new Plane(Vector3.up, objPosWithOffset);

    // Generate a ray from the cursor position
    Ray ray = Camera.main.ScreenPointToRay(dir);
     
    // Determine the point where the cursor ray intersects the plane.
    // This will be the point that the object must look towards to be looking at the mouse.
    // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
    //   then find the point along that ray that meets that distance.  This will be the point
    //   to look at.
    float hitdist = 0.0f;
    // If the ray is parallel to the plane, Raycast will return false.
    if(playerPlane.Raycast(ray, out hitdist))
    {
      // Get the point along the ray that hits the calculated distance.
      //Vector3 targetPoint = ray.GetPoint(hitdist);
      Vector3 finalDir = (ray.GetPoint(hitdist) - objPosWithOffset);
      //Debug.Log("Final Dir: " + finalDir);
      _data._attackDir.Set(finalDir.x, finalDir.z);
      //_data._rot = Quaternion.LookRotation(finalDir);
    }

    StopCoroutine("ResetAttackDir");
    StartCoroutine(ResetAttackDir());
      
    
  }

  private void OnJoystickPress()
  {
    if(!CanTakeInput())
      return;

    m_doPressAttack = true;
    _attack.ProcessDown();
    _attack.ProcessPress();
    //Debug.Log("input Press");
  }

  private void OnJoystickRelease()
  {
    if(!CanTakeInput())
      return;
    //Debug.Log("OnJoystickRelease: " + m_doAttack + " : " + m_doPressAttack);
    if(m_doAttack || m_doPressAttack)
    {
      _attack.ProcessUp();
      m_doAttack = m_doPressAttack = false;
    }
    //_data._attackDir.Set(0, 0);
  }

  private bool CanTakeInput()
  {
    if(_data != null && _data._currentLife <= 0)
      return false;
    return _canTakeInput;
  }

  private IEnumerator ResetAttackDir()
  {
    yield return new WaitForSeconds(_data.AttackInterval);
    _data._attackDir.Set(0, 0);
  }

  /*
    private void CheckTurning()
  {
    Vector3 pos = transform.position + _turningOffset;

    // Generate a plane that intersects the transform's position with an upwards normal.
    Plane playerPlane = new Plane(Vector3.up, pos);

    // Generate a ray from the cursor position
    Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
 
    // Determine the point where the cursor ray intersects the plane.
    // This will be the point that the object must look towards to be looking at the mouse.
    // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
    //   then find the point along that ray that meets that distance.  This will be the point
    //   to look at.
    float hitdist = 0.0f;
    // If the ray is parallel to the plane, Raycast will return false.
    if (playerPlane.Raycast (ray, out hitdist)) 
    {
      // Get the point along the ray that hits the calculated distance.
      Vector3 targetPoint = ray.GetPoint(hitdist);

      // Determine the target rotation.  This is the rotation if the transform looks at the target point.
      _shipData._weaponRotation = Quaternion.LookRotation(targetPoint - pos);
    }
  */
}