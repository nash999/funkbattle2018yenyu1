﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerHUDController : MonoBehaviour
{
  public SharedData _playerData;
  public Constants.TeamSide _team;
  public Slider _life;
  public Slider _lifeRed;
  public Slider _energy;
  public Slider _exp;
  public Text _expText;
  //public Text _lifeText;
  public float _lifeAnimationDelay = 0.5f;
  public float _lifeAniamtionTime = 0.5f;
  public float _baseScale = 100;

  private float m_lastLifeValue;
  private int m_animationID;

  void Start()
  {
    // Disable by default
    _life.gameObject.SetActive(false);
    _energy.gameObject.SetActive(false);
    _exp.gameObject.SetActive(false);
  }

  void OnEnable()
  {
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.AddListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.AddListener(Constants.kTeamUpdateNotification, OnTeamUpdate);
    Messenger.AddListener(Constants.kCharacterResumeNotification, OnCharRestart);
  }

  void OnDisable()
  {
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterDeathNotification, OnCharDeath);
    Messenger.RemoveListener(Constants.kTeamUpdateNotification, OnTeamUpdate);
    Messenger.RemoveListener(Constants.kCharacterResumeNotification, OnCharRestart);
  }

  private void OnTeamUpdate()
  {
    _team = GameManager.instance._localTeam;
  }

  private void OnCharSpawn(Hashtable data)
  {
    // Make sure the right team
    GameObject obj = (GameObject)data[Constants.kCharacterKey];
    if(obj == null)
      return;
    SharedData spawnData = obj.GetComponentInChildren<SharedData>();
    if(spawnData == null)
      return;
    if(spawnData._team != _team)
      return;
    _playerData = spawnData;
    // Enable UI
    _life.gameObject.SetActive(true);
    _energy.gameObject.SetActive(true);
    _exp.gameObject.SetActive(true);
    _lifeRed.value = _life.value = ((_playerData._currentLife / (float)_playerData.MaxLife) * _life.maxValue);
  }

  private void OnCharDeath(Hashtable data)
  {
    GameObject obj = (GameObject)data[Constants.kCharacterKey];
    if(obj == null)
      return;
    SharedData spawnData = obj.GetComponentInChildren<SharedData>();
    if(spawnData == null)
      return;
    if(spawnData._team != _team)
      return;
    _playerData = null;
    // Disable UI
    _life.gameObject.SetActive(false);
    _energy.gameObject.SetActive(false);
    _exp.gameObject.SetActive(false);
  }

  private void OnCharRestart()
  {
    _life.gameObject.SetActive(true);
    _energy.gameObject.SetActive(true);
    _exp.gameObject.SetActive(true);
  }

  void Update()
  {
    if(_playerData == null)
      return;
    if(_life == null)
      return;
    if(_energy == null)
      return;
    UpdateLifeScale();
    _life.value = ((_playerData._currentLife / (float)_playerData.MaxLife) * _life.maxValue);
    _energy.value = _playerData._currentEnergy;
    int currExp = (_playerData._currentEXP - _playerData._expController.CurrentLevelExpBase());
    int maxExp = (_playerData._expController.NextLevelExpBase() - _playerData._expController.CurrentLevelExpBase());
    if(_playerData._currentPlayerLevel >= _playerData._maxLevel-1)
    {
      _exp.value = 1;
      _exp.maxValue = 1;
    }
    else
    {
      _exp.value = currExp;
      _exp.maxValue = maxExp;
    }
    //Debug.Log("curr exp: " + currExp + " max exp: " + maxExp);
    _expText.text = (_playerData._currentPlayerLevel + 1).ToString();
    //_lifeText.text = _playerData._currentLife + "/" + _playerData.MaxLife; 
    if(m_lastLifeValue != _life.value)
    {
      if(m_lastLifeValue > _life.value)
      {
        LeanTween.cancel(m_animationID);
        m_animationID = LeanTween.value(_lifeRed.value, _life.value, _lifeAniamtionTime).setDelay(_lifeAnimationDelay).setOnUpdate(OnLifeValueUpdate).id;
      }
      else
      {
        _lifeRed.value = _life.value;
      }
      //CancelInvoke("AnimateLife", _lifeAnimationDelay);
    }
    m_lastLifeValue = _life.value;
  }

  private void OnLifeValueUpdate(float value)
  {
    _lifeRed.value = value;
  }

  private void UpdateLifeScale()
  {
    Vector3 newScale = _life.transform.localScale;
    float scaleRatio = (_playerData.MaxLife / _baseScale);
    newScale.x = scaleRatio;
    _life.transform.localScale = newScale;
    //_lifeRed.transform.localScale = newScale;
  }
}