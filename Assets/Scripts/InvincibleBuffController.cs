﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleBuffController : MonoBehaviour
{
  public int _damageTotal = 5;
  public float _attackForce = 5.0f;
  public float _attackRadius = 3.0f;
  public LayerMask _mask;

  private PlayerLifeController m_life;
  private SharedData m_data;
  private float m_lastCheckTime;
  private float m_checkInterval = 0.3f;
  private Collider[] m_colliders;

  void OnTransformParentChanged()
  {
    ProcessLogic();
  }

  void Update()
  {
    if(m_lastCheckTime > 0)
    {
      m_lastCheckTime -= Time.deltaTime;
      return;
    }

    m_lastCheckTime = m_checkInterval;

    m_colliders = Physics.OverlapSphere(transform.position, _attackRadius, _mask);
    foreach(Collider c in m_colliders)
    {
      ApplyDamage(c);
    }
  }

  private void ApplyDamage(Collider other)
  {
    // Make sure we have the reference
    if(m_life == null)
      return;
    // Get life script 
    LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return;
    if(otherLife._data._team == m_data._team)
      return;
    // Apply for damage
    otherLife.TakeDamage(_damageTotal, transform.position, m_data);
    Rigidbody otherRigid = other.GetComponentInParent<Rigidbody>();
    if(otherRigid)
      otherRigid.AddForce((other.transform.position - transform.position) * _attackForce, ForceMode.Impulse);
  }

  /*
  void OnTriggerEnter(Collider other)
  {
    // Make sure we have the reference
    if(m_life == null)
      return;
    // Get life script 
    LifeBase otherLife = other.transform.root.GetComponentInChildren<LifeBase>();
    if(otherLife == null)
      return;
    if(otherLife._data._team == m_data._team)
      return;
    // Apply for damage
    otherLife.TakeDamage(_damageTotal, transform.position, m_data);
    Rigidbody otherRigid = other.GetComponentInParent<Rigidbody>();
    if(otherRigid)
      otherRigid.AddForce((other.transform.position - transform.position) * _attackForce, ForceMode.Impulse);
  }
  */
  void OnDisable()
  {
    if(m_life)
      m_life._canTakeDamage = true;
    m_life = null;
    m_data = null;
  }

  private void ProcessLogic()
  {
    m_life = transform.root.GetComponentInChildren<PlayerLifeController>();
    m_life._canTakeDamage = false;
    m_data = transform.root.GetComponentInChildren<SharedData>();
  }
}