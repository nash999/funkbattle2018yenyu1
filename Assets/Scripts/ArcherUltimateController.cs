﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

public class ArcherUltimateController : NetworkUltimateController
{
  public List<Transform> _pos;

  public override void SpawnObj()
  {
    
    if(_data._careerType == Constants.CareerType.Crossbowman)
    {
      CheckWeapon();

      foreach(Transform trans in _pos)
      {
        GameObject spawnedObj = null;
        // GameObject spawnedObj = LeanPool.Spawn(_attackObj, _spawnTrans.position, _spawnTrans.rotation);
        if(m_shouldAttachWeapon)
        {
          //Debug.Log("Attach ultimate");
          spawnedObj = LeanPool.Spawn(m_objToSpawn, trans.position, trans.rotation, trans);
          spawnedObj.transform.localPosition = Constants.kVector3Zero;
          spawnedObj.transform.localRotation = Constants.kQuaternionIdentity;
        }
        else
        {
          //Debug.Log("Don't attach ultimate");
          spawnedObj = LeanPool.Spawn(m_objToSpawn, trans.position, trans.rotation);
        }
        spawnedObj.transform.localScale = GetWeaponSize();
        //Debug.Log("Size: " + spawnedObj.transform.localScale);
        InitSpawnObj(spawnedObj);
      }

    }
    else
    {
      base.SpawnObj();
    }
  }

}