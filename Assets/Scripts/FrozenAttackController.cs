﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrozenAttackController : MonoBehaviour
{
  public SharedData _attackData;
  public Material _frozenMaterial;

  private float m_oriForSpeedRatio;
  private float m_oriRotSpeedRatio;
  private SharedData m_targetData;
  private Material[] m_originalMaterials;

  private bool m_shouldProcessLogic = false;

  void OnWeaponInit()
  {
    // IF there are more than 1 script attached to the parent, we skip 
    FrozenAttackController[] facs = transform.root.GetComponentsInChildren<FrozenAttackController>();
    if(facs.Length > 1)
    {
      m_shouldProcessLogic = false;
      return;
    }
    // Get the data from parent
    LifeBase lb = transform.root.GetComponent<LifeBase>();
    if(lb == null)
      return;
    m_targetData = lb._data;
    if(m_targetData)
    {
      m_oriForSpeedRatio = m_targetData.ForSpeedRatio;
      m_oriRotSpeedRatio = m_targetData.RotSpeedRatio;

      Renderer r = transform.root.GetComponentInChildren<SkinnedMeshRenderer>();
      if(r)
      {                
        // Save old one
        m_originalMaterials = r.materials;
        // Change to frozen material

        // Materials are returned as instance, so need to put them back
        Material[] temp = r.materials;
        for(int i = 0; i < temp.Length; ++i)
        {
          temp[i] = _frozenMaterial;
        }
        r.materials = temp;

      }
    }
    m_shouldProcessLogic = true;
  }

  void Update()
  {
    if(!m_shouldProcessLogic)
      return;

    if(m_targetData)
    {
      m_targetData.ForSpeedRatio = _attackData.ForSpeedRatio;
      m_targetData.RotSpeedRatio = _attackData.RotSpeedRatio;
      m_targetData._canAttack = false;
      m_targetData._canMove = false;
      m_targetData._canRotate = false;
    }
  }

  void OnDisable()
  {
    if(!m_shouldProcessLogic)
      return;
    // Reset data
    if(m_targetData)
    {
      m_targetData.ForSpeedRatio = m_oriForSpeedRatio;
      m_targetData.RotSpeedRatio = m_oriRotSpeedRatio;
      m_targetData._canAttack = true;
      m_targetData._canMove = true;
      m_targetData._canRotate = true;
      Renderer r = transform.root.GetComponentInChildren<SkinnedMeshRenderer>();
      //Debug.Log("Reset: " + m_originalMaterials.Length);
      if(r)
      {
        r.materials = m_originalMaterials;
        //Debug.Log("Reset material");
        //Debug.Log("reset material");
      }
      m_originalMaterials = null;
    }

    m_targetData = null;
    m_shouldProcessLogic = false;
  }
}
