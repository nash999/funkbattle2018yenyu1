﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDPRController : MonoBehaviour
{
  public GameObject _menu;

  public void OnYESDown()
  {
    UpdateChoice(1);
  }

  public void OnNODown()
  {
    UpdateChoice(2);
  }

  void OnEnable()
  {
    int resultIndex = PlayerPrefs.GetInt(Constants.kGDPRChoiceIndexKey, 0);
    _menu.SetActive(resultIndex == 0);

    //Debug.Log("index: " + resultIndex);
    GameManager.instance._GDPRChoiceIndex = resultIndex;

    if(resultIndex != 0)
      Messenger.Broadcast(Constants.kGDPRReadyNotification);

  }

  private void UpdateChoice(int index)
  {
    GameManager.instance._GDPRChoiceIndex = index;
    _menu.SetActive(false);
    PlayerPrefs.SetInt(Constants.kGDPRChoiceIndexKey, index);
    Messenger.Broadcast(Constants.kGDPRReadyNotification);
  }
}