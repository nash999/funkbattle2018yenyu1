﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashController : MonoBehaviour 
{
  public Rigidbody _body;
  public PlayerData _data;
  public Animator _animator;

  private bool m_isDashing = false;
  private Vector3 m_dash;

	public void DoDash(Vector2 dir)
  {
    if(m_isDashing)
      return;
    m_isDashing = true;
    m_dash = new Vector3(dir.x, 0, dir.y);
    //_data._moveDir
   // _data._moveDir = m_dash;
   _data._moveDir.Set(dir.x, dir.y);
    StartCoroutine(doDash());
  }

  private IEnumerator doDash()
  {
    //Vector3 dashVelocity = Vector3.Scale(m_dash, m_data._dashSpeed * new Vector3((Mathf.Log(1f / (Time.deltaTime * m_body.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * m_body.drag + 1)) / -Time.deltaTime)));
    Vector3 dashVelocity = Vector3.Scale
      (
        m_dash, 
        _data._dashSpeed * new Vector3((Mathf.Log(1f / (Time.deltaTime * _body.drag + 1)) / -Time.deltaTime),
        0, 
        (Mathf.Log(1f / (Time.deltaTime * _body.drag + 1)) / -Time.deltaTime))
      );
    // Change the avatar direction to face the dash direction
    _body.rotation = Quaternion.LookRotation(m_dash);
    // Add force to push the avatar regardless of gravity
    _body.AddForce(dashVelocity, ForceMode.VelocityChange);
    // While dashing, disable gravity
    _body.useGravity = false;
  
    // Activate animation
    if(_animator)
      _animator.SetTrigger("DoDash");
    //m_playerAnimator.SetTrigger("RollTrigger");
    // Send out notification about this dash action
    //Messenger.Broadcast(Constants.kDashNotification);
    // Reset gravity after dash is over
    StartCoroutine(ResetForDash(_data._dashAntiGravityTime));
    yield return null;
  }

  private IEnumerator ResetForDash(float time)
  {
    yield return new WaitForSeconds(time);
    _body.useGravity = true;
    m_isDashing = false;
    //if(_animator)
    //  _animator.SetInteger("Dash", 0);
  }
}