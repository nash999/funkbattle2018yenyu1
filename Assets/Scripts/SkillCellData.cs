﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillCellData : MonoBehaviour
{
  public List<Constants.UpdateType> _types;
  public List<int> _items;
  public List<string> _desc;
}
