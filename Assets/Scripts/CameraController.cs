﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.CodeDom.Compiler;

public class CameraController : MonoBehaviour
{
  public GameObject _camera;
  public GameObject _bgRoot;
  public GameObject _bgBack;
  public GameObject _bgMiddle;
  public GameObject _bgFront;

  public float _tiltSpeed = 1.0f;
  public float _bgSizeSpeed = 0.5f;
  public float _backRatio = 0.3f;
  public float _middleRatio = 1.0f;
  public float _frontRatio = 2.0f;
  public float _resetTime = 1.0f;
  public float _bgSize = 10f;

  private Vector3 m_defaultPos;
  private Vector3 m_defaultRot;
  private Vector3 m_acc;
  private Vector3 m_defaultBackPos;
  private Vector3 m_defaultMiddlePos;
  private Vector3 m_defaultFrontPos;
  private Vector3 m_bgSize;
  private float m_defaultBgSize;

  void OnEnable()
  {
    Init();
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.AddListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.AddListener(Constants.kNetworkSystemDisconnectNotification, OnNetworkDisconnect);
    Messenger.AddListener<Hashtable>(Constants.kCameraSizeUpdateNotification, OnSizeUpdate);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharRestart);
    Messenger.RemoveListener<Hashtable>(Constants.kCharacterSpawnNotification, OnCharSpawn);
    Messenger.RemoveListener(Constants.kNetworkSystemDisconnectNotification, OnNetworkDisconnect);
    Messenger.RemoveListener<Hashtable>(Constants.kCameraSizeUpdateNotification, OnSizeUpdate);
  }

  void FixedUpdate()
  {
    m_acc = Input.acceleration;
    // Left is right
    m_acc.x = -m_acc.x;
    // We don't need z
    m_acc.z = 0;
    //Debug.Log("acc: " + m_acc);
  }

  void LateUpdate()
  {
    _bgBack.transform.localPosition = Vector3.Lerp
    (
      _bgBack.transform.localPosition, 
      m_defaultBackPos + (m_acc * _backRatio),
      Time.deltaTime * _tiltSpeed
    );
    _bgMiddle.transform.localPosition = Vector3.Lerp
    (
      _bgMiddle.transform.localPosition, 
      m_defaultMiddlePos + (m_acc * _middleRatio),
      Time.deltaTime * _tiltSpeed
    );
    _bgFront.transform.localPosition = Vector3.Lerp
    (
      _bgFront.transform.localPosition,
      m_defaultFrontPos + (m_acc * _frontRatio),
      Time.deltaTime * _tiltSpeed
    );

    m_bgSize = _bgRoot.transform.localScale;
    m_bgSize.x = _bgSize;
    m_bgSize.y = _bgSize;
    _bgRoot.transform.localScale = Vector3.Lerp(_bgRoot.transform.localScale, m_bgSize, _bgSizeSpeed * Time.deltaTime);
  }

  private void Init()
  {
    m_defaultPos = _camera.transform.position;
    m_defaultRot = _camera.transform.rotation.eulerAngles;
    m_defaultBackPos = _bgBack.transform.localPosition;
    m_defaultMiddlePos = _bgMiddle.transform.localPosition;
    m_defaultFrontPos = _bgFront.transform.localPosition;
    m_defaultBgSize = _bgRoot.transform.localScale.x;
  }

  private void Reset()
  {
    LeanTween.move(_camera, m_defaultPos, _resetTime);
    LeanTween.rotate(_camera, m_defaultRot, _resetTime);
    _bgSize = m_defaultBgSize;

    _bgBack.SetActive(true);
    _bgMiddle.SetActive(true);
    _bgFront.SetActive(true);
  }

  private void OnCharRestart()
  {
    Reset();
  }

  private void OnNetworkDisconnect()
  {
    Reset();
  }

  private void OnCharSpawn(Hashtable data)
  {
    _bgBack.SetActive(true);
    _bgMiddle.SetActive(true);
    _bgFront.SetActive(false);
  }

  private void OnSizeUpdate(Hashtable data)
  {
    // Get new size from data 
    //Debug.Log("data for camera: " + data.ToStringFull());
    _bgSize = int.Parse(data[Constants.kCameraSizeKey].ToString());
    //Debug.Log("Update camera size: " + _bgSize);
  }

}
