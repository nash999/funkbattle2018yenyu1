﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionTextController : MonoBehaviour
{
  public Text _version;
  public string _postDesc;

  void OnEnable()
  {
    _version.text = Application.version + " " + _postDesc;
  }
}