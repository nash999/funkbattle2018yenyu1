﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimationController : MonoBehaviour
{
  public GameObject _obj;
  public Vector3 _dest;
  public float _animationTime = 0.5f;
  public bool _isGlobal = false;

  private Vector3 m_originalDest;

  void OnEnable()
  {
    // Save the original position
    m_originalDest = _obj.transform.localPosition;
    //Debug.Log(gameObject.name + " Local position: " + m_originalDest);
    if(_isGlobal)
    {
      LeanTween.move(_obj, _dest, _animationTime).setEaseOutBack();
    }
    else
    {
      LeanTween.moveLocal(_obj, _dest, _animationTime).setEaseOutBack();
    }
  }

  void OnDisable()
  {
    if(_isGlobal)
    {
      LeanTween.move(_obj, m_originalDest, _animationTime).setEaseOutBack();
    }
    else
    {
      LeanTween.moveLocal(_obj, m_originalDest, _animationTime).setEaseOutBack();
    }
  }
}
