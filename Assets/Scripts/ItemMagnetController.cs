﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMagnetController : MonoBehaviour
{
  public PlayerData _data;
  public float _radius = 10.0f;
  public float _force = 5;
  public LayerMask _mask;
  private Vector3 m_currentMovement;

  void Update()
  {
    if(_data.MagnetForce == 0)
      return;
    
    //Debug.Log("check magnet objs");
    Collider[] colliders = Physics.OverlapSphere(transform.position, _radius * _data.MagnetForce, _mask, QueryTriggerInteraction.Collide);
    //Debug.Log("check magnet objs: " + colliders.Length);
    foreach(Collider c in colliders)
    {
      // Make sure they are the owner in network system 
      PhotonView view = c.transform.root.GetComponent<PhotonView>();
      if(view == null)
        continue;
      if(!view.isMine)
        continue;
      m_currentMovement = ((transform.position - c.transform.position).normalized * _data.MagnetForce * _force);
      // No height force
      m_currentMovement.y = 0;

      Rigidbody rigid = c.transform.root.GetComponent<Rigidbody>();
      if(rigid)
      {
        rigid.AddForce(m_currentMovement, ForceMode.VelocityChange);
      }

    }
  }
}
