﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationController : MonoBehaviour
{
  private bool _wasPlayering = false;
  void OnApplicationPause(bool pauseStatus)
  {
    // Skip if resume
    if(pauseStatus)
    {
      _wasPlayering = PhotonNetwork.inRoom;

      //Debug.Log("Game paused: " + _wasPlayering);
      //Debug.Log("pause the game");
      // If this player is the master client, we switch master client so AI can keep running
      if(PhotonNetwork.isMasterClient && PhotonNetwork.inRoom && PhotonNetwork.otherPlayers.Length > 0)
      {
        Debug.Log("Masterclient in room paused the game, switch master client");
        PhotonNetwork.SetMasterClient(PhotonNetwork.otherPlayers[0]);
      }

    }
    else
    {
      // Player comeback 
      //Debug.Log("Game resumed: " + _wasPlayering);
      //Debug.Log("player in room: " + PhotonNetwork.inRoom);
      // Check if player is still in room, or kicked out by photon 
      if(!PhotonNetwork.inRoom && _wasPlayering)
      {
        // Send out restart notifiation
        Messenger.Broadcast(Constants.kCharacterRestartNotification);
      }
    }




  }
}