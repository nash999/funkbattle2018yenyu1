﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingMenuController : MonoBehaviour 
{
  public GameObject _menu;
  public GameObject _menuBtn;
  public bool _isMenuOn = false;
  public float _animationTime = 0.5f;
  public Vector3 _rotation = new Vector3(0,180,0);

  private Vector3 m_defaultMenuPos;

  public void OnSettingButtonDown()
  {
    _isMenuOn = !_isMenuOn;

    SetMenuVisible(_isMenuOn);
  }

  void OnEnable()
  {
    m_defaultMenuPos = _menu.transform.localPosition;
  }

  private void SetMenuVisible(bool isVisible)
  {
    if(isVisible)
    {
      // Show menu
      LeanTween.moveLocal(_menu, Constants.kVector3Zero, _animationTime).setEaseOutBack();
      LeanTween.rotateLocal(_menuBtn, _rotation, _animationTime).setEaseInOutSine();
    }
    else
    {
      // Hid menu
      LeanTween.moveLocal(_menu, m_defaultMenuPos, _animationTime);
      LeanTween.rotateLocal(_menuBtn, -_rotation, _animationTime).setEaseInOutSine();
    }
  }
}