﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DateController : MonoBehaviour
{
  DateTime currentDate;
  DateTime oldDate;
  public int _firstDateThreshold = 1;
  public int _secondDateThreshold = 2;
  public int _thirdDateThreshold = 3;
  public float _firstLevelReductionRatio = 0.8f;
  public float _secondLevelReductionRatio = 0.5f;
  public float _thirdLevelReductionRatio = 0.3f;
  public float _firstLevelScoreReductionRatio = 0.6f;
  public float _secondLevelScoreReductionRatio = 0.3f;
  public float _thirdLevelScoreReductionRatio = 0.1f;

  private string m_dateString = "SystemDateString";

  void Start()
  {
    // Reset level based on day diff since last entry
    ResetLevel(ComputeDayDiff());
    // Save the current date
    SaveDate();
  }

  void OnApplicationQuit()
  {
    SaveDate();
  }

  void OnEnable()
  {
    Messenger.AddListener(Constants.kCharacterRestartNotification, OnCharacterRestart);
  }

  void OnDisable()
  {
    Messenger.RemoveListener(Constants.kCharacterRestartNotification, OnCharacterRestart);
  }

  private void OnCharacterRestart()
  {
    ResetLevel(ComputeDayDiff());
  }

  private int ComputeDayDiff()
  {
    //Store the current time when it starts
    currentDate = System.DateTime.Now;

    //Grab the old time from the player prefs as a long
    long temp = Convert.ToInt64(PlayerPrefs.GetString(m_dateString, currentDate.ToBinary().ToString()));

    //Convert the old time from binary to a DataTime variable
    DateTime oldDate = DateTime.FromBinary(temp);
    print("oldDate: " + oldDate);
    print("currDate: " + currentDate);

    //Use the Subtract method and store the result as a timespan variable
    TimeSpan difference = currentDate.Subtract(oldDate);
    print("Difference: " + difference.Days);
    return difference.Days;
  }

  private void ResetLevel(int days)
  {
    //Debug.Log("Day diff: " + days);
    if(days > _thirdDateThreshold)
    {
      Debug.Log("Delete all player data, since it has passed: " + _thirdDateThreshold);
      // Reset score data
      PlayerPrefs.DeleteKey(Constants.kPlayerScoreKey);
      // Reset level data
      PlayerPrefs.DeleteKey(Constants.kPlayerExpKey);
    }
    else
    {
      // Calculate reduced exp and level
      float newExp = (float)PlayerPrefs.GetInt(Constants.kPlayerExpKey);
      float newScore = (float)PlayerPrefs.GetInt(Constants.kPlayerScoreKey);

      if(days >= _secondDateThreshold)
      {
        Debug.Log("Update player data after: " + _secondDateThreshold + " days");
        Debug.Log("exp ratio: " + _thirdLevelReductionRatio);
        Debug.Log("Score ratio: " + _thirdLevelScoreReductionRatio);
        newExp *= _thirdLevelReductionRatio;
        newScore *= _thirdLevelScoreReductionRatio;
      }
      else if(days >= _firstDateThreshold)
      {
        Debug.Log("Update player data after: " + _secondDateThreshold + " days");
        Debug.Log("exp ratio: " + _secondLevelReductionRatio);
        Debug.Log("Score ratio: " + _secondLevelScoreReductionRatio);
        newExp *= _secondLevelReductionRatio;
        newScore *= _secondLevelScoreReductionRatio;
      }
      else
      {
        Debug.Log("Update player data");
        Debug.Log("exp ratio: " + _firstLevelReductionRatio);
        Debug.Log("Score ratio: " + _firstLevelScoreReductionRatio);
        newExp *= _firstLevelReductionRatio;
        newScore *= _firstLevelScoreReductionRatio;

      }

      PlayerPrefs.SetInt(Constants.kPlayerExpKey, (int)newExp);
      PlayerPrefs.SetInt(Constants.kPlayerScoreKey, (int)newScore);
    }

    // Delete skill level so player can choose skills under new level
    PlayerPrefs.DeleteKey(Constants.kSkillLevelKey);

    // Delete general level data
    PlayerPrefs.DeleteKey(Constants.kMaxEnergyLevelKey);
    PlayerPrefs.DeleteKey(Constants.kEnergyRecoveryLevelKey);
    PlayerPrefs.DeleteKey(Constants.kRotationLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponDamageForceLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponSlowSpeedResetLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponAttachDamageLevelKey);

    // Skill A
    PlayerPrefs.DeleteKey(Constants.kAttackIntervalLevelKey);
    PlayerPrefs.DeleteKey(Constants.kMaxLifeLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponKillTimerLevelKey);
    // Skill B
    PlayerPrefs.DeleteKey(Constants.kWeaponRegularDamageLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponUltimateDamageLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponBleedingLevelKey);

    // Skill C
    PlayerPrefs.DeleteKey(Constants.kWeaponSuckBloodLevelKey);
    PlayerPrefs.DeleteKey(Constants.kDefenseLevelKey);
    PlayerPrefs.DeleteKey(Constants.kForwardSpeedLevelKey);

    // Skill Random
    PlayerPrefs.DeleteKey(Constants.kWeaponSlowSpeedLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponKnockbackLevelKey);
    PlayerPrefs.DeleteKey(Constants.kWeaponFastAttackLevelKey);
    PlayerPrefs.DeleteKey(Constants.kSkillDodgeLevelKey);
    PlayerPrefs.DeleteKey(Constants.kSkillMagnetLevelKey);

    // Career
    PlayerPrefs.DeleteKey(Constants.kCareerTypeKey);
  }

  private void SaveDate()
  {
    //Savee the current system time as a string in the player prefs class
    PlayerPrefs.SetString(m_dateString, System.DateTime.Now.ToBinary().ToString());
    print("Saving this date to prefs: " + System.DateTime.Now);
  }
}