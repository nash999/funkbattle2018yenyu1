﻿using UnityEngine;
using Werewolf.StatusIndicators.Components;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Werewolf.StatusIndicators.Demo {
  public class CharacterDemo4 : MonoBehaviour {
    public SplatManager Splats { get; set; }

    public void OnQDown()
    {
      Splats.SelectSpellIndicator("Fireball");
    }

    public void OnWDown()
    {
      Splats.SelectSpellIndicator("Frost Blast");
    }

    public void OnEDown()
    {
      Splats.SelectSpellIndicator("Frost Nova");
    }

    public void OnCancelDown()
    {
      Splats.CancelSpellIndicator();
    }

    void Start() {
      Splats = GetComponentInChildren<SplatManager>();
      Splats.SelectSpellIndicator("Frost Blast");
    }

    void Update() {
      if (Input.GetMouseButtonDown(0)) {
        Splats.CancelSpellIndicator();
      }
      if (Input.GetKeyDown(KeyCode.Q)) {
        Splats.SelectSpellIndicator("Fireball");
      }
      if (Input.GetKeyDown(KeyCode.W)) {
        Splats.SelectSpellIndicator("Frost Blast");
      }
      if (Input.GetKeyDown(KeyCode.E)) {
        Splats.SelectSpellIndicator("Frost Nova");
      }
    }
  }
}